﻿using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Auth.Model.ProfileContext.Entities
{
    public class UserProfile
    {
        public UserProfile()
        {
            Regions = new HashSet<Region>();
        }
        public Guid Id { get; set; }
        public Guid LinkId { get; set; }
        public string Title { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        /// <summary>
        /// Код синхронизации с 1С
        /// </summary>                
        public string SyncCode1C { get; set; }
               

        //public virtual City City { get; set; }
        /// <summary>
        /// Регионы
        /// </summary>
        public virtual ICollection<Region> Regions { get; set; }
        public virtual ICollection<UserPhone> UserPhones { get; set; }
        public virtual ICollection<UserAddress> UserAddresses { get; set; }
    }
}
