﻿using System;

namespace KonigDev.Lazurit.Auth.Model.ProfileContext.Entities
{
    public class Settings
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Value { get; set; }
    }
}
