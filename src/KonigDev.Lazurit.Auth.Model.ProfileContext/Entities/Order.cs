﻿using System;

namespace KonigDev.Lazurit.Auth.Model.ProfileContext.Entities
{
    /* todo подумать куда вынести, используется при удалении пользователя с проверкой есть ли у него заказы. */
    public class Order
    {
        public Guid Id { get; set; }
        public Guid UserProfileId { get; set; }
    }
}
