﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Auth.Model.ProfileContext.Entities
{
    public class UserPhone
    {
        public Guid Id { get; set; }
        public Guid UserProfileId { get; set; }
        public string Phone { get; set; }
        public bool IsDefault { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}
