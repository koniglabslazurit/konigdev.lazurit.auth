﻿using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Auth.Model.ProfileContext.Entities
{
    public class Region
    {
        public Region()
        {
            UserProfiles = new HashSet<UserProfile>();
        }
        public Guid Id { get; set; }
        public string Title { get; set; }

        /// <summary>
        /// Хеш пароль для региона, который приходит к нам из 1с. Его нельзя менять. Другой хеш этого пароля хранит Identity в сервисе авторизации.
        /// </summary>
        public string LazuritPasswordHash { get; set; }

        /// <summary>
        /// Код синхронизации с 1С
        /// </summary>                
        public string SyncCode1C { get; set; }

        /// <summary>
        /// Продавцы региона
        /// </summary>
        public virtual ICollection<UserProfile> UserProfiles { get; set; }
    }
}
