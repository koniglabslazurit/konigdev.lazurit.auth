﻿using KonigDev.Lazurit.Auth.Model.ProfileContext.Entities;
using System.Data.Entity;

namespace KonigDev.Lazurit.Auth.Model.ProfileContext.DataContext
{
    public class LazuritProfileContext : DbContext
    {
        public LazuritProfileContext() : base("name=LazuritProfileContext")
        {
            
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            #region User
            modelBuilder.Entity<UserProfile>()
                .ToTable("UsersProfiles")
                .HasKey(p => p.Id);
            #endregion

            #region UserPhone
            modelBuilder.Entity<UserPhone>()
                .ToTable("UsersPhones")
                .HasKey(p => p.Id);

            modelBuilder.Entity<UserPhone>()
               .HasRequired(p => p.UserProfile)
               .WithMany(p => p.UserPhones)
               .HasForeignKey(p => p.UserProfileId)
               .WillCascadeOnDelete(false);
            #endregion

            #region UserAddress
            modelBuilder.Entity<UserAddress>()
                .ToTable("UsersAddresses")
                .HasKey(p => p.Id);

            modelBuilder.Entity<UserAddress>()
              .HasRequired(p => p.UserProfile)
              .WithMany(p => p.UserAddresses)
              .HasForeignKey(p => p.UserProfileId)
              .WillCascadeOnDelete(false);
            #endregion

            #region Region
            modelBuilder.Entity<Region>()
                .ToTable("Regions")
                .HasKey(p => p.Id);
            #endregion

            #region Order
            modelBuilder.Entity<Order>()
               .ToTable("Orders")
               .HasKey(x => x.Id);
            #endregion

            #region Setting
            modelBuilder.Entity<Settings>()
                .ToTable("Settings")
                .HasKey(p => p.Id);

            #endregion
        }
    }
}
