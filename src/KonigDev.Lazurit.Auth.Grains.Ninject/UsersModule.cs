﻿using KonigDev.Lazurit.Auth.Grains.Interfaces;
using KonigDev.Lazurit.Auth.Grains.User.QueryHandlers;
using KonigDev.Lazurit.Auth.Model.DTO.Users;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Queries;
using Ninject.Modules;
using System.Collections.Generic;
using KonigDev.Lazurit.Auth.Grains.User.CommandHandlers;
using KonigDev.Lazurit.Auth.Grains.User.CommandHandlers.Email;
using KonigDev.Lazurit.Auth.Grains.User.CommandHandlers.UsersHandlers;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Commands;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Commands.Email;

namespace KonigDev.Lazurit.Auth.Grains.Ninject
{
    //todo перенести это в грейн, так как только тамэто необходимо
    public class UsersModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IAuthQueryHandler<GetRoleQuery, RoleDto>>().To<GetRoleQueryHandler>();
            Bind<IAuthQueryHandler<AllUsersQuery, List<UserDto>>>().To<AllUsersQueryHandler>();
            Bind<IAuthQueryHandler<GetUserByIdQuery, UserDto>>().To<GetUserByIdQueryHandler>();
            Bind<IAuthCommandHandler<CreateUserCmd>>().To<CreateUserCmdHandler>();
            Bind<IAuthCommandHandler<SendEmailToUserCmd>>().To<SendEmailToUserCmdHandler>();
            Bind<IAuthQueryHandler<GetEmailConfirmTokenForUserQuery, ConfirmationTokenForUserDto>>().To<GetEmailConfirmTokenForUserQueryHandler>();
            Bind<IAuthCommandHandler<ConfirmEmailForUserCmd>>().To<ConfirmEmailForUserCmdHandler>();
            Bind<IAuthQueryHandler<GetUserByEmailQuery, UserDto>>().To<GetUserByEmailQueryHandler>();
            Bind<IAuthCommandHandler<SendRecoveryPasswordForUsersCmd>>().To<SendRecoveryPasswordForUsersCmdHandler>();
            Bind<IAuthQueryHandler<UserByLoginAndPassQuery, UserDto>>().To<UserByLoginAndPassQueryHandler>();
            Bind<IAuthCommandHandler<UpdateUserIdentityCmd>>().To<UpdateUserIdentityCmdHandler>();
            Bind<IAuthQueryHandler<UserByIDAndPassQuery, UserDto>>().To<UserByIDAndPassQueryHandler>();
        }
    }
}