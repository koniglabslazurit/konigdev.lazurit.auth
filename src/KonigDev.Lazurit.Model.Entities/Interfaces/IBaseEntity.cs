﻿using System;

namespace KonigDev.Lazurit.Model.Entities.Interfaces
{
    public interface IBaseEntity
    {
        Guid Id { get; set; }
    }
}
