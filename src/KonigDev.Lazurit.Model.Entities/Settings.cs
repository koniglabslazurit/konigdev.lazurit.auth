﻿using System;
using KonigDev.Lazurit.Model.Entities.Interfaces;

namespace KonigDev.Lazurit.Model.Entities
{
    public class Settings : IBaseEntity
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Value { get; set; }
    }
}
