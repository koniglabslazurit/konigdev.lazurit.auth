﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Model.Entities
{
    class Nomenclature
    {
        public string Id { get; set; }
        public string Code { get; set; }
        public string Articul { get; set; }
        public float BasePrice { get; set; }
        public bool IsInAction { get; set; }
        public float DiscPercent { get; set; }
        public float DiscSumm { get; set; }
        public string Descr { get; set; }
        public int Size { get; set; }
        public int Weight { get; set; }
        public int AmountInPack { get; set; }
        public string Color { get; set; }
        public string Decoration { get; set; }
        public string Material { get; set; }
    }
}
