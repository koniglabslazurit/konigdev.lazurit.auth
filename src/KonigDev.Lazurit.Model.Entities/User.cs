﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Model.Entities
{
    public class UserProfile : IBaseEntity
    {
        public UserProfile()
        {
            Regions=new HashSet<Region>();
        }
        public Guid Id { get; set; }

        public string Title { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }        
        public string Address { get; set; }
        /// <summary>
        /// Код синхронизации с 1С
        /// </summary>                
        public string SyncCode1C { get; set; }

        /// <summary>
        /// Регионы
        /// </summary>
        public virtual ICollection<Region> Regions { get; set; } 
    }
}
