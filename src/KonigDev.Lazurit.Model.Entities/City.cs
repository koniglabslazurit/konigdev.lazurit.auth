﻿using KonigDev.Lazurit.Model.Entities.Interfaces;
using System;

namespace KonigDev.Lazurit.Model.Entities
{
    public class City : IBaseEntity
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public Guid RegionId { get; set; }

        public virtual Region Region { get; set; }
    }
}