﻿using KonigDev.Lazurit.Auth.InternalAPI.AccountApi.Implementations;
using KonigDev.Lazurit.Auth.InternalAPI.AccountApi.Interfaces;
using KonigDev.Lazurit.Auth.InternalAPI.Services.Implementations;
using KonigDev.Lazurit.Core.Services;
using Ninject.Modules;

namespace KonigDev.Lazurit.Auth.InternalAPI.NinjectModules
{
    public class InternalApiModule:NinjectModule
    {
        public override void Load()
        {
            Bind<IAccountInternalAPI>().To<AccountInternalAPI>();
            Bind<ILoggingService>().To<LoggingService>();
        }
    }
}
