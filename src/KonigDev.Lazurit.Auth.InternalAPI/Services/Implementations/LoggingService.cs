﻿using System;
using KonigDev.Lazurit.Core.Services;
using NLog;

namespace KonigDev.Lazurit.Auth.InternalAPI.Services.Implementations
{
    public class LoggingService : Logger, ILoggingService
    {
        readonly Logger _logger;
        public LoggingService()
        {
            _logger=LogManager.GetCurrentClassLogger();
        }

        public void Debug(Exception exception)
        {
            _logger.Debug(exception);
        }

        public void Error(Exception exception)
        {
            _logger.Error(exception);
        }

        public void ErrorWithIncomeParameter(Exception exception,object parameters)
        {
            //сделано в трассировку, что бы на продакшине её отключить. Таким образом будет соблюдаться моменты безопасности приложения. Иначе в логе могут храниться секьюрные данные.
            _logger.Trace(parameters);
            _logger.Error(exception);
        }

        public void Fatal(Exception exception)
        {
            _logger.Fatal(exception);
        }

        public void Info(Exception exception)
        {
            _logger.Info(exception);
        }

        public void InfoMessage(string message)
        {
            _logger.Info(message);
        }

        public void Trace(Exception exception)
        {
            _logger.Trace(exception);
        }

        public void Warn(Exception exception)
        {
            _logger.Warn(exception);
        }
    }
}
