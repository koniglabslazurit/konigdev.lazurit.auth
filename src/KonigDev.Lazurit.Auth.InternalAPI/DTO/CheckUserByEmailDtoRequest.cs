﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Auth.InternalAPI.DTO
{
    public class CheckUserByEmailDtoRequest
    {
        public string Email { get; set; }
    }
}
