﻿using System;

namespace KonigDev.Lazurit.Auth.InternalAPI.DTO
{
    public class DtoChangeEmailRequest
    {
        public Guid UserId { get; set; }
        public string NewEmail { get; set; }
    }
}
