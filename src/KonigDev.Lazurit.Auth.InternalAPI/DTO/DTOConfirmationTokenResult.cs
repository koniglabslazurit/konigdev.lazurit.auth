﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Auth.InternalAPI.DTO
{
    public class DTOConfirmationTokenResult:DTOErrorResult
    {
        public string Token { get; set; }
        public Guid UserId { get; set; }
    }
}
