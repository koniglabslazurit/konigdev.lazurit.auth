﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Auth.InternalAPI.DTO
{
    public class DtoUserConfirmRegistrationRequest
    {
        public Guid UserId { get; set; }
        public string Token { get; set; }
        public string Address { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string Phone { get; set; }
        public bool IsAgreed { get; set; }
    }
}
