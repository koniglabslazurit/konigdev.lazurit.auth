﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KonigDev.Lazurit.Auth.InternalAPI.AccountApi.Interfaces;
using KonigDev.Lazurit.Auth.Grains.Interfaces;
using KonigDev.Lazurit.Auth.InternalAPI.DTO;
using KonigDev.Lazurit.Auth.Model.DTO.Users;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Commands;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Commands.Email;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Exceptions;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Queries;
using KonigDev.Lazurit.Core.Services;

namespace KonigDev.Lazurit.Auth.InternalAPI.AccountApi.Implementations
{
    public class AccountInternalAPI : IAccountInternalAPI
    {
        private readonly IAuthHandlersFactory _authFactory;
        private readonly ILoggingService _logger;

        public AccountInternalAPI(IAuthHandlersFactory authFactory, ILoggingService loggingService)
        {
            _authFactory = authFactory;
            _logger = loggingService;
        }

        /// <summary>
        ///Восстановление пароля
        /// </summary>
        /// <param name="email">электронная почта пользователя</param>
        /// <returns>ошибку</returns>
        public async Task<DTOErrorResult> RecoveryPasswordAsync(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return await Task.FromResult(new DTOErrorResult
                {
                    IsSuccess = false,
                    ErrorMessage = "Не возможно найти пользователя с таким email адресом."
                });

            var findUserByEmailHandler = _authFactory.CreateQueryHandler<GetUserByEmailQuery, UserDto>();
            UserDto user;
            try
            {
                user = await findUserByEmailHandler.Execute(new GetUserByEmailQuery { Email = email });
            }
            catch (Exception ex)
            {
                _logger.ErrorWithIncomeParameter(ex, email);
                return await Task.FromResult(new DTOErrorResult
                {
                    IsSuccess = false,
                    ErrorMessage = "Система не нашла пользователя с таким Email."
                });
            }
            if (user == null)
                return await Task.FromResult(new DTOErrorResult
                {
                    IsSuccess = false,
                    ErrorMessage = "Не возможно найти пользователя с таким email адресом."
                });
            if (!user.EmailConfirmed)
                return await Task.FromResult(new DTOErrorResult
                {
                    IsSuccess = false,
                    ErrorMessage = "Не возможно отослать сообщение, так как пользователь ранее не подтвердил свой email адрес."
                });
            if (user.LockoutEnabled)
                return await Task.FromResult(new DTOErrorResult
                {
                    IsSuccess = false,
                    ErrorMessage = "Не возможно отослать сообщение, так как пользователь заблокирован."
                });
            var cmdParameters = new SendRecoveryPasswordForUsersCmd { Users = new List<UserDto> { user } };
            try
            {
                var sendEmailCmd = _authFactory.CreateCommandHandler<SendRecoveryPasswordForUsersCmd>();
                await sendEmailCmd.Execute(cmdParameters);
            }
            catch (SellerDoesNotContainRegionException ex)
            {
                _logger.ErrorWithIncomeParameter(ex, cmdParameters);
                return new DTOErrorResult
                {
                    IsSuccess = false,
                    ErrorMessage = "Данный продавец не входит не в один регион."
                };
            }
            catch (Exception e)
                when (e is RoleDoesNotExistException || e is UserDoesNotExistException || e is EmailNotSendException)
            {
                _logger.ErrorWithIncomeParameter(e, cmdParameters);
                return new DTOErrorResult { IsSuccess = false, ErrorMessage = e.Message };
            }
            catch (Exception e)
            {
                _logger.ErrorWithIncomeParameter(e, cmdParameters);
                return new DTOErrorResult { IsSuccess = false, ErrorMessage = "Не получилось восстановить пароль. Попробуйте позже или обратитесь к администратору" };
            }

            return await Task.FromResult(new DTOErrorResult() { IsSuccess = true });
        }
        //TODO покрыть тестами
        public async Task<DTOConfirmationTokenResult> CreateUser(string password, CreateUserDto userDto)
        {
            var createUserCmd = new CreateUserCmd
            {
                Password = password,
                User = userDto
            };
            try
            {
                var createUserHandler = _authFactory.CreateCommandHandler<CreateUserCmd>();
                await createUserHandler.Execute(createUserCmd);
            }
            catch (UserDoesNotCreateException ex)
            {
                _logger.ErrorWithIncomeParameter(ex, createUserCmd);
                return new DTOConfirmationTokenResult
                {
                    IsSuccess = false,
                    ErrorMessage = ex.Message
                };
            }
            catch (RoleDoesNotExistException ex)
            {
                _logger.ErrorWithIncomeParameter(ex, createUserCmd);
                return new DTOConfirmationTokenResult
                {
                    IsSuccess = false,
                    ErrorMessage = ex.Message
                };
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return new DTOConfirmationTokenResult
                {
                    IsSuccess = false,
                    ErrorMessage = ex.Message
                };
            }
            var confirmationTokenForUserHandler = _authFactory.CreateQueryHandler<GetEmailConfirmTokenForUserQuery, ConfirmationTokenForUserDto>();
            var getEmailConfirmTokenParam = new GetEmailConfirmTokenForUserQuery
            {
                Password = password,
                UserName = userDto.Email
            };
            ConfirmationTokenForUserDto confirmTokenForUser = null;
            try
            {
                confirmTokenForUser = await confirmationTokenForUserHandler.Execute(getEmailConfirmTokenParam);
            }
            catch (UserDoesNotExistException ex)
            {
                _logger.ErrorWithIncomeParameter(ex, getEmailConfirmTokenParam);
                return new DTOConfirmationTokenResult
                {
                    IsSuccess = false,
                    ErrorMessage = ex.Message
                };
            }
            catch (Exception ex)
            {
                _logger.ErrorWithIncomeParameter(ex, getEmailConfirmTokenParam);
                return new DTOConfirmationTokenResult
                {
                    IsSuccess = false,
                    ErrorMessage = ex.Message
                };
            }

            return new DTOConfirmationTokenResult
            {
                IsSuccess = true,
                UserId = confirmTokenForUser.UserId,
                Token = confirmTokenForUser.Token
            };
        }

        public async Task<DtoConfirmationTokenChangeEmailResult> GetTokenForChangeEmail(ChangeEmailDto dto)
        {
            var confirmationTokenForUserHandler = _authFactory.CreateQueryHandler<GetEmailConfirmTokenForUserByEmailQuery, ConfirmationTokenForUserDto>();
            var getEmailConfirmTokenParam = new GetEmailConfirmTokenForUserByEmailQuery
            {
                Email = dto.Email
            };
            ConfirmationTokenForUserDto confirmTokenForUser = null;
            try
            {
                confirmTokenForUser = await confirmationTokenForUserHandler.Execute(getEmailConfirmTokenParam);
              
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return new DtoConfirmationTokenChangeEmailResult
                {
                    IsSuccess = false,
                    ErrorMessage = ex.Message
                };
            }

            return new DtoConfirmationTokenChangeEmailResult
            {
                IsSuccess = true,
                Token = confirmTokenForUser.Token
            };
        }

        public async Task ChangeEmail(DtoChangeEmailRequest request)
        {
            var checkUserByEmailQueryHandelr = _authFactory.CreateQueryHandler<CheckUserByEmailQuery, CheckUserByEmailDtoResult>();
            CheckUserByEmailDtoResult result = null;
            try
            {
               result = await checkUserByEmailQueryHandelr.Execute(new CheckUserByEmailQuery {Email = request.NewEmail });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            var changeEmailCmdHandler = _authFactory.CreateCommandHandler<ChangeEmailForUserCmd>();
            try
            {
                await changeEmailCmdHandler.Execute(new ChangeEmailForUserCmd
                {
                    NewEmail = request.NewEmail,
                    UserId = request.UserId
                });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public async Task ConfirmChangeEmail(DtoConfirmChangeEmailRequest request)
        {
            var confirmEmailCmdHandler = _authFactory.CreateCommandHandler<ConfirmEmailForUserCmd>(); 
            try
            {
                await confirmEmailCmdHandler.Execute(new ConfirmEmailForUserCmd
                {
                    UserId = request.UserId,
                    Token = request.Token
                });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public async Task<DtoUserConfirmRegistrationResult> ConfirmEmail(DtoUserConfirmRegistrationRequest request)
        {
            var confirmEmailCmdHandler = _authFactory.CreateCommandHandler<ConfirmEmailForUserCmd>();
            var updateUserIdentityCmdHandler = _authFactory.CreateCommandHandler<UpdateUserIdentityCmd>();
            try
            {
                await confirmEmailCmdHandler.Execute(new ConfirmEmailForUserCmd
                {
                    UserId = request.UserId,
                    Token = request.Token
                });

                await updateUserIdentityCmdHandler.Execute(new UpdateUserIdentityCmd
                {
                    UserId = request.UserId,
                    Address = request.Address,
                    Login = request.Login,
                    Password = request.Password,
                    Phone = request.Phone,
                    IsAgreed = request.IsAgreed
                });

            }
            catch (Exception ex) when (ex is ConfirmEmailException || ex is UserDoesNotExistException || ex is UserDoesNotCanUpdatedException)
            {
                _logger.ErrorWithIncomeParameter(ex, request);
                return new DtoUserConfirmRegistrationResult
                {
                    IsSuccess = false,
                    ErrorMessage = "Ошибка регистрации пользователя: " + ex.Message
                };
            }
            return new DtoUserConfirmRegistrationResult
            {
                IsSuccess = true,
            };
        }

        public async Task RestorePassword(RestorePasswordCmd restoreParams)
        {
            try
            {
                await _authFactory.CreateCommandHandler<RestorePasswordCmd>().Execute(restoreParams);
            }
            catch (Exception ex) when (ex is UserDoesNotExistException || ex is UserDoesNotCanUpdatedException)
            {
                _logger.ErrorWithIncomeParameter(ex, restoreParams);
                throw;
            }
            catch (Exception ex)
            {
                _logger.ErrorWithIncomeParameter(ex, restoreParams);
                throw;
            }

        }
    }
}
