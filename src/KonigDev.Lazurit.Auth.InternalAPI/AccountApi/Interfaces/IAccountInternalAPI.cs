﻿using System.Threading.Tasks;
using KonigDev.Lazurit.Auth.InternalAPI.DTO;
using KonigDev.Lazurit.Auth.Model.DTO.Users;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Commands;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Queries;

namespace KonigDev.Lazurit.Auth.InternalAPI.AccountApi.Interfaces
{
    public interface IAccountInternalAPI
    {
        Task<DTOErrorResult> RecoveryPasswordAsync(string email);
        Task<DTOConfirmationTokenResult> CreateUser(string password, CreateUserDto userDto);
        Task<DtoConfirmationTokenChangeEmailResult> GetTokenForChangeEmail(ChangeEmailDto dto);
        Task ChangeEmail(DtoChangeEmailRequest request);
        Task ConfirmChangeEmail(DtoConfirmChangeEmailRequest request);
        Task<DtoUserConfirmRegistrationResult> ConfirmEmail(DtoUserConfirmRegistrationRequest request);
        Task RestorePassword(RestorePasswordCmd restoreParams);
    }
}
