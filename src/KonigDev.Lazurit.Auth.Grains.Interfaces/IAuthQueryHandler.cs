﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Orleans;

namespace KonigDev.Lazurit.Auth.Grains.Interfaces
{
    public interface IAuthQueryHandler<TQuery, TResult> : IGrainWithGuidKey
    {
        Task<TResult> Execute(TQuery query);
    }
}
