﻿using System.Threading.Tasks;
using Orleans;

namespace KonigDev.Lazurit.Auth.Grains.Interfaces
{
    public interface IAuthCommandHandler<TCommand> : IGrainWithGuidKey
    {
        Task Execute(TCommand command);
    }
}
