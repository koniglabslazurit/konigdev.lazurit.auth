﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Auth.Grains.Interfaces
{
    /// <summary>
    /// Универсальая фабрика для создания запросов.
    /// Должна быть реализована в CompositionRoot(web api проект, главный проект сайта и т.д.)
    /// </summary>
    public interface IAuthHandlersFactory
    {
        IAuthQueryHandler<TQuery, TResult> CreateQueryHandler<TQuery, TResult>();
        IAuthCommandHandler<TCommand> CreateCommandHandler<TCommand>();
    }
}
