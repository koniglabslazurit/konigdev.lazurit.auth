﻿using System;
using KonigDev.Lazurit.Auth.Grains.Factories;
using KonigDev.Lazurit.Auth.Grains.Services.Implementations;
using KonigDev.Lazurit.Auth.Grains.Services.Interfaces;
using KonigDev.Lazurit.Auth.Model.Context.DataContext;
using KonigDev.Lazurit.Auth.Model.Context.IdentityEntities;
using KonigDev.Lazurit.Auth.Model.Context.Managers;
using KonigDev.Lazurit.Auth.Model.Mappings.User.Implementations;
using KonigDev.Lazurit.Auth.Model.Mappings.User.Interface;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Extensions.DependencyInjection;

namespace KonigDev.Lazurit.Auth.Grains
{
    public class AppStartup
    {
        //public IServiceProvider ConfigureServices(IServiceCollection services)
        //{
        //    services.AddTransient<ICryptoService, CryptoService>();
        //    services.AddTransient<ISettingsProvider, DefaultSettingsProvider>();
        //    services.AddTransient<IUserMapper, UserMapper>();
        //    services.AddTransient<IRoleMapper, RoleMapper>();
        //    services.AddTransient<IDBContextFactory, DBContextFactory>();

        //    services.AddSingleton<IApplicationUserManager, ApplicationUserManager>(x=> new ApplicationUserManager(new UserStore<ApplicationUser>(x.GetService<IDBContextFactory>().CreateAuthIdentityDbContext())));
        //    services.AddSingleton<IApplicationRoleManager, ApplicationRoleManager>(x=> new ApplicationRoleManager(new RoleStore<IdentityRole>(x.GetService<IDBContextFactory>().CreateAuthIdentityDbContext())));
        //    return services.BuildServiceProvider();
        //}
    }
}
