﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Auth.Grains.Extensions
{
    public static class IEnumerableExtension
    {
        public static string ToErrorString(this IEnumerable<string> errors)
        {
            var strBuilder = new StringBuilder();
            foreach (var error in errors)
                strBuilder.AppendLine(error);
            return strBuilder.ToString();
        }
    }
}
