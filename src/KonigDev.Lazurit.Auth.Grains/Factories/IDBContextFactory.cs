﻿using System.Data;
using System.Data.Entity;
using KonigDev.Lazurit.Auth.Model.Context.DataContext;

namespace KonigDev.Lazurit.Auth.Grains.Factories
{
    public interface IDBContextFactory
    {
        /// <summary>
        /// Контекст Бд для работы с набором таблиц для профиля
        /// </summary>
        /// <returns></returns>
        DbContext CreateLazuritContext();
        AuthIdentityDbContext CreateAuthIdentityDbContext();
        IDbConnection CreateIdentityConnection();

    }
}
