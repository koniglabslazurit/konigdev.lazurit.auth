﻿using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using KonigDev.Lazurit.Auth.Model.Context.DataContext;
using KonigDev.Lazurit.Auth.Model.ProfileContext.DataContext;

namespace KonigDev.Lazurit.Auth.Grains.Factories
{
    public class DBContextFactory: IDBContextFactory
    {
        /// <summary>
        /// Контекст Бд для работы с набором таблиц для профиля
        /// </summary>
        /// <returns></returns>
        public DbContext CreateLazuritContext()
        {
            return new LazuritProfileContext();
        }

        public AuthIdentityDbContext CreateAuthIdentityDbContext()
        {
            return new AuthIdentityDbContext();
        }

        public IDbConnection CreateIdentityConnection()
        {
            return new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        }
    }
}
