﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Auth.Grains.Interfaces;
using KonigDev.Lazurit.Auth.Model.Context.Managers;
using KonigDev.Lazurit.Auth.Model.DTO.Users;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Queries;
using KonigDev.Lazurit.Auth.Model.Mappings.User.Interface;
using Orleans;

namespace KonigDev.Lazurit.Auth.Grains.User.QueryHandlers
{
    public class AllUsersQueryHandler : Grain, IAuthQueryHandler<AllUsersQuery, List<UserDto>>
    {
        private readonly IApplicationUserManager _userManager;
        private readonly IUserMapper _userMapper;

        public AllUsersQueryHandler(IApplicationUserManager userManager, IUserMapper userMapper)
        {
            _userManager = userManager;
            _userMapper = userMapper;
        }

        public async Task<List<UserDto>> Execute(AllUsersQuery query)
        {
            var users = _userManager.Users.ToList();
            return await Task.Run(()=>users.Select(x => _userMapper.MapToDTO(x)).ToList());
        }
    }
}
