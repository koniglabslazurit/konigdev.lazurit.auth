﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using KonigDev.Lazurit.Auth.Grains.Factories;
using KonigDev.Lazurit.Auth.Grains.Interfaces;
using KonigDev.Lazurit.Auth.Model.Context.IdentityEntities;
using KonigDev.Lazurit.Auth.Model.Context.Managers;
using KonigDev.Lazurit.Auth.Model.DTO.Users;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Queries;
using KonigDev.Lazurit.Auth.Model.Mappings.User.Interface;
using Orleans;

namespace KonigDev.Lazurit.Auth.Grains.User.QueryHandlers
{
    public class UserByPhoneAndPassQueryHandler : Grain, IAuthQueryHandler<UserByPhoneAndPassQuery, UserDto>
    {
        private readonly IApplicationUserManager _userManager;
        private readonly IDBContextFactory _contextFactory;
        private readonly IUserMapper _userMapper;

        public UserByPhoneAndPassQueryHandler(IApplicationUserManager userManager, IDBContextFactory contextFactory, IUserMapper userMapper)
        {
            _userManager = userManager;
            _contextFactory = contextFactory;
            _userMapper = userMapper;
        }

        public async Task<UserDto> Execute(UserByPhoneAndPassQuery query)
        {
            ApplicationUser identityUser;
            //находим юзера по телефону для того что бы узнать его UserName. После этого мы можем проверить, пароль по которому нам надо искать пользователя подходит для этого пользователя или нет.
            using (var db = _contextFactory.CreateIdentityConnection())
            {
                identityUser = db.Query<ApplicationUser>($"Select * From AspNetUsers where PhoneNumber='{query.UserPhone}'").ToList().Single();
            }
            if (identityUser == null)
                return null;

            var userWithConfirmetPassword = await _userManager.FindAsync(identityUser.UserName, query.Password);
            if (userWithConfirmetPassword == null)
                return null;

            var userDto = _userMapper.MapToDTO(userWithConfirmetPassword);
            userDto.ClaimsIdentity = await _userManager.CreateIdentityAsync(userWithConfirmetPassword, query.AuthentificationType);
            return userDto;
        }
    }
}
