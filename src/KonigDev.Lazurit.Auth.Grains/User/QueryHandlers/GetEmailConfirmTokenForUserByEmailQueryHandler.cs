﻿using KonigDev.Lazurit.Auth.Grains.Interfaces;
using KonigDev.Lazurit.Auth.Model.Context.Managers;
using KonigDev.Lazurit.Auth.Model.DTO.Users;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Exceptions;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Queries;
using Orleans;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Auth.Grains.User.QueryHandlers
{
    public class GetEmailConfirmTokenForUserByEmailQueryHandler : Grain, IAuthQueryHandler<GetEmailConfirmTokenForUserByEmailQuery, ConfirmationTokenForUserDto>
    {
        private readonly IApplicationUserManager _userManager;

        public GetEmailConfirmTokenForUserByEmailQueryHandler(IApplicationUserManager userManager)
        {
            _userManager = userManager;
        }

        public async Task<ConfirmationTokenForUserDto> Execute(GetEmailConfirmTokenForUserByEmailQuery query)
        {
            var user = await _userManager.FindByEmailAsync(query.Email);
            if (user == null)
                throw new UserDoesNotExistException("Пользователь не существует");

            var token = await _userManager.GenerateEmailConfirmationTokenAsync(user.Id);
            return new ConfirmationTokenForUserDto
            {
                UserId = user.Id,
                Token = token
            };
        }
    }
}
