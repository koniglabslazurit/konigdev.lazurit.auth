﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KonigDev.Lazurit.Auth.Grains.Interfaces;
using KonigDev.Lazurit.Auth.Model.Context.Managers;
using KonigDev.Lazurit.Auth.Model.DTO.Users;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Queries;
using KonigDev.Lazurit.Auth.Model.Mappings.User.Interface;
using Microsoft.AspNet.Identity;
using Orleans;

namespace KonigDev.Lazurit.Auth.Grains.User.QueryHandlers
{
    public class UserByIDAndPassQueryHandler : Grain, IAuthQueryHandler<UserByIDAndPassQuery, UserDto>
    {
        private readonly IApplicationUserManager _userManager;
        private readonly IUserMapper _userMapper;

        public UserByIDAndPassQueryHandler(IApplicationUserManager userManager, IUserMapper userMapper)
        {
            _userManager = userManager;
            _userMapper = userMapper;
        }

        public async Task<UserDto> Execute(UserByIDAndPassQuery query)
        {
            //находим юзера по id для того что бы узнать его UserName. После этого мы можем проверить, пароль по которому нам надо искать пользователя подходит для этого пользователя или нет.
            var user = await _userManager.FindByIdAsync(query.UserId);
            if (user == null)
                return null;

            var userWithConfirmetPassword = await _userManager.FindAsync(user.UserName, query.Password);
            if (userWithConfirmetPassword == null)
                return null;

            var userDto = _userMapper.MapToDTO(userWithConfirmetPassword);

            if (await _userManager.IsInRoleAsync(user.Id, Model.DTO.Constants.SallerRoleName))
                userDto.ClaimsIdentity = await _userManager.CreateIdentityAsync(userWithConfirmetPassword, query.AuthentificationType);
            else
            {
                if (userWithConfirmetPassword.EmailConfirmed)
                    userDto.ClaimsIdentity = await _userManager.CreateIdentityAsync(userWithConfirmetPassword, query.AuthentificationType);
            }
            return userDto;
        }
    }
}
