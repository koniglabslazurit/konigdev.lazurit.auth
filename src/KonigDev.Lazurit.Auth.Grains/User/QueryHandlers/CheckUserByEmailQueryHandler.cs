﻿using KonigDev.Lazurit.Auth.Grains.Interfaces;
using KonigDev.Lazurit.Auth.Model.Context.Managers;
using KonigDev.Lazurit.Auth.Model.DTO.Users;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Exceptions;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Queries;
using Orleans;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Auth.Grains.User.QueryHandlers
{
    public class CheckUserByEmailQueryHandler : Grain, IAuthQueryHandler<CheckUserByEmailQuery, CheckUserByEmailDtoResult>
    {
        private readonly IApplicationUserManager _userManager;

        public CheckUserByEmailQueryHandler(IApplicationUserManager userManager)
        {
            _userManager = userManager;
        }

        public async Task<CheckUserByEmailDtoResult> Execute(CheckUserByEmailQuery query)
        {
            var check = await _userManager.FindByEmailAsync(query.Email);
            if (check != null)
                throw new UserWithThisEmailExist("Пользователь с таким e-mail уже существует");

            return new CheckUserByEmailDtoResult
            {
                IsUserExist = true
            };
        }
    }
}
