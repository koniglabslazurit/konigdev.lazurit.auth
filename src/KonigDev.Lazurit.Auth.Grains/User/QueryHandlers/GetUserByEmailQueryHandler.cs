﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using KonigDev.Lazurit.Auth.Grains.Factories;
using KonigDev.Lazurit.Auth.Grains.Interfaces;
using KonigDev.Lazurit.Auth.Model.Context.IdentityEntities;
using KonigDev.Lazurit.Auth.Model.Context.Managers;
using KonigDev.Lazurit.Auth.Model.DTO.Users;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Exceptions;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Queries;
using KonigDev.Lazurit.Auth.Model.Mappings.User.Interface;
using KonigDev.Lazurit.Auth.Model.ProfileContext.Entities;
using Orleans;

namespace KonigDev.Lazurit.Auth.Grains.User.QueryHandlers
{
    public class GetUserByEmailQueryHandler : Grain, IAuthQueryHandler<GetUserByEmailQuery, UserDto>
    {
        private readonly IApplicationUserManager _userManager;
        private readonly IUserMapper _userMapper;
        private readonly IDBContextFactory _contextFactory;

        public GetUserByEmailQueryHandler(IApplicationUserManager userManager, IUserMapper userMapper, IDBContextFactory contextFactory)
        {
            _userManager = userManager;
            _userMapper = userMapper;
            _contextFactory = contextFactory;
        }

        public Task<UserDto> Execute(GetUserByEmailQuery query)
        {
            if (string.IsNullOrEmpty(query.Email))
                throw new NotSupportedException("Невозможно найти пользователя по пустому Email.");
            ApplicationUser identityUser;
            using (IDbConnection db = _contextFactory.CreateIdentityConnection())
            {
                identityUser = db.Query<ApplicationUser>($"Select * From AspNetUsers where Email='{query.Email}' or UserName='{query.Email}'").ToList().SingleOrDefault(); /* выбираем одного или дефолтного */
            }
            if (identityUser != null)
                return Task.FromResult(_userMapper.MapToDTO(identityUser));

            else
            {
                UserDto result = null;
                return Task.FromResult(result);
            }
            //TODO изменить, если надо будет что бы Юзер возвращал роли.
            //ApplicationUser identityUser;
            //string qur = $"SELECT *  FROM [Lazurit].[dbo].[AspNetUsers]  as u left join[dbo].[AspNetUserRoles] as ur on ur.UserId = u.Id inner join[dbo].[AspNetRoles] as r on r.Id = ur.RoleId where u.Email = '{query.Email}'";

        }
    }
}
