﻿using System.Threading.Tasks;
using KonigDev.Lazurit.Auth.Grains.Interfaces;
using KonigDev.Lazurit.Auth.Model.Context.Managers;
using KonigDev.Lazurit.Auth.Model.DTO.Users;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Queries;
using KonigDev.Lazurit.Auth.Model.Mappings.User.Interface;
using Orleans;

namespace KonigDev.Lazurit.Auth.Grains.User.QueryHandlers
{
    public class GetRoleQueryHandler : Grain, IAuthQueryHandler<GetRoleQuery, RoleDto>
    {
        private readonly IApplicationRoleManager _roleManager;
        private readonly IRoleMapper _roleMapper;

        public GetRoleQueryHandler(IApplicationRoleManager roleManager, IRoleMapper roleMapper)
        {
            _roleManager = roleManager;
            _roleMapper = roleMapper;
        }

        public async Task<RoleDto> Execute(GetRoleQuery query)
        {
            var identityRole = await _roleManager.FindByIdAsync(query.RoleId);
            return _roleMapper.MapToDTO(identityRole);
        }
    }
}
