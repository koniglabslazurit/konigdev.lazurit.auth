﻿using Dapper;
using KonigDev.Lazurit.Auth.Grains.Factories;
using KonigDev.Lazurit.Auth.Grains.Interfaces;
using KonigDev.Lazurit.Auth.Model.Context.IdentityEntities;
using KonigDev.Lazurit.Auth.Model.Context.Managers;
using KonigDev.Lazurit.Auth.Model.DTO.Users;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Queries;
using KonigDev.Lazurit.Auth.Model.Mappings.User.Interface;
using Orleans;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Auth.Grains.User.QueryHandlers
{
    [Serializable]
    public class GetUserByPhoneQueryHandler : Grain, IAuthQueryHandler<GetUserByPhoneQuery, UserDto>
    {
        private readonly IApplicationUserManager _userManager;
        private readonly IUserMapper _userMapper;
        private readonly IDBContextFactory _contextFactory;

        public GetUserByPhoneQueryHandler(IApplicationUserManager userManager, IUserMapper userMapper, IDBContextFactory contextFactory)
        {
            _userManager = userManager;
            _userMapper = userMapper;
            _contextFactory = contextFactory;
        }

        public Task<UserDto> Execute(GetUserByPhoneQuery query)
        {
            ApplicationUser identityUser;            
            using (var db = _contextFactory.CreateIdentityConnection())
            {
                identityUser = db.Query<ApplicationUser>($"Select * From AspNetUsers where PhoneNumber='{query.UserPhone}'").ToList().SingleOrDefault();
            }
            if (identityUser == null)
            {
                UserDto result = null;
                return Task.FromResult(result);
            }
            return Task.FromResult(_userMapper.MapToDTO(identityUser));
        }
    }
}
