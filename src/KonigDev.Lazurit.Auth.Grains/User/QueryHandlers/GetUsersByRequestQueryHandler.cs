﻿using System;
using Dapper;
using KonigDev.Lazurit.Auth.Grains.Factories;
using KonigDev.Lazurit.Auth.Grains.Interfaces;
using KonigDev.Lazurit.Auth.Model.DTO.Common;
using KonigDev.Lazurit.Auth.Model.DTO.Common.Exceptions;
using KonigDev.Lazurit.Auth.Model.DTO.Common.ExceptionsMessages;
using KonigDev.Lazurit.Auth.Model.DTO.Users;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Exceptions;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Queries;
using Orleans;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Auth.Model.ProfileContext.Entities;
using System.Collections.Generic;
using KonigDev.Lazurit.Auth.Model.Context.Managers;

namespace KonigDev.Lazurit.Auth.Grains.User.QueryHandlers
{
    public class GetUsersByRequestQueryHandler : Grain, IAuthQueryHandler<GetUsersByRequestQuery, DtoCommonTableResponse<DtoUserItem>>
    {
        private readonly IDBContextFactory _contextFactory;

        public GetUsersByRequestQueryHandler(IDBContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }
        public Task<DtoCommonTableResponse<DtoUserItem>> Execute(GetUsersByRequestQuery query)
        {
            if (query == null)
                throw new QueryNullException(ExceptionsMessage.Common.ModelIsNull);
            if (query.Roles.Count == 0)
                throw new UserRolesIsEmpty(ExceptionsMessage.Users.UserRolesIsEmpty);

            List<UserDto> profileUsers = new List<UserDto>();
            using (var profileContext = _contextFactory.CreateLazuritContext())
            {
                var profileQuery = profileContext.Set<UserProfile>().AsQueryable();
                if (!string.IsNullOrEmpty(query.Search))
                {
                    //сюда добавлять поиск по имени по необходимости 
                    profileQuery = profileQuery.Where(p => p.Email.ToLower().Contains(query.Search.ToLower()));
                    //sqlString = sqlString + $" AND AspNetUsers.Email LIKE '%{query.Search}%'";
                }
                profileUsers = profileQuery
                    .Where(p => !string.IsNullOrEmpty(p.Email))
                    .Select(p => new UserDto
                    {
                        Id = p.Id,
                        UserName = p.Title
                    }).ToList();
            }
            var sqlString = @"SELECT AspNetUserRoles.UserId, AspNetUsers.Email, AspNetRoles.Name as Role
                                FROM AspNetUserRoles 
                                inner JOIN AspNetUsers ON AspNetUserRoles.UserId=AspNetUsers.Id 
                                inner JOIN AspNetRoles ON AspNetUserRoles.RoleId=AspNetRoles.Id 
                                WHERE AspNetRoles.Name IN @Roles AND AspNetUsers.Email<>''";

            List<DtoUserItem> users = new List<DtoUserItem>();
            using (IDbConnection db = _contextFactory.CreateIdentityConnection())
            {
                users = db.Query<DtoUserItem>(sqlString, query).ToList();
            }

            /* map two lists. Use select and map? */
            List<DtoUserItem> usersResult = new List<DtoUserItem>();
            foreach (var item in users)
            {
                var user = profileUsers.FirstOrDefault(p => p.Id == item.UserId);
                if (user != null)
                {
                    item.Name = user.UserName;
                    usersResult.Add(item);
                }                    
            }

            usersResult = usersResult.OrderBy(p => p.Name).ToList();
            DtoCommonTableResponse<DtoUserItem> result = new DtoCommonTableResponse<DtoUserItem>();
            result.TotalCount = usersResult.Count;

            if (query.Page != 0)
            {
                usersResult.Skip((query.Page - 1) * query.ItemsOnPage).Take(query.ItemsOnPage);
                //сюда добавить пользовательскую сортировку когда она будет
                //sqlString = sqlString + $" order by AspNetUsers.JoinDate offset CONVERT(INT,'{skipCount}') Rows fetch next CONVERT(INT,'{query.ItemsOnPage}') Rows only";
            }

            result.Items = usersResult;
            return Task.FromResult(result);
        }
    }
}
