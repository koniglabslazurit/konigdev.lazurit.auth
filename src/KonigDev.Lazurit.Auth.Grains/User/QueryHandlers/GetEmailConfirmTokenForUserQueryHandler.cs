﻿using System.Threading.Tasks;
using KonigDev.Lazurit.Auth.Grains.Interfaces;
using KonigDev.Lazurit.Auth.Model.Context.Managers;
using KonigDev.Lazurit.Auth.Model.DTO.Users;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Exceptions;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Queries;
using Orleans;

namespace KonigDev.Lazurit.Auth.Grains.User.QueryHandlers
{
    public class GetEmailConfirmTokenForUserQueryHandler : Grain, IAuthQueryHandler<GetEmailConfirmTokenForUserQuery, ConfirmationTokenForUserDto>
    {
        private readonly IApplicationUserManager _userManager;

        public GetEmailConfirmTokenForUserQueryHandler(IApplicationUserManager userManager)
        {
            _userManager = userManager;
        }

        public async Task<ConfirmationTokenForUserDto> Execute(GetEmailConfirmTokenForUserQuery query)
        {
            var user = await _userManager.FindAsync(query.UserName, query.Password);
            if (user == null)
                throw new UserDoesNotExistException("Пользователь не существует");

            var token = await _userManager.GenerateEmailConfirmationTokenAsync(user.Id);
            return new ConfirmationTokenForUserDto
            {
                UserId = user.Id,
                Token = token
            };
        }
    }
}
