﻿using System.Threading.Tasks;
using KonigDev.Lazurit.Auth.Grains.Interfaces;
using KonigDev.Lazurit.Auth.Model.Context.Managers;
using KonigDev.Lazurit.Auth.Model.DTO.Users;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Exceptions;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Queries;
using KonigDev.Lazurit.Auth.Model.Mappings.User.Interface;
using Orleans;

namespace KonigDev.Lazurit.Auth.Grains.User.QueryHandlers
{
    public class UserByLoginAndPassQueryHandler : Grain, IAuthQueryHandler<UserByLoginAndPassQuery, UserDto>
    {
        private readonly IApplicationUserManager _userManager;
        private readonly IUserMapper _userMapper;

        public UserByLoginAndPassQueryHandler(IApplicationUserManager userManager, IUserMapper userMapper)
        {
            _userManager = userManager;
            _userMapper = userMapper;
        }

        public async Task<UserDto> Execute(UserByLoginAndPassQuery query)
        {
           
            var user = await _userManager.FindAsync(query.UserName, query.Password);

            if (user == null)
                return null;
            var userDto = _userMapper.MapToDTO(user);

            if (user.EmailConfirmed)
                userDto.ClaimsIdentity = await _userManager.CreateIdentityAsync(user, query.AuthentificationType);

            return userDto;
        }
    }
}
