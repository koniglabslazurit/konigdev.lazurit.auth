﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KonigDev.Lazurit.Auth.Grains.Interfaces;
using KonigDev.Lazurit.Auth.Model.Context.Managers;
using KonigDev.Lazurit.Auth.Model.DTO.Users;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Queries;
using KonigDev.Lazurit.Auth.Model.Mappings.User.Interface;
using Orleans;

namespace KonigDev.Lazurit.Auth.Grains.User.QueryHandlers
{
    public class GetUserByIdQueryHandler : Grain, IAuthQueryHandler<GetUserByIdQuery, UserDto>
    {
        private readonly IApplicationUserManager _userManager;
        private readonly IUserMapper _userMapper;

        public GetUserByIdQueryHandler(IApplicationUserManager userManager, IUserMapper userMapper)
        {
            _userManager = userManager;
            _userMapper = userMapper;
        }

        public async Task<UserDto> Execute(GetUserByIdQuery query)
        {
            var appUser = await _userManager.FindByIdAsync(query.Id);
            if (appUser == null)
            {
                return null;
            }
            return _userMapper.MapToDTO(appUser);
        }
    }
}
