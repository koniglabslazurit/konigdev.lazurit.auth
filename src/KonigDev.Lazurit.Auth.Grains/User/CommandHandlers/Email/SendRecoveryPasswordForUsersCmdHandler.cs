﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using KonigDev.Lazurit.Auth.Grains.Extensions;
using KonigDev.Lazurit.Auth.Grains.Factories;
using KonigDev.Lazurit.Auth.Grains.Interfaces;
using KonigDev.Lazurit.Auth.Grains.Services.Implementations;
using KonigDev.Lazurit.Auth.Grains.Services.Interfaces;
using KonigDev.Lazurit.Auth.Model.Context.Filters;
using KonigDev.Lazurit.Auth.Model.Context.IdentityEntities;
using KonigDev.Lazurit.Auth.Model.Context.Managers;
using KonigDev.Lazurit.Auth.Model.DTO.Users;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Commands.Email;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Exceptions;
using KonigDev.Lazurit.Core.Services;
using KonigDev.Lazurit.Core.Transactions;
using KonigDev.Lazurit.Auth.Model.ProfileContext.Entities;
using Orleans;

namespace KonigDev.Lazurit.Auth.Grains.User.CommandHandlers.Email
{
    //TODO прикрутить логирование,SRP
    public class SendRecoveryPasswordForUsersCmdHandler : Grain, IAuthCommandHandler<SendRecoveryPasswordForUsersCmd>
    {
        private readonly IApplicationRoleManager _roleManager;
        private readonly IApplicationUserManager _userManager;
        private readonly IDBContextFactory _contextFactory;
        private readonly ICryptoService _cryptoService;
        private readonly IEmailTemplateRenderingService _emailTemplateRenderingService;
        private readonly ITransactionFactory _transactionFactory;

        public SendRecoveryPasswordForUsersCmdHandler(IApplicationRoleManager roleManager, IApplicationUserManager userManager,
            IDBContextFactory contextFactory, ICryptoService cryptoService, IEmailTemplateRenderingService emailTemplateRenderingService,
            ITransactionFactory transactionFactory)
        {
            _roleManager = roleManager;
            _userManager = userManager;
            _contextFactory = contextFactory;
            _cryptoService = cryptoService;
            _emailTemplateRenderingService = emailTemplateRenderingService;
            _transactionFactory = transactionFactory;
        }

        public async Task Execute(SendRecoveryPasswordForUsersCmd command)
        {
            if (command.Users == null) return;

            foreach (UserDto user in command.Users)
            {
                var identityUser = await _userManager.FindByIdAsync(user.Id);
                if (identityUser == null)
                    throw new UserDoesNotExistException("Пользователь с такими параметрами не существует.");
                List<ApplicationRole> identityRoles = await GetIdentityRoles(identityUser.Roles.ToList());
                if (!identityRoles.Any()) throw new RoleDoesNotExistException("Пользователь не имеет роли.");

                if (identityRoles.Count > 1)
                {
                    if (await RecoveryPasswordsForRoles(identityUser, identityRoles)) return;
                    //Для пользователя с ролью регион нельзя восстановить пароль
                    else throw new EmailNotSendException("Для данного пользователя нельзя восстановить пароль.");
                }
                else
                {
                    if (await RecoveryPasswordForRole(identityUser, identityRoles[0])) return;
                    //Для пользователя с ролью регион нельзя восстановить пароль
                    else throw new EmailNotSendException("Для данного пользователя нельзя восстановить пароль.");
                }
            }
        }

        private async Task<bool> RecoveryPasswordForRole(ApplicationUser identityUser, ApplicationRole identityRole)
        {
            if (identityRole.IsSalonSaller() || identityRole.IsShopSaller() || identityRole.IsSaller())
            {
                await UpdatePasswordForSellers(identityUser);
                return true;
            }
            if (identityRole.IsAdmin() || identityRole.IsContentManager() || identityRole.IsMarketer() || identityRole.IsBuyer())
            {
                await UpdatePasswordForClients(identityUser);
                return true;
            }
            return false;
        }

        private async Task<bool> RecoveryPasswordsForRoles(ApplicationUser identityUser, List<ApplicationRole> roles)
        {
            if (roles.Any(x => x.IsSalonSaller() || x.IsShopSaller() || x.IsSaller()))
            {
                await UpdatePasswordForSellers(identityUser);
                return true;
            }
            if (roles.Any(x => x.IsAdmin() || x.IsContentManager() || x.IsMarketer() || x.IsBuyer()))
            {
                await UpdatePasswordForClients(identityUser);
                return true;
            }
            return false;
        }

        private async Task<List<ApplicationRole>> GetIdentityRoles(List<ApplicationUserRole> userRoles)
        {
            List<ApplicationRole> roles = new List<ApplicationRole>();
            foreach (var role in userRoles)
            {
                var identityRole = await _roleManager.FindByIdAsync(role.RoleId);

                if (identityRole == null)
                    throw new RoleDoesNotExistException($"Роль {role} не существует.");
                roles.Add(identityRole);
            }
            return roles;
        }

        private async Task UpdatePasswordForSellers(ApplicationUser identityUser)
        {
            using (var db = _contextFactory.CreateLazuritContext())
            {
                var userProfile = db.Set<UserProfile>().Single(x => x.Id == identityUser.Id);
                var regionPasswords = new Dictionary<string, string>();
                foreach (var region in userProfile.Regions)
                {
                    var pass = _cryptoService.DecryptStringAES(region.LazuritPasswordHash, Constants.SharedSecret);
                    var regionIdentity = await _userManager.FindByIdAsync(region.Id);
                    regionPasswords.Add(regionIdentity.UserName, pass);
                }

                if (!regionPasswords.Any())
                    throw new SellerDoesNotContainRegionException($"Продавец с логином {identityUser.UserName} не содержит не одного региона.");

                await _userManager.SendEmailAsync(identityUser.Id, Constants.RecoveryPasswordSubject,
                    _emailTemplateRenderingService.GetBodyForSallers(identityUser.UserName, regionPasswords));
            }
        }

        private async Task UpdatePasswordForClients(ApplicationUser identityUser)
        {
            var newPassword = PasswordGenerator.GenerateRandomString(Constants.MinPasswordLength + 1);

            var newPasswordHash = _userManager.PasswordHasher.HashPassword(newPassword);
            identityUser.PasswordHash = newPasswordHash;

            using (var transaction = _transactionFactory.CreateScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                var updateResult = await _userManager.UpdateAsync(identityUser);
                if (!updateResult.Succeeded)
                    throw new Exception(updateResult.Errors.ToErrorString());
                await _userManager.SendEmailAsync(identityUser.Id, Constants.RecoveryPasswordSubject,
                    _emailTemplateRenderingService.GetBodyClients(identityUser.UserName, newPassword));
                transaction.Complete();
            }
        }
    }
}
