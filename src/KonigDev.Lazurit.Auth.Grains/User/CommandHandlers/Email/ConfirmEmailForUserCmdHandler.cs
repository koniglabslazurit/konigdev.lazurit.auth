﻿using System.Threading.Tasks;
using KonigDev.Lazurit.Auth.Grains.Extensions;
using KonigDev.Lazurit.Auth.Grains.Interfaces;
using KonigDev.Lazurit.Auth.Model.Context.Managers;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Commands.Email;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Exceptions;
using Orleans;

namespace KonigDev.Lazurit.Auth.Grains.User.CommandHandlers.Email
{
    public class ConfirmEmailForUserCmdHandler : Grain, IAuthCommandHandler<ConfirmEmailForUserCmd>
    {
        private readonly IApplicationUserManager _userManager;

        public ConfirmEmailForUserCmdHandler(IApplicationUserManager userManager)
        {
            _userManager = userManager;
        }

        public async Task Execute(ConfirmEmailForUserCmd command)
        {
            var res = await _userManager.ConfirmEmailAsync(command.UserId, command.Token);
            if (!res.Succeeded)
                throw new ConfirmEmailException(res.Errors.ToErrorString());
        }
    }
}
