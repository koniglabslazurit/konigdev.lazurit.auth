﻿using KonigDev.Lazurit.Auth.Grains.Factories;
using KonigDev.Lazurit.Auth.Grains.Interfaces;
using KonigDev.Lazurit.Auth.Model.Context.Managers;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Commands.Email;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Exceptions;
using KonigDev.Lazurit.Auth.Model.ProfileContext.Entities;
using KonigDev.Lazurit.Core.Transactions;
using Orleans;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace KonigDev.Lazurit.Auth.Grains.User.CommandHandlers.Email
{
    public class ChangeEmailForUserCmdHandler : Grain, IAuthCommandHandler<ChangeEmailForUserCmd>
    {
        private readonly IApplicationUserManager _userManager;
        private readonly ITransactionFactory _transactionFactory;
        private readonly IDBContextFactory _contextFactory;

        public ChangeEmailForUserCmdHandler(IApplicationUserManager userManager, ITransactionFactory transactionFactory, IDBContextFactory contextFactory)
        {
            _userManager = userManager;
            _transactionFactory = transactionFactory;
            _contextFactory = contextFactory;
        }

        public async Task Execute(ChangeEmailForUserCmd command)
        {
            //!!!TODO разобраться с транзакцией на разные БД, включить!
            using (var transaction = _transactionFactory.CreateScope(TransactionScopeAsyncFlowOption.Enabled))
            {                
                var user = await _userManager.FindByIdAsync(command.UserId);
                if (user != null)
                {
                    if(user.Email == user.UserName)
                    {
                        user.UserName = command.NewEmail;
                    }
                    user.Email = command.NewEmail;
                    user.EmailConfirmed = false;
                    await _userManager.UpdateAsync(user);
                    using (var context = _contextFactory.CreateLazuritContext())
                    {
                        var contextUser = context.Set<UserProfile>().FirstOrDefault(p => p.Id == command.UserId);
                        if (contextUser != null)
                        {
                            contextUser.Email = command.NewEmail;
                        }
                        context.SaveChanges();
                    }
                }
                else
                {
                    throw new UserDoesNotExistException("Пользователь не найден");
                }               
                transaction.Complete();
            }
        }
    }
}
