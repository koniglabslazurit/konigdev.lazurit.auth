using System.Threading.Tasks;
using KonigDev.Lazurit.Auth.Grains.Interfaces;
using KonigDev.Lazurit.Auth.Model.Context.Managers;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Commands.Email;
using Orleans;

namespace KonigDev.Lazurit.Auth.Grains.User.CommandHandlers.Email
{
    public class SendEmailToUserCmdHandler:Grain, IAuthCommandHandler<SendEmailToUserCmd>
    {
        private readonly IApplicationUserManager _userManager;

        public SendEmailToUserCmdHandler(IApplicationUserManager userManager)
        {
            _userManager = userManager;
        }

        public async Task Execute(SendEmailToUserCmd command)
        {
            await _userManager.SendEmailAsync(command.UserId,command.Subject, command.Body);
        }
    }
}
