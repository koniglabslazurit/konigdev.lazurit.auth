﻿using KonigDev.Lazurit.Auth.Grains.Factories;
using KonigDev.Lazurit.Auth.Grains.Interfaces;
using KonigDev.Lazurit.Auth.Model.Context.IdentityEntities;
using KonigDev.Lazurit.Auth.Model.Context.Managers;
using KonigDev.Lazurit.Auth.Model.DTO.Common.Exceptions;
using KonigDev.Lazurit.Auth.Model.DTO.Common.ExceptionsMessages;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Commands;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Exceptions;
using KonigDev.Lazurit.Auth.Model.Mappings.User.Interface;
using KonigDev.Lazurit.Core.Transactions;
using KonigDev.Lazurit.Auth.Model.ProfileContext.Entities;
using Orleans;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace KonigDev.Lazurit.Auth.Grains.User.CommandHandlers
{
    public class DeleteUserCmdHandler : Grain, IAuthCommandHandler<DeleteUserCmd>
    {
        private readonly IApplicationUserManager _userManager;
        private readonly IDBContextFactory _contextFactory;
        private readonly IUserMapper _userMapper;
        private readonly ITransactionFactory _transactionFactory;

        public DeleteUserCmdHandler(IApplicationUserManager userManager, IDBContextFactory contextFactory, IUserMapper userMapper, ITransactionFactory transactionFactory)
        {
            _userManager = userManager;
            _contextFactory = contextFactory;
            _userMapper = userMapper;
            _transactionFactory = transactionFactory;
        }

        public async Task Execute(DeleteUserCmd command)
        {
            if (command == null || command.UserId == null || command.UserId == Guid.Empty)
                throw new QueryNullException(ExceptionsMessage.Common.ModelIsNull);

            using (var transaction = _transactionFactory.CreateScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (var context = _contextFactory.CreateLazuritContext())
                {
                    var isOrderExist = context.Set<Order>().Any(p => p.UserProfileId == command.UserId);
                    if (isOrderExist)
                        throw new UserCanNotBeDeletedException(ExceptionsMessage.Users.UserHasOrder);

                    ApplicationUser user = _userManager.FindByIdAsync(command.UserId).Result;
                    var deleteResult = _userManager.DeleteAsync(user).Result;
                    if (!deleteResult.Succeeded)
                        throw new UserCanNotBeDeletedException(ExceptionsMessage.Users.UserCanNotBeDeleted);

                    var userProfile = context.Set<UserProfile>().FirstOrDefault(p => p.Id == command.UserId);

                    if (userProfile == null)
                        throw new UserDoesNotExistException(ExceptionsMessage.Users.UserCanNotBeFound);
                    context.Set<UserProfile>().Remove(userProfile);
                    await context.SaveChangesAsync();
                }
                transaction.Complete();
            }
        }
    }
}
