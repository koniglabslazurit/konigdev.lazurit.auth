﻿using System.Threading.Tasks;
using KonigDev.Lazurit.Auth.Grains.Interfaces;
using KonigDev.Lazurit.Auth.Model.Context.Managers;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Commands;
using KonigDev.Lazurit.Auth.Model.Mappings.User.Interface;
using KonigDev.Lazurit.Auth.Model.ProfileContext.Entities;
using Orleans;
using System;
using System.Linq;
using System.Transactions;
using KonigDev.Lazurit.Auth.Grains.Extensions;
using KonigDev.Lazurit.Auth.Grains.Factories;
using KonigDev.Lazurit.Auth.Model.Context.IdentityEntities;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Exceptions;
using KonigDev.Lazurit.Core.Transactions;
using DomainConstants = KonigDev.Lazurit.Auth.Model.DTO.Constants;

namespace KonigDev.Lazurit.Auth.Grains.User.CommandHandlers
{
    public class CreateUserCmdHandler : Grain, IAuthCommandHandler<CreateUserCmd>
    {
        private readonly IApplicationUserManager _userManager;
        private readonly IUserMapper _userMapper;
        private readonly IDBContextFactory _contextFactory;
        private readonly ITransactionFactory _transactionFactory;

        public CreateUserCmdHandler(IApplicationUserManager userManager, IUserMapper userMapper, IDBContextFactory contextFactory, ITransactionFactory transactionFactory)
        {
            _userManager = userManager;
            _userMapper = userMapper;
            _contextFactory = contextFactory;
            _transactionFactory = transactionFactory;
        }

        public async Task Execute(CreateUserCmd command)
        {
            if (!DomainConstants.AllRoles.Contains(command.User.RoleName))
                throw new RoleDoesNotExistException($"Роль {command.User.RoleName} не существует.");

            var applicationUser = _userMapper.MapToApplicationUser(command.User);
            /* todo генерируем уникальный идентификатор для айдентити */
            applicationUser.Id = Guid.NewGuid();

            //!!!TODO разобраться с транзакцией на разные БД, включить!
            using (var transaction = _transactionFactory.CreateScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                var res = await _userManager.CreateAsync(applicationUser, command.Password);
                if (!res.Succeeded)
                    throw new UserDoesNotCreateException(res.Errors.ToErrorString());
                UserProfile user = _userMapper.MapToEntityUser(command.User, applicationUser.Id);
                //id for creating url for sharing Favorite page 
                user.LinkId = Guid.NewGuid();
                try
                {
                    using (var context = _contextFactory.CreateLazuritContext())
                    {
                        if (user.UserPhones != null)
                            foreach (var phone in user.UserPhones)
                                context.Set<UserPhone>().Add(phone);
                        if (user.UserAddresses != null)
                            foreach (var adress in user.UserAddresses)
                                context.Set<UserAddress>().Add(adress);
                        context.Set<UserProfile>().Add(user);
                        context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                   // await _userManager.DeleteAsync(applicationUser); /* если произошла ошибка при создании профайла, удаляем идентити */
                    throw ex;
                }

                //!!!TODO разобраться с транзакцией на разные БД
                transaction.Complete();
            }
            ApplicationUser appUser = await _userManager.FindByNameAsync(applicationUser.UserName);
            await _userManager.AddToRoleAsync(appUser.Id, command.User.RoleName);
        }
    }
}
