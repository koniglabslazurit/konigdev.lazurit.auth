﻿using System.Linq;
using KonigDev.Lazurit.Auth.Grains.Factories;
using KonigDev.Lazurit.Auth.Grains.Interfaces;
using KonigDev.Lazurit.Auth.Model.Context.IdentityEntities;
using KonigDev.Lazurit.Auth.Model.Context.Managers;
using KonigDev.Lazurit.Auth.Model.DTO.Common.Exceptions;
using KonigDev.Lazurit.Auth.Model.DTO.Common.ExceptionsMessages;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Commands.Roles;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Exceptions;
using Orleans;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Auth.Grains.User.CommandHandlers.Roles
{
    public class UpdateUserRoleCmdHandler : Grain, IAuthCommandHandler<UpdateUserRoleCmd>
    {
        private readonly IApplicationUserManager _userManager;
        private readonly IDBContextFactory _contextFactory;
        public UpdateUserRoleCmdHandler(IApplicationUserManager userManager, IDBContextFactory contextFactory)
        {
            _userManager = userManager;
            _contextFactory = contextFactory;
        }
        public async Task Execute(UpdateUserRoleCmd command)
        {
            if (command == null)
                throw new QueryNullException(ExceptionsMessage.Common.ModelIsNull);
            if (string.IsNullOrEmpty(command.Role))
                throw new UserRolesIsEmpty(ExceptionsMessage.Users.UserRolesIsEmpty);
            ApplicationUser user = await _userManager.FindByIdAsync(command.UserId);
            //удалить старые роли
            var roles = user.Roles.ToList();
            var length = roles.Count;
            for (var i = 0; i < length; i++)
            {
                user.Roles.Remove(roles[i]);
            }
            //сохранить изменения юзера
            var updateResult = _userManager.UpdateAsync(user).Result;
            //назначить новую роль
            var addResult = await _userManager.AddToRoleAsync(command.UserId, command.Role);
            /* TODO Добавить проверку на резалт */            
        }
    }
}
