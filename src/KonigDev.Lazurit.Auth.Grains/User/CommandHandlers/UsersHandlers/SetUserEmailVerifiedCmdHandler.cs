﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using KonigDev.Lazurit.Auth.Grains.Interfaces;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Commands;
using KonigDev.Lazurit.Auth.Grains.Factories;
using KonigDev.Lazurit.Auth.Model.Context.Managers;
using KonigDev.Lazurit.Auth.Model.Mappings.User.Interface;
using KonigDev.Lazurit.Core.Transactions;
using KonigDev.Lazurit.Auth.Model.ProfileContext.Entities;

namespace KonigDev.Lazurit.Auth.Grains.User.CommandHandlers.UsersHandlers
{
    public class SetUserEmailVerifiedCmdHandler : IAuthCommandHandler<SetUserEmailVerifiedCmd>
    {
        private readonly IApplicationUserManager _userManager;
        private readonly ITransactionFactory _transactionFactory;
        private readonly IDBContextFactory _contextFactory;

        public SetUserEmailVerifiedCmdHandler(IDBContextFactory contextFactory, IApplicationUserManager userManager, ITransactionFactory transactionFactory)
        {
            _contextFactory = contextFactory;
            _userManager = userManager;
            _transactionFactory = transactionFactory;
        }

        public async Task Execute(SetUserEmailVerifiedCmd command)
        {
            var appUser = await _userManager.FindByIdAsync(command.UserId);
            if (appUser.EmailConfirmed)
            {
                throw new Exception("Пользователь уже был верифицирован");
            }
            appUser.Email = command.Email;
            using (var transaction = _transactionFactory.CreateScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (var context = _contextFactory.CreateLazuritContext())
                {
                    var userProfile = context.Set<UserProfile>().FirstOrDefault(p => p.Id == command.UserId);
                    if (userProfile != null)
                    {
                        userProfile.Email = command.Email;
                    }
                    await context.SaveChangesAsync();
                }
                appUser.EmailConfirmed = true;
                await _userManager.UpdateAsync(appUser);
                transaction.Complete();
            }
        }
    }
}
