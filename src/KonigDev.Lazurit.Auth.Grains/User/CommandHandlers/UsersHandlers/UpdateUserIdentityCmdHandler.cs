﻿using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using KonigDev.Lazurit.Auth.Grains.Factories;
using KonigDev.Lazurit.Auth.Grains.Interfaces;
using KonigDev.Lazurit.Auth.Model.Context.Managers;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Commands;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Exceptions;
using KonigDev.Lazurit.Core.Transactions;
using KonigDev.Lazurit.Auth.Model.ProfileContext.Entities;
using System;

namespace KonigDev.Lazurit.Auth.Grains.User.CommandHandlers.UsersHandlers
{
    public class UpdateUserIdentityCmdHandler : IAuthCommandHandler<UpdateUserIdentityCmd>
    {
        private readonly IApplicationUserManager _userManager;
        private readonly IDBContextFactory _contextFactory;
        private readonly ITransactionFactory _transactionFactory;

        public UpdateUserIdentityCmdHandler(IApplicationUserManager userManager, IDBContextFactory contextFactory, ITransactionFactory transactionFactory)
        {
            _userManager = userManager;
            _contextFactory = contextFactory;
            _transactionFactory = transactionFactory;
        }

        public async Task Execute(UpdateUserIdentityCmd command)
        {
            var appUser = await _userManager.FindByIdAsync(command.UserId);
            if (appUser == null)
                throw new UserDoesNotExistException("Пользователь не найден");

            var newPasswordHash = _userManager.PasswordHasher.HashPassword(command.Password);
            appUser.PasswordHash = newPasswordHash;
            appUser.UserName = command.Login;

            using (var transaction = _transactionFactory.CreateScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                var updateResult = await _userManager.UpdateAsync(appUser);
                if (!updateResult.Succeeded)
                {
                    throw new UserDoesNotCanUpdatedException("Ошибка обнолвления пользователя");
                }
                using (var context = _contextFactory.CreateLazuritContext())
                {
                    var userId = command.UserId;
                    var userProfile = context.Set<UserProfile>().FirstOrDefault(p => p.Id == userId);
                    if (userProfile == null)
                        throw new UserDoesNotExistException("Профиль пользователя не найден");
                    if (!string.IsNullOrWhiteSpace(command.Address))
                    {
                        context.Set<UserAddress>().Add(new UserAddress
                        {
                            Id = Guid.NewGuid(),
                            Address = command.Address,
                            IsDefault = true,
                            UserProfileId = userId
                        });
                    }
                    if (!string.IsNullOrWhiteSpace(command.Phone) && !userProfile.UserPhones.Where(p => p.Phone == command.Phone).Any())
                    {
                        context.Set<UserPhone>().Add(new UserPhone
                        {
                            Id = Guid.NewGuid(),
                            IsDefault = !userProfile.UserPhones.Any(),
                            Phone = command.Phone,
                            UserProfileId = userId
                        });
                    }
                    await context.SaveChangesAsync();
                }
                transaction.Complete();
            }
        }
    }
}
