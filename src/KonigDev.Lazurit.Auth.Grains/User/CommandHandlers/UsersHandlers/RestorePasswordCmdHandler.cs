﻿using System.Threading.Tasks;
using KonigDev.Lazurit.Auth.Grains.Interfaces;
using KonigDev.Lazurit.Auth.Model.Context.Managers;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Commands;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Exceptions;

namespace KonigDev.Lazurit.Auth.Grains.User.CommandHandlers.UsersHandlers
{
    public class RestorePasswordCmdHandler : IAuthCommandHandler<RestorePasswordCmd>
    {
        private readonly IApplicationUserManager _userManager;

        public RestorePasswordCmdHandler(IApplicationUserManager userManager)
        {
            _userManager = userManager;
        }

        public async Task Execute(RestorePasswordCmd command)
        {
            var appUser = await _userManager.FindByIdAsync(command.UserId);
            if (appUser == null)
                throw new UserDoesNotExistException(Constants.UserNotFoundException);
            //проверяем,правильный ли пароль пришёл в параметрах
            var sureUser = await _userManager.FindAsync(appUser.UserName, command.OldPassword);
            if (sureUser == null)
                throw new UserDoesNotExistException(Constants.UserWithPassNotFoundException);

            var newPasswordHash = _userManager.PasswordHasher.HashPassword(command.NewPassword);
            appUser.PasswordHash = newPasswordHash;
            var updateResult = await _userManager.UpdateAsync(appUser);
            if (!updateResult.Succeeded)
                throw new UserDoesNotCanUpdatedException(Constants.ErrorRestoreUserPassException);
        }
    }
}
