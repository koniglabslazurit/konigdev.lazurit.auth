﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Auth.Grains
{
    public class Constants
    {
        public const string RecoveryPasswordSubject = "Восстановление пароля.";
        public const byte MinPasswordLength = 6;
        public const string SharedSecret = "Lazurit2016";
        public const string UserNotFoundException = "Пользователь не найден";
        public const string UserWithPassNotFoundException = "Пользователь c таким паролем не найден";
        public const string ErrorRestoreUserPassException = "Ошибка сброса пароля у пользователя";
    }
}
