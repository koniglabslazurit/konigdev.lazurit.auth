﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KonigDev.Lazurit.Core.Services;

namespace KonigDev.Lazurit.Auth.Grains.Services.Implementations
{
    public class EmailTemplateRenderingService : IEmailTemplateRenderingService
    {
        public string GetBodyForSallers(string userName, Dictionary<string, string> regionPasswords)
        {
            if (regionPasswords.Count > 1)
            {
                var strBuilder = new StringBuilder();
                strBuilder.AppendLine($"<h2>Ваш логин:{userName}</h2>");
                foreach (var regionPassword in regionPasswords)
                {
                    strBuilder.AppendLine($"<h2>Ваш пароль на регион {regionPassword.Key}: {regionPassword.Value}</h2>");
                }
                return strBuilder.ToString();
            }
            else
            {
                var regionItem = regionPasswords.First();
                return $"<h2>Ваш логин:{userName} Ваш пароль на регион {regionItem.Key}: {regionItem.Value}</h2>";
            }
        }

        public string GetBodyClients(string userName, string password)
        {
            return $"<h2>Ваш логин:{userName} Ваш новый пароль:{password}</h2>";
        }
    }
}
