﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using KonigDev.Lazurit.Auth.Grains.Services.Interfaces;
using Microsoft.AspNet.Identity;
using KonigDev.Lazurit.Auth.Model.ProfileContext.Constants;

namespace KonigDev.Lazurit.Auth.Grains.Services.Implementations
{
    public class EmailService : IIdentityMessageService
    {
        private readonly ISettingsProvider _settingsProvider;

        public EmailService(ISettingsProvider settingsProvider)
        {
            _settingsProvider = settingsProvider;
        }

        public async Task SendAsync(IdentityMessage message)
        {
            MailMessage mail = new MailMessage(_settingsProvider.GetValue(LazuritBaseDomainConstants.FromMailName), message.Destination);
            SmtpClient smtpClient = new SmtpClient(_settingsProvider.GetValue(LazuritBaseDomainConstants.SmtpHostName),Convert.ToInt32(_settingsProvider.GetValue(LazuritBaseDomainConstants.SmtpPortName)))
            {
                Credentials =new NetworkCredential(_settingsProvider.GetValue(LazuritBaseDomainConstants.SmtpUserCredentName),_settingsProvider.GetValue(LazuritBaseDomainConstants.SmtpPasswordCredentName)),
                EnableSsl = true
            };
            mail.Subject = message.Subject;
            mail.BodyEncoding = Encoding.UTF8;
            mail.Body = message.Body;
            mail.IsBodyHtml = true;
            mail.SubjectEncoding = Encoding.UTF8;
            await smtpClient.SendMailAsync(mail);
        }
    }
}
