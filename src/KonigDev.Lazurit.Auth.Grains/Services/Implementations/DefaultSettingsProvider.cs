﻿using System.Linq;
using KonigDev.Lazurit.Auth.Grains.Factories;
using KonigDev.Lazurit.Auth.Grains.Services.Interfaces;
using KonigDev.Lazurit.Auth.Model.ProfileContext.Entities;

namespace KonigDev.Lazurit.Auth.Grains.Services.Implementations
{
    public class DefaultSettingsProvider: ISettingsProvider
    {
        private readonly IDBContextFactory _dbContextFactory;

        public DefaultSettingsProvider(IDBContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public string GetValue(string name)
        {
            using (var db = _dbContextFactory.CreateLazuritContext())
            {
                var s = db.Set<Settings>().FirstOrDefault(setting => setting.Name == name);
                if (s == null)
                {
                    db.Set<Settings>().Add(new Settings
                    {
                        Name = name,
                        Title = name,
                        Value = null
                    });
                    db.SaveChanges();
                }
                return s?.Value;
            }
        }

        public void SetValue(string name, string value)
        {
            using (var db = _dbContextFactory.CreateLazuritContext())
            {
                var s = db.Set<Settings>().FirstOrDefault(setting => setting.Name == name);
                if (s == null)
                {
                    s = new Settings { Name = name };
                    db.Set<Settings>().Add(s);
                }
                s.Value = value;
                db.SaveChanges();
            }
        }
    }
}
