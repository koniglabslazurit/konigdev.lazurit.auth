﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Auth.Grains.Services.Interfaces
{
    public interface ICryptoService
    {
        string DecryptStringAES(string cipherText, string sharedSecret);
        string EncryptStringAES(string plainText, string sharedSecret);
    }
}
