﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Auth.Grains.Services.Interfaces
{
    public interface ISettingsProvider
    {
        string GetValue(string name);
        void SetValue(string name, string value);
    }
}
