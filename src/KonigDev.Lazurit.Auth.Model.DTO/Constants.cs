﻿using System.Collections.Generic;

namespace KonigDev.Lazurit.Auth.Model.DTO
{
    public static class Constants
    {
        public const string AdminPassword = "Lazurit2016!";
        public const string AdminEmail = "kobyb@mail.ru";
        public const string AdminUserName = "admin";

        #region Role Names
        public const string AdminRoleName = "admin";
        public const string ContentManagerRoleName = "contentManager";
        public const string MarketerRoleName = "marketer";
        public const string BuyerRoleName = "buyer";
        public const string RegionRoleName = "region";
        public const string SalonSallerRoleName = "salonSaller";
        public const string ShopSallerRoleName = "shopSaller";
        public const string SallerRoleName = "saller";
        /// <summary>
        /// Роль анонимного пользователя: anonym
        /// </summary>
        public const string AnonymRoleName = "anonym";

        public static List<string> AllRoles = new List<string>
        {
            AdminRoleName,
            ContentManagerRoleName,
            MarketerRoleName,
            BuyerRoleName,
            RegionRoleName,
            SalonSallerRoleName,
            ShopSallerRoleName,
            SallerRoleName,
            AnonymRoleName
        };
        #endregion
    }
}
