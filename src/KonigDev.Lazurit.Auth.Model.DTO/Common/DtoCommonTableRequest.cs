﻿using System;

namespace KonigDev.Lazurit.Auth.Model.DTO.Common
{
    [Serializable]
    public class DtoCommonTableRequest
    {
        public int Page { get; set; }
        public int ItemsOnPage { get; set; }
        public string Sort { get; set; }
        public string SortColumn { get; set; }
        public string Search { get; set; }
    }
}
