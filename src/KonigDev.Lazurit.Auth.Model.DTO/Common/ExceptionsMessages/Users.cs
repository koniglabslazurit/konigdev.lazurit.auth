﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Auth.Model.DTO.Common.ExceptionsMessages
{
    public static partial class ExceptionsMessage
    {
        public class Users
        {
            public const string UserRolesIsEmpty = "Роли пользователя не заданы";
            public const string UserCanNotBeDeleted = "Пользователь не может быть удален";
            public const string UserCanNotBeFound = "Пользователь не может быть найден";
            public const string UserHasOrder = "У данного пользователя имеются заказы";
        }
    }
}
