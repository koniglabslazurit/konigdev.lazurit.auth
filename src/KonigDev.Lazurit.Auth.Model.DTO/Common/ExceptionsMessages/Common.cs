﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Auth.Model.DTO.Common.ExceptionsMessages
{
    public static partial class ExceptionsMessage
    {
        public class Common
        {
            public const string ModelIsNull = "Пустая модель";
        }
    }
}
