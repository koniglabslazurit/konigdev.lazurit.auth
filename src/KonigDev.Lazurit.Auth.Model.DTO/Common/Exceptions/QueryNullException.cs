﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Auth.Model.DTO.Common.Exceptions
{
    public class QueryNullException: Exception
    {
        public QueryNullException(string message): base(message)
        {
        }
    }
}
