﻿using System.Collections.Generic;

namespace KonigDev.Lazurit.Auth.Model.DTO
{
    public class IdentityResultDTO
    {
        public IdentityResultDTO(bool succeeded, IEnumerable<string> errors)
        {
            Succeeded = succeeded;
            Errors = errors;
        }

        public bool Succeeded { get; private set; }

       
        public IEnumerable<string> Errors { get; private set; }
    }
}
