﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Auth.Model.DTO.Users
{
    public class CheckUserByEmailDtoResult
    {
        public bool IsUserExist { get; set; }
    }
}
