﻿using System;

namespace KonigDev.Lazurit.Auth.Model.DTO.Users
{
    [Serializable]
    public class DtoUserItem
    {        
        public Guid UserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
    }
}
