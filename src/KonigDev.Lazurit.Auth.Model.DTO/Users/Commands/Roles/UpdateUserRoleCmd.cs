﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Auth.Model.DTO.Users.Commands.Roles
{
    [Serializable]
    [DataContract]
    public class UpdateUserRoleCmd
    {
        public Guid UserId { get; set; }
        public string Role { get; set; }
    }
}
