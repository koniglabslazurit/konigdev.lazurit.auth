﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Auth.Model.DTO.Users.Commands
{
    [Serializable]
    [DataContract]

    public class RestorePasswordCmd
    {
        public Guid UserId { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
}
