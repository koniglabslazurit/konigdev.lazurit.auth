﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Auth.Model.DTO.Users.Commands
{
    public class SetUserEmailVerifiedCmd
    {
        public Guid UserId { get; set; }
        public string Email { get; set; }
    }
}
