﻿using System;

namespace KonigDev.Lazurit.Auth.Model.DTO.Users.Commands
{
    public class UpdateUserIdentityCmd
    {
        public Guid UserId { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Phone { get; set; }
        public bool IsAgreed { get; set; }
        public string Address { get; set; }
    }
}
