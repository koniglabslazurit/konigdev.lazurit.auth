﻿using System;
using System.Runtime.Serialization;

namespace KonigDev.Lazurit.Auth.Model.DTO.Users.Commands
{
    [Serializable]
    [DataContract]
    public class DeleteUserCmd
    {
        public Guid UserId { get; set; }
    }
}
