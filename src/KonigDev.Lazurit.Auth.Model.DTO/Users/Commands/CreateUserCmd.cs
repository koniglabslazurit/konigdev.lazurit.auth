﻿using System;
using System.Runtime.Serialization;

namespace KonigDev.Lazurit.Auth.Model.DTO.Users.Commands
{
    [Serializable]
    [DataContract]
    public class CreateUserCmd
    {
        public string Password { get; set; }
        public CreateUserDto User { get; set; }
    }
}
