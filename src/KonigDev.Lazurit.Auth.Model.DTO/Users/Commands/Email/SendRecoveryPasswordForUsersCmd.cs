﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace KonigDev.Lazurit.Auth.Model.DTO.Users.Commands.Email
{
    [Serializable]
    [DataContract]
    public class SendRecoveryPasswordForUsersCmd
    {
       
        public List<UserDto> Users { get; set; }
    }
}
