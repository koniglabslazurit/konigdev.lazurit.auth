﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Auth.Model.DTO.Users.Commands.Email
{
    [Serializable]
    public class ChangeEmailForUserCmd
    {
        public Guid UserId { get; set; }
        public string NewEmail { get; set; }
    }
}
