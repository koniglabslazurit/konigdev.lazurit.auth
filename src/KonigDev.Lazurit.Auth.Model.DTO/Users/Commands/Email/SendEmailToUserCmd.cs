﻿using System;
using System.Runtime.Serialization;

namespace KonigDev.Lazurit.Auth.Model.DTO.Users.Commands.Email
{
    [Serializable]
    [DataContract]
    public class SendEmailToUserCmd
    {
        public Guid UserId { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }

    }
}
