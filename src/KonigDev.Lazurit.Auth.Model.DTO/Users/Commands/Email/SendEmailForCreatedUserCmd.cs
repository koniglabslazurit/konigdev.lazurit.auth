﻿using System;

namespace KonigDev.Lazurit.Auth.Model.DTO.Users.Commands.Email
{
    [Serializable]
    public class SendEmailForCreatedUserCmd
    {
        public string Url { get; set; }
    }
}
