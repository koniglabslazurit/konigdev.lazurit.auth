﻿using System;

namespace KonigDev.Lazurit.Auth.Model.DTO.Users.Commands.Email
{
    [Serializable]
    public class ConfirmEmailForUserCmd
    {
        public Guid UserId { get; set; }
        public string Token { get; set; }
    }
}
