﻿using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace KonigDev.Lazurit.Auth.Model.DTO.Users
{
    /// <summary>
    /// Хранит информацию о пользователе
    /// </summary>
    [Serializable]
    public class UserDto
    {
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Email пользователя
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Логин пользователя
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Имя пользователя
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия пользователя
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Адресс проживания
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// Телефонный номер
        /// </summary>
        public string PhoneNumber { get; set; }
        /// <summary>
        /// Дата присоединяния к системе
        /// </summary>
        public DateTime JoinDate { get; set; }
        /// <summary>
        /// True-Email пользователя был подтверждён
        /// </summary>
        public bool EmailConfirmed { get; set; }
        /// <summary>
        /// True-пароль пользователя является временным
        /// </summary>
        public bool IsPasswordTemp { get; set; }

        /// <summary>
        /// True-пользователь заблокирован
        /// </summary>
        public bool LockoutEnabled { get; set; }

        /// <summary>
        /// Список ролей пользователя
        /// </summary>
        public List<UserRoleDto> Roles { get; set; }

        /// <summary>
        /// Права пользователя
        /// </summary>
        public ClaimsIdentity ClaimsIdentity { get; set; }

    }
}
