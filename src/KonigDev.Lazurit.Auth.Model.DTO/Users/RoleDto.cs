﻿using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Auth.Model.DTO.Users
{
    [Serializable]
    public class RoleDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<UserRoleDto> Users;
    }
}
