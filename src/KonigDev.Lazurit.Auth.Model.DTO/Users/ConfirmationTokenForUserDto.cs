﻿using System;

namespace KonigDev.Lazurit.Auth.Model.DTO.Users
{
    [Serializable]
    public class ConfirmationTokenForUserDto
    {
        public string Token { get; set; }
        public Guid UserId { get; set; }
    }
}
