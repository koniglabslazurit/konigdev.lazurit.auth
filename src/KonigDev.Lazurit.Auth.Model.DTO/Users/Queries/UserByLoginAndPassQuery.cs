﻿using System;
using System.Runtime.Serialization;

namespace KonigDev.Lazurit.Auth.Model.DTO.Users.Queries
{
    [Serializable]
    [DataContract]
    public class UserByLoginAndPassQuery
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string AuthentificationType { get; set; }
    }
}
