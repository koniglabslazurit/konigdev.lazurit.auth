﻿using System;
using System.Runtime.Serialization;

namespace KonigDev.Lazurit.Auth.Model.DTO.Users.Queries
{
    [Serializable]
    [DataContract]
    public class UserByPhoneAndPassQuery
    {
        public string UserPhone { get; set; }
        public string Password { get; set; }
        public string AuthentificationType { get; set; }
    }
}
