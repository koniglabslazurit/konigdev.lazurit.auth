﻿using KonigDev.Lazurit.Auth.Model.DTO.Common;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Auth.Model.DTO.Users.Queries
{
    [Serializable]
    public class GetUsersByRequestQuery : DtoCommonTableRequest
    {        
        public List<string> Roles { get; set; }
    }
}