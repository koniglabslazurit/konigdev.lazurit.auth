﻿using System;

namespace KonigDev.Lazurit.Auth.Model.DTO.Users.Queries
{
    [Serializable]
    public class GetUserByIdQuery
    {
        public Guid Id { get; set; }
    }
}
