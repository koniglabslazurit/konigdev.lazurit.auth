﻿using System;

namespace KonigDev.Lazurit.Auth.Model.DTO.Users.Queries
{
    [Serializable]
    public class GetEmailConfirmTokenForUserQuery
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
