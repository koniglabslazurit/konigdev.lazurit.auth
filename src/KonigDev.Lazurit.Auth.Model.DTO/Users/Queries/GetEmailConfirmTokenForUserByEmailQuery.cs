﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Auth.Model.DTO.Users.Queries
{
    [Serializable]
    public class GetEmailConfirmTokenForUserByEmailQuery
    {
        public string Email { get; set; }
    }
}
