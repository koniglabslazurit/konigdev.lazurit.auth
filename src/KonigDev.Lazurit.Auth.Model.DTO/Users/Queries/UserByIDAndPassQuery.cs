﻿using System;
using System.Runtime.Serialization;

namespace KonigDev.Lazurit.Auth.Model.DTO.Users.Queries
{
    [Serializable]
    [DataContract]
    public class UserByIDAndPassQuery
    {
        public string Password { get; set; }
        public Guid UserId { get; set; }
        public string AuthentificationType { get; set; }

    }
}
