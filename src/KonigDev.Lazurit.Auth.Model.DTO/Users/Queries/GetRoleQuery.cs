﻿using System;
using System.Runtime.Serialization;

namespace KonigDev.Lazurit.Auth.Model.DTO.Users.Queries
{
    [Serializable]
    [DataContract]
    public class GetRoleQuery
    {
        public Guid RoleId { get; set; }
    }
}
