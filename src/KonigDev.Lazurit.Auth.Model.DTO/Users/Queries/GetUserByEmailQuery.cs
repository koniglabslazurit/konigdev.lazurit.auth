﻿using System;
using System.Runtime.Serialization;

namespace KonigDev.Lazurit.Auth.Model.DTO.Users.Queries
{
    [Serializable]
    [DataContract]
    public class GetUserByEmailQuery
    {
        public string Email { get; set; }
    }
}
