﻿using System;

namespace KonigDev.Lazurit.Auth.Model.DTO.Users.Queries
{
    [Serializable]
    public class GetUserByPhoneQuery
    {
        public string UserPhone { get; set; }
    }
}
