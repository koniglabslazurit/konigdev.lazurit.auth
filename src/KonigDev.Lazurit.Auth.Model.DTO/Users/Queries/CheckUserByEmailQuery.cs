﻿using System;

namespace KonigDev.Lazurit.Auth.Model.DTO.Users.Queries
{
    [Serializable]
    public class CheckUserByEmailQuery
    {
        public string Email { get; set; }
    }
}
