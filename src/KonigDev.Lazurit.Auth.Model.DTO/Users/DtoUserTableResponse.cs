﻿using KonigDev.Lazurit.Auth.Model.DTO.Common;

namespace KonigDev.Lazurit.Auth.Model.DTO.Users
{
    public class DtoUserTableResponse : DtoCommonTableResponse<DtoUserItem>
    {
    }
}
