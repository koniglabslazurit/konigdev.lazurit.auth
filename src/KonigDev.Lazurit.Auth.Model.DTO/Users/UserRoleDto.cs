﻿using System;

namespace KonigDev.Lazurit.Auth.Model.DTO.Users
{
    /// <summary>
    /// Класс, содержащий связь пользователей и роли
    /// </summary>
    [Serializable]
    public class UserRoleDto
    {
       
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Идентификатор роли
        /// </summary>
        public Guid RoleId { get; set; }
    }
}
