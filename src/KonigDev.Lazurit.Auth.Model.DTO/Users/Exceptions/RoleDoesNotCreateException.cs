﻿using System;

namespace KonigDev.Lazurit.Auth.Model.DTO.Users.Exceptions
{
    public class RoleDoesNotCreateException:Exception
    {
        public RoleDoesNotCreateException(string message):base(message)
        {
        }
    }
}
