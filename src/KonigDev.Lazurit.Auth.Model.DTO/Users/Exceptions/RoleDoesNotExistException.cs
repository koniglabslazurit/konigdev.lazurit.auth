﻿using System;

namespace KonigDev.Lazurit.Auth.Model.DTO.Users.Exceptions
{
    public class RoleDoesNotExistException:Exception
    {
        public RoleDoesNotExistException(string message) : base(message) { }
    }
}
