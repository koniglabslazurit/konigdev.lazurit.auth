﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Auth.Model.DTO.Users.Exceptions
{
    public class UserCanNotBeDeletedException : Exception
    {
        public UserCanNotBeDeletedException(string message) : base(message)
        {

        }
    }
}
