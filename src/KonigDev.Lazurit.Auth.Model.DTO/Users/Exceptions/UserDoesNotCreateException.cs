﻿using System;

namespace KonigDev.Lazurit.Auth.Model.DTO.Users.Exceptions
{
    public class UserDoesNotCreateException:Exception
    {
        public UserDoesNotCreateException(string message) : base(message) { }
    }
}
