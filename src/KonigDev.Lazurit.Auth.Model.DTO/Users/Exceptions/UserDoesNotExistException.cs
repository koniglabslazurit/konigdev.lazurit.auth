﻿using System;

namespace KonigDev.Lazurit.Auth.Model.DTO.Users.Exceptions
{
    public class UserDoesNotExistException:Exception
    {
        public UserDoesNotExistException(string message):base(message)
        {
        }
    }
}
