﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Auth.Model.DTO.Users.Exceptions
{
    public class UserRolesIsEmpty: Exception
    {
        public UserRolesIsEmpty(string message): base(message) { }
    }
}
