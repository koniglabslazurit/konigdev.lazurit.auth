﻿using System;

namespace KonigDev.Lazurit.Auth.Model.DTO.Users.Exceptions
{
    public class ConfirmEmailException : Exception
    {
        public ConfirmEmailException(string message) : base(message) { }
    }
}
