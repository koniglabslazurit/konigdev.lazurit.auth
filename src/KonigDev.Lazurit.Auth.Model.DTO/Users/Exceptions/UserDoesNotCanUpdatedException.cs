﻿using System;

namespace KonigDev.Lazurit.Auth.Model.DTO.Users.Exceptions
{
    public class UserDoesNotCanUpdatedException : Exception
    {
        public UserDoesNotCanUpdatedException(string message) : base(message)
        {
            
        }
    }
}