﻿using System;

namespace KonigDev.Lazurit.Auth.Model.DTO.Users.Exceptions
{
    public class UserWithThisEmailExist : Exception
    {
        public UserWithThisEmailExist(string message):base(message)
        {
        }
    }
}
