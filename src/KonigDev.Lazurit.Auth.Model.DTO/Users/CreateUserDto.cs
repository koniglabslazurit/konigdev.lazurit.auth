﻿using System;

namespace KonigDev.Lazurit.Auth.Model.DTO.Users
{
    [Serializable]
    public class CreateUserDto
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime JoinDate { get; set; }
        public bool EmailConfirmed { get; set; }
        public bool IsPasswordTemp { get; set; }

        public bool LockoutEnabled { get; set; }
        public string RoleName { get; set; } 

    }
}
