﻿using System;
using System.Data.Entity;
using KonigDev.Lazurit.Auth.Model.Context.IdentityEntities;
using Microsoft.AspNet.Identity.EntityFramework;
using KonigDev.Lazurit.Auth.Model.Context.Migrations;

namespace KonigDev.Lazurit.Auth.Model.Context.DataContext
{
    public class AuthIdentityDbContext : IdentityDbContext<ApplicationUser,ApplicationRole,Guid,ApplicationUserLogin,ApplicationUserRole,ApplicationUserClaim>
    {
        public AuthIdentityDbContext() : base("DefaultConnection")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<AuthIdentityDbContext, Configuration>("DefaultConnection"));

            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
        }
    }
}
