﻿using KonigDev.Lazurit.Auth.Model.Context.DataContext;
using KonigDev.Lazurit.Auth.Model.Context.IdentityEntities;
using KonigDev.Lazurit.Auth.Model.Context.Managers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Constants = KonigDev.Lazurit.Auth.Model.DTO.Constants;

namespace KonigDev.Lazurit.Auth.Model.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<AuthIdentityDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DataContext.AuthIdentityDbContext context)
        {
            /* seed roles */
            var needAddedRoles= Constants.AllRoles.Where(x => !context.Roles.Any(r=>r.Name==x)).ToList();
            if (needAddedRoles.Any())
            {
                var store = new ApplicationRoleStore(context);
                var manager = new ApplicationRoleManager(store);
                foreach (var roleName in needAddedRoles)
                {
                    var role = new ApplicationRole { Name = roleName };
                    role.Id = Guid.NewGuid();
                    manager.Create(role);
                }
            } 

            /* seed user admin */
            if (!context.Users.Any(u => u.UserName == Constants.AdminUserName))
            {
                var store = new ApplicationUserStore(context);
                var manager = new UserManager<ApplicationUser,Guid>(store);
                var user = new ApplicationUser
                {
                    /* задаем ид, чтоб была возможность создать профиль для этого же пользователя */
                    Id = Guid.Parse("1c7ff1e3-24b0-4e34-991c-d5930a7a9a2f"),
                    UserName = Constants.AdminUserName,
                    Email = Constants.AdminEmail,
                    EmailConfirmed = true,
                    JoinDate = DateTime.Now
                };
                manager.Create(user, Constants.AdminPassword);
                manager.AddToRole(user.Id, Constants.AdminRoleName);
            }
        }
    }
}
