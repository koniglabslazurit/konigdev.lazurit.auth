namespace KonigDev.Lazurit.Auth.Model.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPasswordTempPrtoperty : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "IsPasswordTemp", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "IsPasswordTemp");
        }
    }
}
