﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KonigDev.Lazurit.Auth.Model.Context.IdentityEntities;
using KonigDev.Lazurit.Auth.Model.DTO;
using Microsoft.AspNet.Identity.EntityFramework;

namespace KonigDev.Lazurit.Auth.Model.Context.Filters
{
    public static class IdentityRoleFilters
    {
        public static bool IsAdmin(this ApplicationRole role)
        {
            return role.Name == Constants.AdminRoleName;
        }
        public static bool IsBuyer(this ApplicationRole role)
        {
            return role.Name == Constants.BuyerRoleName;
        }
        public static bool IsContentManager(this ApplicationRole role)
        {
            return role.Name == Constants.ContentManagerRoleName;
        }
        public static bool IsMarketer(this ApplicationRole role)
        {
            return role.Name == Constants.MarketerRoleName;
        }
        public static bool IsRegion(this ApplicationRole role)
        {
            return role.Name == Constants.RegionRoleName;
        }
        public static bool IsSalonSaller(this ApplicationRole role)
        {
            return role.Name == Constants.SalonSallerRoleName;
        }
        public static bool IsShopSaller(this ApplicationRole role)
        {
            return role.Name == Constants.ShopSallerRoleName;
        }
        public static bool IsSaller(this ApplicationRole role)
        {
            return role.Name == Constants.SallerRoleName;
        }
        public static bool IsAnonym(this ApplicationRole role)
        {
            return role.Name == Constants.AnonymRoleName;
        }
    }
}
