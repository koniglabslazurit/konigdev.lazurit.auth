﻿using System;
using System.ComponentModel.DataAnnotations;
using KonigDev.Lazurit.Auth.Model.Context.DataContext;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace KonigDev.Lazurit.Auth.Model.Context.IdentityEntities
{
    public class ApplicationUser : IdentityUser<Guid, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>
    {
        [Required]
        public DateTime JoinDate { get; set; }

        public bool IsPasswordTemp { get; set; }
    }
    public class ApplicationUserRole : IdentityUserRole<Guid> { }
    public class ApplicationUserClaim : IdentityUserClaim<Guid> { }
    public class ApplicationUserLogin : IdentityUserLogin<Guid> { }
    //public class ApplicationUserPasswordStore : IUserPasswordStore<ApplicationUser, Guid>
    //{ }

    public class ApplicationRole : IdentityRole<Guid, ApplicationUserRole>
    {
        public ApplicationRole() { }
        public ApplicationRole(string name) { Name = name; }
    }

    public class ApplicationUserStore : UserStore<ApplicationUser, ApplicationRole, Guid,
        ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>
    {
        public ApplicationUserStore(AuthIdentityDbContext context)
            : base(context)
        {
        }
    }

    public class ApplicationRoleStore : RoleStore<ApplicationRole, Guid, ApplicationUserRole>
    {
        public ApplicationRoleStore(AuthIdentityDbContext context)
            : base(context)
        {
        }
    }
}
