﻿using System;
using System.Threading.Tasks;
using KonigDev.Lazurit.Auth.Model.Context.IdentityEntities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.DataProtection;

namespace KonigDev.Lazurit.Auth.Model.Context.Managers
{
    public class ApplicationUserManager : UserManager<ApplicationUser, Guid>, IApplicationUserManager
    {
        public ApplicationUserManager(IUserStore<ApplicationUser,Guid> store, IDataProtectionProvider dataProtectionProvider, IIdentityMessageService emailService) : base(store)
        {
            UserValidator = new UserValidator<ApplicationUser, Guid>(this)
            {
                AllowOnlyAlphanumericUserNames = true,
                RequireUniqueEmail = true
            };

            /* todo проверка толко на длину не менее 4 символов */
            PasswordValidator = new PasswordValidator
            {
                RequiredLength = 4,
                RequireNonLetterOrDigit = false,
                RequireDigit = false,
                RequireLowercase = false,
                RequireUppercase = false,
            };
            EmailService = emailService;
            if (dataProtectionProvider != null)
            {
                UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser, Guid>(dataProtectionProvider.Create("ASP.NET Identity"))
                {
                    //Жизненный цикл кода и подтверждения email
                    TokenLifespan = TimeSpan.FromHours(6)
                };
            }
        }

    }
}
