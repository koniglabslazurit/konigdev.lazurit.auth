﻿using System;
using System.Threading.Tasks;
using KonigDev.Lazurit.Auth.Model.Context.IdentityEntities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace KonigDev.Lazurit.Auth.Model.Context.Managers
{
    public class ApplicationRoleManager : RoleManager<ApplicationRole,Guid>, IApplicationRoleManager
    {
        public ApplicationRoleManager(ApplicationRoleStore store) : base(store)
        {}
    }
}
