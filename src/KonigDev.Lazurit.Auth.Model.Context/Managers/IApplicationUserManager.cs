﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using KonigDev.Lazurit.Auth.Model.Context.IdentityEntities;
using Microsoft.AspNet.Identity;

namespace KonigDev.Lazurit.Auth.Model.Context.Managers
{
    public interface IApplicationUserManager
    {
        IQueryable<ApplicationUser> Users { get; }
        Task<ApplicationUser> FindByIdAsync(Guid userId);
        Task<ApplicationUser> FindAsync(string userName, string password);
        Task SendEmailAsync(Guid userId, string subject, string body);
        Task<IdentityResult> CreateAsync(ApplicationUser user);
        Task<IdentityResult> CreateAsync(ApplicationUser user, string password);
        Task<ApplicationUser> FindByNameAsync(string userName);
        Task<IdentityResult> AddToRoleAsync(Guid userId, string role);
        Task<string> GenerateEmailConfirmationTokenAsync(Guid userId);
        Task<IdentityResult> ConfirmEmailAsync(Guid userId, string token);
        Task<ClaimsIdentity> CreateIdentityAsync(ApplicationUser user, string authenticationType);
        Task<ApplicationUser> FindByEmailAsync(string email);
        IPasswordHasher PasswordHasher { get; set; }
        Task<IdentityResult> UpdateAsync(ApplicationUser user);
        Task<IdentityResult> ChangePasswordAsync(Guid userId, string currentPassword, string newPassword);

        //Task<IdentityResult> UpdatePassword(ApplicationUser user, string newPassword);
        Task<IdentityResult> AddPasswordAsync(Guid userId, string password);
        Task<IdentityResult> RemovePasswordAsync(Guid userId);
        Task<bool> IsInRoleAsync(Guid userId, string role);
        Task<IdentityResult> DeleteAsync(ApplicationUser user);
    }
}
