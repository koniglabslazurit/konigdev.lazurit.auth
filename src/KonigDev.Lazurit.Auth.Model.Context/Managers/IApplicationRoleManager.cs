﻿using System;
using System.Threading.Tasks;
using KonigDev.Lazurit.Auth.Model.Context.IdentityEntities;
using Microsoft.AspNet.Identity.EntityFramework;

namespace KonigDev.Lazurit.Auth.Model.Context.Managers
{
    public interface IApplicationRoleManager
    {
        Task<ApplicationRole> FindByIdAsync(Guid roleId);
    }
}
