﻿using System;

namespace KonigDev.Lazurit.Auth.Model.Entities
{
    public class UserToShowRoom
    {
        public Guid UserId { get; set; }
        public Guid ShowRoomId { get; set; }

        public virtual ShowRoom ShowRoom { get; set; }
        public virtual User User { get; set; }
    }
}
