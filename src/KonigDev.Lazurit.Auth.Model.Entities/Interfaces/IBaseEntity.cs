﻿using System;

namespace KonigDev.Lazurit.Auth.Model.Entities.Interfaces
{
    public interface IBaseEntity
    {
        Guid Id { get; set; }
    }
}
