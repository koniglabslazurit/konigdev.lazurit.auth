﻿using System;

namespace KonigDev.Lazurit.Auth.Model.Entities.Interfaces
{

    public interface IBaseEnum
    {
        Guid Id { get; set; }
        string Title { get; set; }
        string TechName { get; set; }
        byte Sort { get; set; }
    }
}
