﻿using KonigDev.Lazurit.Auth.Model.Entities.Interfaces;
using System;

namespace KonigDev.Lazurit.Auth.Model.Entities
{
    public class User : IBaseEntity
    {
        public Guid Id { get; set; }

        public string Title { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }        
        public string Address { get; set; }        
        /// <summary>
        /// Код синхронизации с 1С
        /// </summary>
        public string SyncCode { get; set; }

        public Guid TypeId { get; set; }

        public virtual UserType UserType { get; set; }
    }
}
