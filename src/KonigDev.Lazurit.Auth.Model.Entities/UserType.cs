﻿using KonigDev.Lazurit.Auth.Model.Entities.Interfaces;
using System;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Auth.Model.Entities
{
    public class UserType : IBaseEnum
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string TechName { get; set; }
        public byte Sort { get; set; }
    }
}