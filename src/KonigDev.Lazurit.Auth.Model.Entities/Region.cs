﻿using KonigDev.Lazurit.Auth.Model.Entities.Interfaces;
using System;

namespace KonigDev.Lazurit.Auth.Model.Entities
{
    public class Region : IBaseEntity
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        /// <summary>
        /// Код синхронизации с 1С
        /// </summary>        

        //todo 1c
        public string SyncCode { get; set; }
    }
}
