﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KonigDev.Lazurit.Auth.Grains.Factories;
using KonigDev.Lazurit.Auth.Grains.User.QueryHandlers;
using KonigDev.Lazurit.Auth.Model.DTO.Common;
using KonigDev.Lazurit.Auth.Model.DTO.Users;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Queries;
using NUnit.Framework;
using KonigDev.Lazurit.Auth.Model.Context.Managers;
using System.Data;
using Microsoft.AspNet.Identity;
using KonigDev.Lazurit.Auth.Model.Context.IdentityEntities;
using System;
using Ninject;
using Microsoft.Owin.Security.DataProtection;
using KonigDev.Lazurit.Auth.GrainResolver.NinjectModules;
using KonigDev.Lazurit.Auth.Model.ProfileContext.Entities;
using Ploeh.AutoFixture;

namespace KonigDev.Lazurit.Auth.Grains.IntegratedTests.User.QueryHandlers
{
    [TestFixture]
    public class GetUsersByRequestQueryHandlerIntegrationTests
    {
        private StandardKernel _kernel;
        private IDBContextFactory _contextFactory;
        private GetUsersByRequestQueryHandler _class;

        [SetUp]
        public void SetUp()
        {
            _contextFactory = new DBContextFactory();
            _class = new GetUsersByRequestQueryHandler(_contextFactory);
            _kernel = new StandardKernel();
            _kernel.Bind<IDataProtectionProvider>().ToMethod(x => new DpapiDataProtectionProvider());
            _kernel.Load<CommandQueryModule>();
            _kernel.Load<BaseModule>();
        }

        [Test]
        public async Task GetUsersByRequestQueryHandler_ShouldReturnsUsers_WhenRequestWithRolesOnly()
        {
            //up
            var usersList = new List<TestData>
            {
                new TestData { Email = Guid.NewGuid().ToString().Replace("-","")+ "@mail.ru", Password = "123456789", Role = "buyer", UserName = Guid.NewGuid().ToString().Replace("-","") },
                new TestData { Email = Guid.NewGuid().ToString().Replace("-", "") + "@mail.ru", Password = "123456789", Role = "buyer", UserName = Guid.NewGuid().ToString().Replace("-", "") },
                new TestData { Email = Guid.NewGuid().ToString().Replace("-", "") + "@mail.ru", Password = "123456789", Role = "buyer", UserName = Guid.NewGuid().ToString().Replace("-", "") },
                new TestData { Email = Guid.NewGuid().ToString().Replace("-", "") + "@mail.ru", Password = "123456789", Role = "buyer", UserName = Guid.NewGuid().ToString().Replace("-", "") }
            };

            foreach (var item in usersList)
                await Set(item);

            //arrange
            var query = new GetUsersByRequestQuery
            {
                Roles = new List<string> { "buyer" },
                Page = 0,
                ItemsOnPage = 10
            };
            //act
            var actual = await _class.Execute(query);
            //assert
            Assert.IsInstanceOf<DtoCommonTableResponse<DtoUserItem>>(actual);
            /* todo реализовать очистку бд перед прогоном теста, чтобы получить только тех, кого создали в этом тесте */
            Assert.True(usersList.Select(p => p.UserName).ToList().Contains(usersList[0].UserName));
            Assert.True(usersList.Select(p => p.UserName).ToList().Contains(usersList[1].UserName));
            Assert.True(usersList.Select(p => p.UserName).ToList().Contains(usersList[2].UserName));
            Assert.True(usersList.Select(p => p.UserName).ToList().Contains(usersList[3].UserName));
            //down
            foreach (var item in usersList)
                await Down(item);
        }

        [Test]
        public async Task GetUsersByRequestQueryHandler_ShouldReturnsAlexaUsers_WhenRequestWithRolesAndSearch()
        {
            //up
            var usersList = new List<TestData>
            {
                new TestData { Email = Guid.NewGuid().ToString().Replace("-","")+ "@mail.ru", Password = "123456789", Role = "buyer", UserName = Guid.NewGuid().ToString().Replace("-","") }
            };

            foreach (var item in usersList)
                await Set(item);

            //arrange
            var query = new GetUsersByRequestQuery
            {
                Roles = new List<string> { "buyer" },
                Page = 0,
                ItemsOnPage = 10,
                Search = usersList[0].Email
            };
            //act
            var actual = await _class.Execute(query);
            //assert
            Assert.IsInstanceOf<DtoCommonTableResponse<DtoUserItem>>(actual);
            Assert.AreEqual(1, actual.TotalCount);
            Assert.AreEqual(usersList.First().Email, actual.Items.First().Email);

            //down
            foreach (var item in usersList)
                await Down(item);
        }

        [Test]
        public async Task GetUsersByRequestQueryHandler_ShouldReturnsAlexaUsers_WhenRequestWithRolesAndPaging()
        {
            //up
            var usersList = new List<TestData>
            {
                new TestData { Email = Guid.NewGuid().ToString().Replace("-","")+ "@mail.ru", Password = "123456789", Role = "region", UserName = Guid.NewGuid().ToString().Replace("-","") },
                new TestData { Email = Guid.NewGuid().ToString().Replace("-","")+ "@mail.ru", Password = "123456789", Role = "region", UserName = Guid.NewGuid().ToString().Replace("-","") }
            };
            foreach (var item in usersList)
                await Set(item);

            //arrange
            var query = new GetUsersByRequestQuery
            {
                Roles = new List<string> { "region" },
                Page = 2,
                ItemsOnPage = 2
            };
            //act
            var actual = await _class.Execute(query);
            //assert
            Assert.IsInstanceOf<DtoCommonTableResponse<DtoUserItem>>(actual);
            Assert.AreEqual(2, actual.TotalCount);

            //down
            foreach (var item in usersList)
                await Down(item);
        }

        public async Task Set(TestData data)
        {
            var _userManager = _kernel.Get<IApplicationUserManager>();
            IDBContextFactory _contextFactory = new DBContextFactory();

            var res = await _userManager.CreateAsync(new ApplicationUser { Id = Guid.NewGuid(), Email = data.Email, UserName = data.UserName, JoinDate = DateTime.UtcNow }, data.Password);
            ApplicationUser appUser = await _userManager.FindByNameAsync(data.UserName);
            await _userManager.AddToRoleAsync(appUser.Id, data.Role);
            using (var context = _contextFactory.CreateLazuritContext())
            {
                context.Set<UserProfile>().Add(new UserProfile { Id = appUser.Id, Email = data.Email, LinkId = Guid.NewGuid(), Title = Guid.NewGuid().ToString() });
                context.SaveChanges();
            }
        }

        public async Task Down(TestData data)
        {
            var _userManager = _kernel.Get<IApplicationUserManager>();
            IDBContextFactory _contextFactory = new DBContextFactory();
            ApplicationUser appUser = await _userManager.FindByNameAsync(data.UserName);
            await _userManager.DeleteAsync(appUser);
            using (var context = _contextFactory.CreateLazuritContext())
            {
                var userProfile = context.Set<UserProfile>().FirstOrDefault(p => p.Id == appUser.Id);
                context.Set<UserProfile>().Remove(userProfile);
                context.SaveChanges();
            }
        }

        public class TestData
        {
            public string Email { get; set; }
            public string Password { get; internal set; }
            public string Role { get; set; }
            public string UserName { get; internal set; }
        }
    }
}
