﻿using System;
using System.Globalization;
using System.Threading.Tasks;
using KonigDev.Lazurit.Auth.GrainResolver.NinjectModules;
using KonigDev.Lazurit.Auth.Grains.Factories;
using KonigDev.Lazurit.Auth.Grains.IntegratedTests.CommonModels;
using KonigDev.Lazurit.Auth.Grains.User.CommandHandlers;
using KonigDev.Lazurit.Auth.Model.Context.Managers;
using KonigDev.Lazurit.Auth.Model.DTO.Users;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Commands;
using KonigDev.Lazurit.Auth.Model.Mappings.User.Interface;
using KonigDev.Lazurit.Core.Transactions;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.DataProtection;
using Moq;
using Ninject;
using NUnit.Framework;
using KonigDev.Lazurit.Auth.Model.ProfileContext.Entities;
using System.Linq;

namespace KonigDev.Lazurit.Auth.Grains.IntegratedTests.User.CommandHandlers
{
    [TestFixture]
    public class CreateUserCmdHandlerTests
    {
        private StandardKernel _kernel;


        [SetUp]
        public void Init()
        {
            _kernel = new StandardKernel();
            _kernel.Bind<IDataProtectionProvider>().ToMethod(x => new DpapiDataProtectionProvider());
            _kernel.Load<CommandQueryModule>();
            _kernel.Load<BaseModule>();
        }

        [Test]
        public async Task Execute_CreateLazuritContextThrowTestException_IdentityUserNotCreated()
        {
            //arr
            var rnd = new Random();
            var uniqUserName = rnd.NextDouble().ToString(CultureInfo.InvariantCulture);
            string email = $"{uniqUserName}@test.ru";
            var lazuritContextMock = new Mock<IDBContextFactory>();
            lazuritContextMock.Setup(x => x.CreateLazuritContext()).Throws<TestException>();
            var applicationUserManager = _kernel.Get<IApplicationUserManager>();
            var createUser = new CreateUserCmdHandler(applicationUserManager, _kernel.Get<IUserMapper>(), lazuritContextMock.Object, _kernel.Get<ITransactionFactory>());

            //act
            try
            {
                await createUser.Execute(new CreateUserCmd
                {
                    Password = "123456",
                    User = new CreateUserDto
                    {
                        Email = email,
                        UserName = uniqUserName,
                        RoleName = Model.DTO.Constants.BuyerRoleName
                    }
                });
            }
            catch (TestException)
            {
            }

            //assert
            var identityUser = await applicationUserManager.FindByEmailAsync(email);
            Assert.That(identityUser == null);
        }

        [Test]
        public async Task CreateUserCmdHandler_UserProfileAndIdentitityUser_IdentityUserWasCreated()
        {
            //arr            
            var uniqUserName = Guid.NewGuid().ToString().Replace("-", "");
            string email = $"{uniqUserName}@test.ru";

            var lazuritContext = _kernel.Get<IDBContextFactory>();
            var applicationUserManager = _kernel.Get<IApplicationUserManager>();
            var createUser = new CreateUserCmdHandler(applicationUserManager, _kernel.Get<IUserMapper>(), lazuritContext, _kernel.Get<ITransactionFactory>());

            //act
            var command = new CreateUserCmd
            {
                Password = "123456",
                User = new CreateUserDto
                {
                    Email = email,
                    UserName = uniqUserName,
                    RoleName = Model.DTO.Constants.BuyerRoleName
                }
            };
            await createUser.Execute(command);
            //assert
            var identityUser = await applicationUserManager.FindByEmailAsync(email);
            UserProfile profile;
            using (var context = lazuritContext.CreateLazuritContext())
            {
                profile = context.Set<UserProfile>().FirstOrDefault(p => p.Email == email);
            }

            Assert.That(identityUser != null);
            Assert.That(profile != null);
            Assert.AreEqual(identityUser.Id, profile.Id);

            await applicationUserManager.DeleteAsync(identityUser);
            using (var context = lazuritContext.CreateLazuritContext())
            {
                context.Set<UserProfile>().Attach(profile);
                context.Set<UserProfile>().Remove(profile);
            }
        }
    }
}
