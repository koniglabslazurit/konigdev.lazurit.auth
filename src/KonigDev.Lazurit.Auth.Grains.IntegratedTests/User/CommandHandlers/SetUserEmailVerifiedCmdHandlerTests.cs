﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KonigDev.Lazurit.Auth.GrainResolver.NinjectModules;
using KonigDev.Lazurit.Auth.Grains.Factories;
using KonigDev.Lazurit.Auth.Grains.IntegratedTests.CommonModels;
using KonigDev.Lazurit.Auth.Grains.User.CommandHandlers.UsersHandlers;
using KonigDev.Lazurit.Auth.Model.Context.IdentityEntities;
using KonigDev.Lazurit.Auth.Model.Context.Managers;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Commands;
using KonigDev.Lazurit.Core.Transactions;
using KonigDev.Lazurit.Auth.Model.ProfileContext.Entities;
using Microsoft.Owin.Security.DataProtection;
using Moq;
using Ninject;
using NUnit.Framework;

namespace KonigDev.Lazurit.Auth.Grains.IntegratedTests.User.CommandHandlers
{
    [TestFixture]
    public class SetUserEmailVerifiedCmdHandlerTests
    {
        private StandardKernel _kernel;
        private IApplicationUserManager _applicationUserManager;
        private ApplicationUser _newIdentityUser;

        [SetUp]
        public async Task Init()
        {
            _kernel = new StandardKernel();
            _kernel.Bind<IDataProtectionProvider>().ToMethod(x => new DpapiDataProtectionProvider());
            _kernel.Load<CommandQueryModule>();
            _kernel.Load<BaseModule>();

            await CreateTempUser();
        }

        private async Task CreateTempUser()
        {
            _applicationUserManager = _kernel.Get<IApplicationUserManager>();
            var rnd= new Random();
            _newIdentityUser = new ApplicationUser
            {
                Id = Guid.NewGuid(),
                UserName = rnd.NextDouble().ToString(CultureInfo.InvariantCulture),
                Email = $"{rnd.NextDouble().ToString(CultureInfo.InvariantCulture)}@test.ru",
                PasswordHash = "123456",
                JoinDate = DateTime.Now,
                EmailConfirmed = false
            };
            try
            {
                var createUserRes = await _applicationUserManager.CreateAsync(_newIdentityUser);
                if (!createUserRes.Succeeded)
                    throw new Exception("Не получилось подготовить данные и создать пользователя.");
                using (var context = _kernel.Get<IDBContextFactory>().CreateLazuritContext())
                {
                    var userProfile = new UserProfile()
                    {
                        Id = _newIdentityUser.Id,
                        Email = _newIdentityUser.Email
                    };
                    context.Set<UserProfile>().Add(userProfile);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [Test]
        public async Task Execute_userManagerUpdateAsyncThrowTestException_UserProfileNotUpdated()
        {
            //arr
            var uniqUserEmailForAssert = "CreateLazuritContextThrowTestExceptionIdentityUserNotUpdated2";
            var userManagerMock = new Mock<IApplicationUserManager>();
            userManagerMock.Setup(x => x.UpdateAsync(It.IsAny<ApplicationUser>())).Throws<TestException>();
            
            userManagerMock.Setup(x => x.FindByIdAsync(It.IsAny<Guid>())).ReturnsAsync(_newIdentityUser);
            var updateUser = new SetUserEmailVerifiedCmdHandler(_kernel.Get<IDBContextFactory>(), userManagerMock.Object, _kernel.Get<ITransactionFactory>());

            //act
            try
            {
                await updateUser.Execute(new SetUserEmailVerifiedCmd
                {
                    UserId = _newIdentityUser.Id,
                    Email = uniqUserEmailForAssert
                });
            }
            catch (TestException)
            {
            }

            //assert
            //Обновляем контекст что бы обнаружить изменения
            using (var lazuritContex = _kernel.Get<IDBContextFactory>().CreateLazuritContext())
            {
                var identityUser = lazuritContex.Set<UserProfile>().Single(x => x.Id == _newIdentityUser.Id);
                Assert.That(identityUser.Email != uniqUserEmailForAssert);
            }
        }

        [TearDown]
        public async Task Dispose()
        {
            var user = await _applicationUserManager.FindByIdAsync(_newIdentityUser.Id);
            if (user != null)
            {
                try
                {
                    await _applicationUserManager.DeleteAsync(user);
                    using (var context = _kernel.Get<IDBContextFactory>().CreateLazuritContext())
                    {
                        var userProfile=context.Set<UserProfile>().Single(x => x.Id == user.Id);
                        context.Set<UserProfile>().Remove(userProfile);
                        context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }
    }
}
