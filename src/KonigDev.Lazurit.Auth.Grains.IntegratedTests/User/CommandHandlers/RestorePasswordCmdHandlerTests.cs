﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KonigDev.Lazurit.Auth.GrainResolver.NinjectModules;
using KonigDev.Lazurit.Auth.Grains.Factories;
using KonigDev.Lazurit.Auth.Grains.IntegratedTests.CommonModels;
using KonigDev.Lazurit.Auth.Grains.User.CommandHandlers;
using KonigDev.Lazurit.Auth.Grains.User.CommandHandlers.UsersHandlers;
using KonigDev.Lazurit.Auth.Model.Context.IdentityEntities;
using KonigDev.Lazurit.Auth.Model.Context.Managers;
using KonigDev.Lazurit.Auth.Model.DTO.Users;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Commands;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Exceptions;
using KonigDev.Lazurit.Auth.Model.Mappings.User.Interface;
using KonigDev.Lazurit.Core.Transactions;
using KonigDev.Lazurit.Auth.Model.ProfileContext.Entities;
using Microsoft.Owin.Security.DataProtection;
using Moq;
using Ninject;
using NUnit.Framework;

namespace KonigDev.Lazurit.Auth.Grains.IntegratedTests.User.CommandHandlers
{
    [TestFixture]
    public class RestorePasswordCmdHandlerTests
    {
        private StandardKernel _kernel;
        private IApplicationUserManager _applicationUserManager;
        private ApplicationUser _newIdentityUser;
        private string _uniqUserPassword;

        [SetUp]
        public void Init()
        {
            _kernel = new StandardKernel();
            _kernel.Bind<IDataProtectionProvider>().ToMethod(x => new DpapiDataProtectionProvider());
            _kernel.Load<CommandQueryModule>();
            _kernel.Load<BaseModule>();
            CreateTempUser().Wait();


        }

        private async Task CreateTempUser()
        {
            _applicationUserManager = _kernel.Get<IApplicationUserManager>();
            var rndPass = new Random(1000);
            _uniqUserPassword = rndPass.NextDouble().ToString(CultureInfo.InvariantCulture);
            var rnd = new Random();
            _newIdentityUser = new ApplicationUser
            {
                Id = Guid.NewGuid(),
                UserName = rnd.NextDouble().ToString(CultureInfo.InvariantCulture),
                Email = $"{rnd.NextDouble().ToString(CultureInfo.InvariantCulture)}@test.ru",
                PasswordHash = _applicationUserManager.PasswordHasher.HashPassword(_uniqUserPassword),
                JoinDate = DateTime.Now,
                EmailConfirmed = false
            };
            try
            {
                var createUserRes = await _applicationUserManager.CreateAsync(_newIdentityUser);
                if (!createUserRes.Succeeded)
                    throw new Exception("Не получилось подготовить данные и создать пользователя.");
                using (var context = _kernel.Get<IDBContextFactory>().CreateLazuritContext())
                {
                    var userProfile = new UserProfile()
                    {
                        Id = _newIdentityUser.Id,
                        Email = _newIdentityUser.Email
                    };
                    context.Set<UserProfile>().Add(userProfile);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [Test]
        public async Task Execute_PasswordHashChanged_AllInputParamsIsValid()
        {
            //arr
            var rndPass = new Random(1000);
            var newPassword = rndPass.NextDouble().ToString(CultureInfo.InvariantCulture);
            var applicationUserManager = _kernel.Get<IApplicationUserManager>();
            var restorePass = new RestorePasswordCmdHandler(applicationUserManager);

            //act

            var userWithOLdPassword = await applicationUserManager.FindByIdAsync(_newIdentityUser.Id);
            await restorePass.Execute(new RestorePasswordCmd
            {
                NewPassword = newPassword,
                OldPassword = _uniqUserPassword,
                UserId = _newIdentityUser.Id
            });

            //assert
            var identityUser = await applicationUserManager.FindByIdAsync(_newIdentityUser.Id);
            Assert.IsTrue(_newIdentityUser.PasswordHash != identityUser.PasswordHash);
            Assert.IsTrue(userWithOLdPassword.PasswordHash == identityUser.PasswordHash);
        }

        [Test]
        public void Execute_NewUserId_UserDoesNotExistExceptionWilThrow()
        {
            //arr
            var rndPass = new Random(1000);
            var newPassword = rndPass.NextDouble().ToString(CultureInfo.InvariantCulture);
            var applicationUserManager = _kernel.Get<IApplicationUserManager>();
            var restorePass = new RestorePasswordCmdHandler(applicationUserManager);

            //act

            //assert
            Assert.ThrowsAsync<UserDoesNotExistException>(async ()=>await restorePass.Execute(new RestorePasswordCmd
            {
                NewPassword = newPassword,
                OldPassword = _uniqUserPassword,
                UserId = Guid.NewGuid()
            }));
        }

        [Test]
        public void Execute_OldPasswordNotValid_UserDoesNotExistExceptionWilThrow()
        {
            //arr
            var rndPass = new Random(1000);
            var newPassword = rndPass.NextDouble().ToString(CultureInfo.InvariantCulture);
            var applicationUserManager = _kernel.Get<IApplicationUserManager>();
            var restorePass = new RestorePasswordCmdHandler(applicationUserManager);

            //act

            //assert
            Assert.ThrowsAsync<UserDoesNotExistException>(async () => await restorePass.Execute(new RestorePasswordCmd
            {
                NewPassword = newPassword,
                OldPassword = newPassword+"123456",
                UserId = _newIdentityUser.Id
            }));
        }

        [TearDown]
        public async Task Dispose()
        {
            var user = await _applicationUserManager.FindByIdAsync(_newIdentityUser.Id);
            if (user != null)
            {
                try
                {
                    await _applicationUserManager.DeleteAsync(user);
                    using (var context = _kernel.Get<IDBContextFactory>().CreateLazuritContext())
                    {
                        var userProfile = context.Set<UserProfile>().Single(x => x.Id == user.Id);
                        context.Set<UserProfile>().Remove(userProfile);
                        context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }
    }
}
