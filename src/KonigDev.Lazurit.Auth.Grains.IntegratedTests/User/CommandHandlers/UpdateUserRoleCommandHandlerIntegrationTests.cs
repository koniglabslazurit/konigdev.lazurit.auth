﻿using System;
using System.Linq;
using KonigDev.Lazurit.Auth.Grains.Factories;
using KonigDev.Lazurit.Auth.Grains.Services.Implementations;
using KonigDev.Lazurit.Auth.Grains.Services.Interfaces;
using KonigDev.Lazurit.Auth.Grains.User.CommandHandlers.Roles;
using KonigDev.Lazurit.Auth.Model.Context.IdentityEntities;
using KonigDev.Lazurit.Auth.Model.Context.Managers;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Commands.Roles;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security.DataProtection;
using NUnit.Framework;
using System.Threading.Tasks;
using Ninject;
using KonigDev.Lazurit.Auth.Model.ProfileContext.Entities;
using System.Collections.Generic;

namespace KonigDev.Lazurit.Auth.Grains.IntegratedTests.User.CommandHandlers
{
    public class UpdateUserRoleCommandHandlerIntegrationTests
    {
        private IDBContextFactory _contextFactory;
        private IApplicationUserManager _userManager;
        private IDataProtectionProvider _dataProtectionProvider;
        private IIdentityMessageService _identityMessageService;
        private ISettingsProvider _settingsProvider;
        private IUserStore<ApplicationUser, Guid> _userStore;
        private UpdateUserRoleCmdHandler _class;
        private StandardKernel _kernel;

        [SetUp]
        public void SetUp()
        {
            _kernel = new StandardKernel();
            _contextFactory = new DBContextFactory();
            _userStore = new ApplicationUserStore(_contextFactory.CreateAuthIdentityDbContext());
            _dataProtectionProvider = new DpapiDataProtectionProvider();
            _settingsProvider = new DefaultSettingsProvider(_contextFactory);
            _identityMessageService = new EmailService(_settingsProvider);
            _userManager = new ApplicationUserManager(_userStore, _dataProtectionProvider, _identityMessageService);
            _class = new UpdateUserRoleCmdHandler(_userManager, _contextFactory);
        }

        [Test]
        public async Task UpdateUserRoleCommandHandler_ShouldChangeUserRole_WhenValidRequest()
        {
            //up
            var usersList = new List<TestData>
            {
                new TestData { Id = Guid.NewGuid(), Email = Guid.NewGuid().ToString().Replace("-","")+ "@mail.ru", Password = "123456789", Role = "saller", UserName = Guid.NewGuid().ToString().Replace("-","") }                
            };
            foreach (var item in usersList)
                await Set(item);

            //arrange
            var command = new UpdateUserRoleCmd
            {
                UserId = usersList[0].Id,
                Role = "salonSaller"
            };

            //act
            await _class.Execute(command);
            //assert
            var actual =  await _userManager.IsInRoleAsync(command.UserId, "salonSaller");
            Assert.IsTrue(actual);
            //down
            foreach (var item in usersList)
                await Down(item);
        }

        public async Task Set(TestData data)
        {            
            IDBContextFactory _contextFactory = new DBContextFactory();

            var res = await _userManager.CreateAsync(new ApplicationUser { Id = data.Id, Email = data.Email, UserName = data.UserName, JoinDate = DateTime.UtcNow }, data.Password);
            ApplicationUser appUser = await _userManager.FindByNameAsync(data.UserName);
            await _userManager.AddToRoleAsync(appUser.Id, data.Role);
            using (var context = _contextFactory.CreateLazuritContext())
            {
                context.Set<UserProfile>().Add(new UserProfile { Id = appUser.Id, Email = data.Email, LinkId = Guid.NewGuid(), Title = Guid.NewGuid().ToString() });
                context.SaveChanges();
            }
        }

        public async Task Down(TestData data)
        {            
            IDBContextFactory _contextFactory = new DBContextFactory();
            ApplicationUser appUser = await _userManager.FindByNameAsync(data.UserName);
            await _userManager.DeleteAsync(appUser);
            using (var context = _contextFactory.CreateLazuritContext())
            {
                var userProfile = context.Set<UserProfile>().FirstOrDefault(p => p.Id == appUser.Id);
                context.Set<UserProfile>().Remove(userProfile);
                context.SaveChanges();
            }
        }

        public class TestData
        {
            public string Email { get; set; }
            public Guid Id { get; internal set; }
            public string Password { get; internal set; }
            public string Role { get; set; }
            public string UserName { get; internal set; }
        }
    }
}
