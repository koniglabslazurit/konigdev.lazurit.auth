﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KonigDev.Lazurit.Auth.GrainResolver.NinjectModules;
using KonigDev.Lazurit.Auth.Grains.Factories;
using KonigDev.Lazurit.Auth.Grains.IntegratedTests.CommonModels;
using KonigDev.Lazurit.Auth.Grains.User.CommandHandlers;
using KonigDev.Lazurit.Auth.Grains.User.CommandHandlers.UsersHandlers;
using KonigDev.Lazurit.Auth.Model.Context.IdentityEntities;
using KonigDev.Lazurit.Auth.Model.Context.Managers;
using KonigDev.Lazurit.Auth.Model.DTO.Users;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Commands;
using KonigDev.Lazurit.Auth.Model.Mappings.User.Interface;
using KonigDev.Lazurit.Core.Transactions;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security.DataProtection;
using Moq;
using Ninject;
using NUnit.Framework;

namespace KonigDev.Lazurit.Auth.Grains.IntegratedTests.User.CommandHandlers
{
    [TestFixture]
    public class UpdateUserIdentityCmdHandlerTests
    {
        private StandardKernel _kernel;
        private IApplicationUserManager _applicationUserManager;
        private ApplicationUser _newIdentityUser;

        [SetUp]
        public async Task Init()
        {
            _kernel = new StandardKernel();
            _kernel.Bind<IDataProtectionProvider>().ToMethod(x => new DpapiDataProtectionProvider());
            _kernel.Load<CommandQueryModule>();
            _kernel.Load<BaseModule>();

            await CreateTempUser();
        }

        private async Task CreateTempUser()
        {
            _applicationUserManager = _kernel.Get<IApplicationUserManager>();

            var rnd= new Random();
            _newIdentityUser = new ApplicationUser
            {
                Id = Guid.NewGuid(),
                UserName = rnd.NextDouble().ToString(CultureInfo.InvariantCulture),
                Email = $"{rnd.NextDouble().ToString(CultureInfo.InvariantCulture)}@test.ru",
                PasswordHash = "123456",
                JoinDate = DateTime.Now
            };
            try
            {
                var createUserRes = await _applicationUserManager.CreateAsync(_newIdentityUser);
                if (!createUserRes.Succeeded)
                    throw new Exception("Не получилось подготовить данные и создать пользователя.");
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [Test]
        public async Task Execute_CreateLazuritContextThrowTestException_IdentityUserNotUpdated()
        {
            //arr
            var uniqUserNameForAssert = "CreateLazuritContextThrowTestExceptionIdentityUserNotUpdated2";
            var lazuritContextMock = new Mock<IDBContextFactory>();
            lazuritContextMock.Setup(x => x.CreateLazuritContext()).Throws<TestException>();
            var updateUser = new UpdateUserIdentityCmdHandler(_applicationUserManager, lazuritContextMock.Object, _kernel.Get<ITransactionFactory>());

            //act
            try
            {
                await updateUser.Execute(new UpdateUserIdentityCmd
                {
                    Password = "123456",
                    Login = uniqUserNameForAssert,
                    UserId = _newIdentityUser.Id
                });
            }
            catch (TestException)
            {
            }

            //assert
            //Обновляем контекст что бы обнаружить изменения
            _applicationUserManager = _kernel.Get<IApplicationUserManager>();
            var identityUser = await _applicationUserManager.FindByEmailAsync(_newIdentityUser.Email);
            string savedUserName = identityUser.UserName;
            Assert.That(savedUserName != uniqUserNameForAssert);
        }

        [TearDown]
        public async Task Dispose()
        {
            var user = await _applicationUserManager.FindByIdAsync(_newIdentityUser.Id);
            if (user != null)
            {
                try
                {
                    await _applicationUserManager.DeleteAsync(user);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }
    }
}
