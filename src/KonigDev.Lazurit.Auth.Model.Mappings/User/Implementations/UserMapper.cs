﻿using System;
using System.Collections.Generic;
using System.Linq;
using KonigDev.Lazurit.Auth.Model.Context.IdentityEntities;
using KonigDev.Lazurit.Auth.Model.DTO.Users;
using KonigDev.Lazurit.Auth.Model.Mappings.User.Interface;
using KonigDev.Lazurit.Auth.Model.ProfileContext.Entities;

namespace KonigDev.Lazurit.Auth.Model.Mappings.User.Implementations
{
    public class UserMapper : IUserMapper
    {
        private RoleMapper _roleMapper;

        public UserMapper()
        {
            _roleMapper = new RoleMapper();
        }

        public UserDto MapToDTO(ApplicationUser user)
        {
            return new UserDto
            {
                Id = user.Id,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                JoinDate = user.JoinDate,
                EmailConfirmed = user.EmailConfirmed,
                IsPasswordTemp = user.IsPasswordTemp,
                LockoutEnabled = user.LockoutEnabled,
                Roles = user.Roles.Select(x => _roleMapper.MapToUserRoleDto(x)).ToList()
            };
        }

        public ApplicationUser MapToApplicationUser(UserDto userDto)
        {
            return new ApplicationUser
            {
                UserName = userDto.UserName,
                Email = userDto.Email,
                JoinDate = DateTime.Now.Date,
                EmailConfirmed = userDto.EmailConfirmed,
                IsPasswordTemp = userDto.IsPasswordTemp,
                LockoutEnabled = userDto.LockoutEnabled
            };
        }

        public ApplicationUser MapToApplicationUser(CreateUserDto userDto)
        {
            return new ApplicationUser
            {
                UserName = userDto.Email, // todo мапим при создании в юзер нейм его емейл
                //UserName = userDto.UserName,
                Email = userDto.Email,
                JoinDate = DateTime.Now.Date,
                EmailConfirmed = userDto.EmailConfirmed,
                IsPasswordTemp = userDto.IsPasswordTemp,
                LockoutEnabled = userDto.LockoutEnabled,
                PhoneNumber = userDto.PhoneNumber
            };
        }

        public UserProfile MapToEntityUser(CreateUserDto user, Guid id)
        {
            var userProf = new UserProfile
            {
                Id = id,
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Title = user.UserName
            };

            if (!string.IsNullOrEmpty(user.PhoneNumber))
                userProf.UserPhones = new List<UserPhone>
                {
                    new UserPhone {Id=Guid.NewGuid(),IsDefault = true, Phone = user.PhoneNumber}
                };
            if (!string.IsNullOrEmpty(user.Address))
                userProf.UserAddresses = new List<UserAddress>
                {
                    //TODO работа с KLADR кодами
                    new UserAddress {Id=Guid.NewGuid(),IsDefault = true,Address=user.Address}
                };
            return userProf;
        }
    }
}
