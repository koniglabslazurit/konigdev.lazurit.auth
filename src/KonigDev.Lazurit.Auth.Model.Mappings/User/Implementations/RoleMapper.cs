﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KonigDev.Lazurit.Auth.Model.Context.IdentityEntities;
using KonigDev.Lazurit.Auth.Model.DTO.Users;
using KonigDev.Lazurit.Auth.Model.Mappings.User.Interface;
using Microsoft.AspNet.Identity.EntityFramework;

namespace KonigDev.Lazurit.Auth.Model.Mappings.User.Implementations
{
    public class RoleMapper:IRoleMapper
    {
        public RoleDto MapToDTO(ApplicationRole role)
        {

            return new RoleDto
            {
                Id = role.Id,
                Name = role.Name,
                Users = role.Users.Select(MapToUserRoleDto).ToList()
            };
        }

        public UserRoleDto MapToUserRoleDto(ApplicationUserRole userRole)
        {
            return new UserRoleDto
            {
                UserId = userRole.UserId,
                RoleId = userRole.RoleId
            };
        }
    }
}
