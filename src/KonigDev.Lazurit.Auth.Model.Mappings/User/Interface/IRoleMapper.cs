﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KonigDev.Lazurit.Auth.Model.Context.IdentityEntities;
using KonigDev.Lazurit.Auth.Model.DTO.Users;
using Microsoft.AspNet.Identity.EntityFramework;

namespace KonigDev.Lazurit.Auth.Model.Mappings.User.Interface
{
    public interface IRoleMapper
    {
        RoleDto MapToDTO(ApplicationRole role);
        UserRoleDto MapToUserRoleDto(ApplicationUserRole userRole);

    }
}
