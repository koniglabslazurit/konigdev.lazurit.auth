﻿using KonigDev.Lazurit.Auth.Model.Context.IdentityEntities;
using KonigDev.Lazurit.Auth.Model.DTO.Users;
using KonigDev.Lazurit.Auth.Model.ProfileContext.Entities;
using System;

namespace KonigDev.Lazurit.Auth.Model.Mappings.User.Interface
{
    public interface IUserMapper
    {
        UserDto MapToDTO(ApplicationUser user);
        ApplicationUser MapToApplicationUser(UserDto userDto);
        ApplicationUser MapToApplicationUser(CreateUserDto userDto);
        UserProfile MapToEntityUser(CreateUserDto user, Guid id);
    }
}
