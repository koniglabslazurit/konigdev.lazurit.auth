﻿using KonigDev.Lazurit.Auth.Grains.User.CommandHandlers.Email;
using KonigDev.Lazurit.Auth.Model.Context.Managers;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Commands.Email;
using Moq;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Auth.Grains.UnitTests.UserTests.CommandHandlerTests.EmailTests
{
    [TestFixture]
    public class ConfirmEmailForUserCmdHandlerTest
    {
        [Test]
        public async Task Execute_ParameterUsersEqualsNull_ExceptionThrow()
        {
            //arrange
            bool exceptionIsThrow = false;
            var classForTest = new ConfirmEmailForUserCmdHandler(Mock.Of<IApplicationUserManager>());

            //act
            try
            {
                await classForTest.Execute(new ConfirmEmailForUserCmd { });
            }
            catch (Exception)
            {
                exceptionIsThrow = true;
            }

            //assert
            Assert.IsTrue(exceptionIsThrow);
        }
    }
}
