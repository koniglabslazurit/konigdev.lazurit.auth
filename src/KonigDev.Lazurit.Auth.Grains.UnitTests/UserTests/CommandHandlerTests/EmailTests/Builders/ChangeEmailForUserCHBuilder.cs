﻿using KonigDev.Lazurit.Auth.Grains.Factories;
using KonigDev.Lazurit.Auth.Grains.UnitTests.CommonExtensions;
using KonigDev.Lazurit.Auth.Grains.User.CommandHandlers.Email;
using KonigDev.Lazurit.Auth.Model.Context.IdentityEntities;
using KonigDev.Lazurit.Auth.Model.Context.Managers;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Commands.Email;
using KonigDev.Lazurit.Auth.Model.ProfileContext.Entities;
using KonigDev.Lazurit.Core.Transactions;
using Microsoft.AspNet.Identity;
using Moq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Auth.Grains.UnitTests.UserTests.CommandHandlerTests.EmailTests.Builders
{
    public class ChangeEmailForUserCHBuilder
    {
        private ChangeEmailForUserCmd _parameterCmd;
        private Mock<IApplicationUserManager> _userManagerMock;
        private Mock<ITransactionFactory> _transactionFactoryMock;
        private Mock<IDBContextFactory> _dataContextFactory;
        
        public ChangeEmailForUserCmd ParameterCmd => _parameterCmd;
        public Mock<IApplicationUserManager> UserManagerMock => _userManagerMock;
        public Mock<ITransactionFactory> TransactionFactoryMock => _transactionFactoryMock;
        public Mock<IDBContextFactory> DataContextFactory => _dataContextFactory;

        public ChangeEmailForUserCHBuilder WithCmdParameters(Guid userId)
        {
            _parameterCmd = new ChangeEmailForUserCmd
            {
                NewEmail = "q@q.ru",
                UserId = userId
            };
            return this;
        }

        public ChangeEmailForUserCHBuilder WithUserManagerForRandomFindByIdAsyncMock()
        {
            _userManagerMock = new Mock<IApplicationUserManager>();
            _userManagerMock.Setup(x => x.FindByIdAsync(It.IsAny<Guid>()))
                .Returns(Task.FromResult(new ApplicationUser
                {
                    JoinDate = new DateTime(),
                    IsPasswordTemp = true,
                    Id = Guid.NewGuid()
                }));
            return this;
        }

        public ChangeEmailForUserCHBuilder WithUserManagerIdFindByIdAsyncMock(Guid userId)
        {
            _userManagerMock = new Mock<IApplicationUserManager>();
            _userManagerMock.Setup(x => x.FindByIdAsync(It.IsAny<Guid>()))
                .Returns(Task.FromResult(new ApplicationUser
                {
                    JoinDate = new DateTime(),
                    IsPasswordTemp = true,
                    Id = userId
                }));
            return this;
        }

        public ChangeEmailForUserCHBuilder WithDbContextFactoryForMockUserProfiles(Guid userId)
        {
            var dbContextMock = new Mock<DbContext>();
            dbContextMock.Setup(x => x.Set<UserProfile>()).Returns(new List<UserProfile>() {new UserProfile
            {               
                Id = userId
            } }.GetQueryableMockDbSet());
            _dataContextFactory = new Mock<IDBContextFactory>();
            _dataContextFactory.Setup(x => x.CreateLazuritContext()).Returns(dbContextMock.Object);
            return this;
        }

        public ChangeEmailForUserCmdHandler Build()
        {            
            if (_userManagerMock == null)
                _userManagerMock = new Mock<IApplicationUserManager>();
            /*setup for funcs */
            _userManagerMock.Setup(p => p.RemovePasswordAsync(It.IsAny<Guid>()))
                .Returns(Task.FromResult(IdentityResult.Success));
            _userManagerMock.Setup(p => p.AddPasswordAsync(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(Task.FromResult(IdentityResult.Success));

            if (_dataContextFactory == null)
                _dataContextFactory = new Mock<IDBContextFactory>();
            return new ChangeEmailForUserCmdHandler(_userManagerMock.Object, Mock.Of<ITransactionFactory>(), _dataContextFactory.Object);
        }
    }
}
