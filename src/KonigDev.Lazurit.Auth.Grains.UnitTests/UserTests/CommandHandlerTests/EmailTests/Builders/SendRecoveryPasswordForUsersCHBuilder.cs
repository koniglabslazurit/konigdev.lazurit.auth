﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KonigDev.Lazurit.Auth.Grains.Factories;
using KonigDev.Lazurit.Auth.Grains.Services.Interfaces;
using KonigDev.Lazurit.Auth.Grains.UnitTests.CommonExtensions;
using KonigDev.Lazurit.Auth.Grains.User.CommandHandlers.Email;
using KonigDev.Lazurit.Auth.Model.Context.IdentityEntities;
using KonigDev.Lazurit.Auth.Model.Context.Managers;
using KonigDev.Lazurit.Auth.Model.DTO.Users;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Commands.Email;
using KonigDev.Lazurit.Core.Services;
using KonigDev.Lazurit.Core.Transactions;
using KonigDev.Lazurit.Auth.Model.ProfileContext.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using Moq;
using Microsoft.AspNet.Identity;

namespace KonigDev.Lazurit.Auth.Grains.UnitTests.UserTests.CommandHandlerTests.EmailTests.Builders
{
    /// <summary>
    /// Класс, инкапсулирующий логику моков для обработчика комманды SendRecoveryPasswordForUsers
    /// </summary>
    public class SendRecoveryPasswordForUsersCHBuilder
    {
        private SendRecoveryPasswordForUsersCmd _parameterCmd;
        private Mock<IApplicationUserManager> _userManagerMock;
        private Mock<ICryptoService> _cryptoServiceMock;
        private Mock<IDBContextFactory> _dataContextFactory;
        private Mock<IApplicationRoleManager> _roleManagerMock;
        private Mock<IEmailTemplateRenderingService> _emailTemplateRenderingMock;

        private readonly Guid _marketerRoleId = Guid.NewGuid();
        private readonly Guid _buyerRoleId = Guid.NewGuid();
        private readonly Guid _sellerRoleId = Guid.NewGuid();

        public string DecryptingPassword { get; } = "qwerty";

        public Mock<IEmailTemplateRenderingService> EmailTemplateRenderingMock => _emailTemplateRenderingMock;
        public SendRecoveryPasswordForUsersCmd ParameterCmd => _parameterCmd;
        public Mock<IApplicationUserManager> UserManagerMock => _userManagerMock;

        public SendRecoveryPasswordForUsersCHBuilder WithCmdParametersWithOneRoles()
        {
            _parameterCmd = new SendRecoveryPasswordForUsersCmd
            {
                Users = new List<UserDto>() { new UserDto() { Roles = new List<UserRoleDto>() { new UserRoleDto() } } }
            };
            return this;
        }

        public SendRecoveryPasswordForUsersCHBuilder WithCmdParametersWithOneRoles(Guid userId)
        {
            _parameterCmd = new SendRecoveryPasswordForUsersCmd
            {
                Users = new List<UserDto>() { new UserDto
                {
                    Roles = new List<UserRoleDto>() { new UserRoleDto() },
                    Id = userId
                } }
            };
            return this;
        }

        public SendRecoveryPasswordForUsersCHBuilder WithCmdParametersWithSeveralRoles(Guid userId)
        {
            _parameterCmd = new SendRecoveryPasswordForUsersCmd
            {
                Users = new List<UserDto>() { new UserDto
                {
                    Roles = new List<UserRoleDto>()
                    {
                        new UserRoleDto() {RoleId =_marketerRoleId},
                        new UserRoleDto() {RoleId = _buyerRoleId},
                        new UserRoleDto() {RoleId = _sellerRoleId}
                    },
                    Id = userId
                } }
            };
            return this;
        }


        public SendRecoveryPasswordForUsersCHBuilder WithUserManagerForRandomFindByIdAsyncMock()
        {
            _userManagerMock = new Mock<IApplicationUserManager>();
            _userManagerMock.Setup(x => x.FindByIdAsync(It.IsAny<Guid>()))
                .Returns(Task.FromResult(new ApplicationUser
                {
                    JoinDate = new DateTime(),
                    IsPasswordTemp = true,
                    Id = Guid.NewGuid()
                }));
            return this;
        }

        public SendRecoveryPasswordForUsersCHBuilder WithUserManagerForRandomFindByIdAsyncMock(Guid userId,Guid regionId)
        {
            _userManagerMock = new Mock<IApplicationUserManager>();
            var identityUser = new ApplicationUser
            {
                JoinDate = new DateTime(),
                IsPasswordTemp = true,
                Id = userId,
                UserName = "TestLogin",
                Roles = { new ApplicationUserRole() {RoleId = _sellerRoleId} }
            };
            _userManagerMock.Setup(x => x.FindByIdAsync(userId))
                .Returns(Task.FromResult(identityUser));

            _userManagerMock.Setup(x => x.FindByIdAsync(regionId))
               .Returns(Task.FromResult(identityUser));
            _userManagerMock.Setup(x => x.PasswordHasher.HashPassword(It.IsAny<string>())).Returns("Test");
            return this;
        }

        public SendRecoveryPasswordForUsersCHBuilder WithDbContextFactoryForMockUserProfiles(string passwordForRegion, Guid userId, Guid regionId)
        {
            string testTitle = passwordForRegion + "Title";
            var dbContextMock = new Mock<DbContext>();
            dbContextMock.Setup(x => x.Set<UserProfile>()).Returns(new List<UserProfile>() {new UserProfile
            {
                Regions = new List<Region>()
                {
                    new Region()
                    {
                        LazuritPasswordHash = passwordForRegion,
                        Title = testTitle,
                        Id = regionId
                    }
                },
                Id = userId
            } }.GetQueryableMockDbSet());
            _dataContextFactory = new Mock<IDBContextFactory>();
            _dataContextFactory.Setup(x => x.CreateLazuritContext()).Returns(dbContextMock.Object);
            return this;
        }

        public SendRecoveryPasswordForUsersCHBuilder WithRoleMenegerForFindByIdSeller()
        {
            _roleManagerMock = new Mock<IApplicationRoleManager>();
            _roleManagerMock.Setup(x => x.FindByIdAsync(It.IsAny<Guid>())).Returns(Task.FromResult(new ApplicationRole(Model.DTO.Constants.SallerRoleName)));
            return this;
        }
        public SendRecoveryPasswordForUsersCHBuilder WithRoleMenegerForFindByIdMarketer()
        {
            _roleManagerMock = new Mock<IApplicationRoleManager>();
            _roleManagerMock.Setup(x => x.FindByIdAsync(It.IsAny<Guid>())).Returns(Task.FromResult(new ApplicationRole(Model.DTO.Constants.MarketerRoleName)));
            return this;
        }
        public SendRecoveryPasswordForUsersCHBuilder WithRoleMenegerForFindByIdSellerBuyerMarketer()
        {
            _roleManagerMock = new Mock<IApplicationRoleManager>();
            _roleManagerMock.Setup(x => x.FindByIdAsync(_sellerRoleId)).Returns(Task.FromResult(new ApplicationRole(Model.DTO.Constants.SallerRoleName)));
            _roleManagerMock.Setup(x => x.FindByIdAsync(_marketerRoleId)).Returns(Task.FromResult(new ApplicationRole(Model.DTO.Constants.MarketerRoleName)));
            _roleManagerMock.Setup(x => x.FindByIdAsync(_buyerRoleId)).Returns(Task.FromResult(new ApplicationRole(Model.DTO.Constants.BuyerRoleName)));
            return this;
        }


        public SendRecoveryPasswordForUsersCHBuilder WithCryptoServiceMock()
        {
            _cryptoServiceMock = new Mock<ICryptoService>();
            _cryptoServiceMock.Setup(x => x.DecryptStringAES(It.IsAny<string>(), It.IsAny<string>())).Returns(DecryptingPassword);
            return this;
        }


        public SendRecoveryPasswordForUsersCmdHandler Build()
        {
            if (_roleManagerMock == null)
                _roleManagerMock = new Mock<IApplicationRoleManager>();
            if (_userManagerMock == null)            
                _userManagerMock = new Mock<IApplicationUserManager>();
            /*setup for funcs */
            _userManagerMock.Setup(p => p.RemovePasswordAsync(It.IsAny<Guid>()))
                .Returns(Task.FromResult(IdentityResult.Success));
            _userManagerMock.Setup(p => p.AddPasswordAsync(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(Task.FromResult(IdentityResult.Success)); 

            if (_dataContextFactory == null)
                _dataContextFactory = new Mock<IDBContextFactory>();
            if (_cryptoServiceMock == null)
                _cryptoServiceMock = new Mock<ICryptoService>();
            if (_emailTemplateRenderingMock == null)
                _emailTemplateRenderingMock = new Mock<IEmailTemplateRenderingService>();
            return new SendRecoveryPasswordForUsersCmdHandler(_roleManagerMock.Object, _userManagerMock.Object, _dataContextFactory.Object, _cryptoServiceMock.Object, _emailTemplateRenderingMock.Object,Mock.Of<ITransactionFactory>());
        }
    }
}
