﻿using System;
using System.Threading.Tasks;
using KonigDev.Lazurit.Auth.Grains.Factories;
using KonigDev.Lazurit.Auth.Grains.Services.Implementations;
using KonigDev.Lazurit.Auth.Grains.Services.Interfaces;
using KonigDev.Lazurit.Auth.Grains.UnitTests.UserTests.CommandHandlerTests.EmailTests.Builders;
using KonigDev.Lazurit.Auth.Grains.User.CommandHandlers.Email;
using KonigDev.Lazurit.Auth.Model.Context.Managers;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Commands.Email;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Exceptions;
using KonigDev.Lazurit.Core.Transactions;
using Moq;
using NUnit.Framework;

namespace KonigDev.Lazurit.Auth.Grains.UnitTests.UserTests.CommandHandlerTests.EmailTests
{
    [TestFixture]
    public class SendRecoveryPasswordForUsersCmdHandlerTest
    {
        [Test]
        public async Task Execute_ParameterUsersEualsNull_ExceptionNotThrow()
        {
            //arrange
            bool exceptionIsThrow = false;
            var classForTest = new SendRecoveryPasswordForUsersCmdHandler(Mock.Of<IApplicationRoleManager>(), Mock.Of<IApplicationUserManager>(), Mock.Of<IDBContextFactory>(), Mock.Of<ICryptoService>(), new EmailTemplateRenderingService(), Mock.Of<ITransactionFactory>());

            //act
            try
            {
                await classForTest.Execute(new SendRecoveryPasswordForUsersCmd());
            }
            catch (Exception)
            {
                exceptionIsThrow = true;
            }

            //assert
            Assert.IsFalse(exceptionIsThrow);
        }

        [Test]
        public void Execute_NotExistRoleIdForOneRoles_RoleDoesNotExistException()
        {
            //arrange
            var actor = new SendRecoveryPasswordForUsersCHBuilder()
                .WithCmdParametersWithOneRoles()
                .WithUserManagerForRandomFindByIdAsyncMock();

            var classForTest = actor.Build();

            //assert, act
            Assert.That(async () => await classForTest.Execute(actor.ParameterCmd), Throws.TypeOf<RoleDoesNotExistException>());
        }

        [Test]
        public void Execute_NotExistRoleIdForSeveralRoles_RoleDoesNotExistException()
        {
            //arrange
            var actor = new SendRecoveryPasswordForUsersCHBuilder()
                .WithCmdParametersWithSeveralRoles(Guid.NewGuid())
                .WithUserManagerForRandomFindByIdAsyncMock();

            var classForTest = actor.Build();

            //assert, act
            Assert.That(async () => await classForTest.Execute(actor.ParameterCmd), Throws.TypeOf<RoleDoesNotExistException>());
        }

        [Test]
        public void Execute_NotExistUserIdForOneRoles_UserDoesNotExistException()
        {
            //arrange
            var actor = new SendRecoveryPasswordForUsersCHBuilder()
                .WithCmdParametersWithOneRoles();

            var classForTest = actor.Build();

            //assert,act
            Assert.That(async () => await classForTest.Execute(actor.ParameterCmd), Throws.TypeOf<UserDoesNotExistException>());
        }

        [Test]
        public void Execute_NotExistUserIdForSeveralRoles_UserDoesNotExistException()
        {
            //arrange
            var actor = new SendRecoveryPasswordForUsersCHBuilder()
                .WithCmdParametersWithSeveralRoles(Guid.NewGuid());

            var classForTest = actor.Build();

            //assert,act
            Assert.That(async () => await classForTest.Execute(actor.ParameterCmd), Throws.TypeOf<UserDoesNotExistException>());
        }

        [Test]
        public async Task Execute_UserIsSaller_SendEmailForSellersInvoke()
        {
            //arrange
            Guid userId = Guid.NewGuid();
            Guid regionId = Guid.NewGuid();
            string passwordForRegion = "123";

            var actor = new SendRecoveryPasswordForUsersCHBuilder()
                .WithCmdParametersWithOneRoles(userId)
                .WithUserManagerForRandomFindByIdAsyncMock(userId, regionId)
                .WithDbContextFactoryForMockUserProfiles(passwordForRegion, userId, regionId)
                .WithCryptoServiceMock()
                .WithRoleMenegerForFindByIdSeller();

            var classForTest = actor.Build();

            //act
            await classForTest.Execute(actor.ParameterCmd);

            //assert
            actor.UserManagerMock.Verify(x => x.SendEmailAsync(It.IsAny<Guid>(), Constants.RecoveryPasswordSubject, It.IsAny<string>()), Times.Once);
        }

        [Test]
        public async Task Execute_UserIsSallerAndMarketerAndBuyer_SendEmailForSellersInvoke()
        {
            //arrange
            Guid userId = Guid.NewGuid();
            Guid regionId = Guid.NewGuid();
            string passwordForRegion = "123";

            var actor = new SendRecoveryPasswordForUsersCHBuilder()
                .WithCmdParametersWithSeveralRoles(userId)
                .WithUserManagerForRandomFindByIdAsyncMock(userId, regionId)
                .WithDbContextFactoryForMockUserProfiles(passwordForRegion, userId, regionId)
                .WithCryptoServiceMock()
                .WithRoleMenegerForFindByIdSellerBuyerMarketer();

            var classForTest = actor.Build();

            //act
            await classForTest.Execute(actor.ParameterCmd);

            //assert
            actor.UserManagerMock.Verify(x => x.SendEmailAsync(It.IsAny<Guid>(), Constants.RecoveryPasswordSubject, It.IsAny<string>()), Times.Once);
        }

    }
}
