﻿using KonigDev.Lazurit.Auth.Grains.Factories;
using KonigDev.Lazurit.Auth.Grains.UnitTests.UserTests.CommandHandlerTests.EmailTests.Builders;
using KonigDev.Lazurit.Auth.Grains.User.CommandHandlers.Email;
using KonigDev.Lazurit.Auth.Model.Context.Managers;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Commands.Email;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Exceptions;
using KonigDev.Lazurit.Core.Transactions;
using Moq;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace KonigDev.Lazurit.Auth.Grains.UnitTests.UserTests.CommandHandlerTests.EmailTests
{
    [TestFixture]
    public class ChangeEmailForUserCmdHandlerTest
    {
        [Test]
        public async Task Execute_ParameterUsersEqualsNull_ExceptionThrow()
        {
            //arrange
            bool exceptionIsThrow = false;
            var classForTest = new ChangeEmailForUserCmdHandler(Mock.Of<IApplicationUserManager>(), Mock.Of<ITransactionFactory>(), Mock.Of<IDBContextFactory>());

            //act
            try
            {
                await classForTest.Execute(new ChangeEmailForUserCmd { });
            }
            catch (Exception)
            {
                exceptionIsThrow = true;
            }

            //assert
            Assert.IsTrue(exceptionIsThrow);
        }
    }
}
