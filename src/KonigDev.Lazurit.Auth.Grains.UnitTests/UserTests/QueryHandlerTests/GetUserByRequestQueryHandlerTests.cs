﻿using KonigDev.Lazurit.Auth.Grains.Factories;
using KonigDev.Lazurit.Auth.Grains.User.QueryHandlers;
using KonigDev.Lazurit.Auth.Model.DTO.Common.Exceptions;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Exceptions;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Queries;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Data;

namespace KonigDev.Lazurit.Auth.Grains.UnitTests.UserTests.QueryHandlerTests
{
    [TestFixture]
    public class GetUserByRequestQueryHandlerTests
    {
        private Mock<IDBContextFactory> _mockContextFactory;
        private GetUsersByRequestQueryHandler _testClass;
        private Mock<IDbConnection> _contextMock;

        [SetUp]
        public void SetUp()
        {
            _mockContextFactory = new Mock<IDBContextFactory>();
            _testClass = new GetUsersByRequestQueryHandler(_mockContextFactory.Object);
            _contextMock = new Mock<IDbConnection>();
            _mockContextFactory.Setup(x => x.CreateIdentityConnection()).Returns(_contextMock.Object);
        }

        [Test]
        public void Execute_ShouldReturnException_WhenModelIsEmpty()
        {
            //arrange
            GetUsersByRequestQuery query = null;
            //act
            //assert
            Assert.Throws<QueryNullException>(() => { _testClass.Execute(query); });            
        }
        [Test]
        public void Execute_ShouldReturnException_WhenRolesIsEmpty()
        {
            //arrange
            var query = new GetUsersByRequestQuery {
                Roles = new List<string>()
            };
            //act
            //assert
            Assert.Throws<UserRolesIsEmpty>(() => { _testClass.Execute(query); });
        }       

    }
}
