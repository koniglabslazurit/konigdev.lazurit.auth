﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KonigDev.Lazurit.Auth.Grains.Services.Implementations;
using NUnit.Framework;

namespace KonigDev.Lazurit.Auth.Grains.UnitTests.ServicesTests
{
    [TestFixture]
    public class CryptoServiceTests
    {
        [Test]
        public void CompareEncryptedAndDecryptedResults()
        {
            //arrange
            string password = "123456";
            var service =new CryptoService();

            //act

            var encryptedPassword=service.EncryptStringAES(password, Constants.SharedSecret);
            var decryptString= service.DecryptStringAES(encryptedPassword, Constants.SharedSecret);
            
            //assert
            Assert.That(decryptString==password);
            Assert.Pass(encryptedPassword);
        }
    }
}
