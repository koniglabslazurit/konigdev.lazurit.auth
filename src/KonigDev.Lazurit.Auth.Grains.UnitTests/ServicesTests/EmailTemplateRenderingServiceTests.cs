﻿using System.Collections.Generic;
using KonigDev.Lazurit.Auth.Grains.Services.Implementations;
using NUnit.Framework;

namespace KonigDev.Lazurit.Auth.Grains.UnitTests.ServicesTests
{
    [TestFixture]
    public class EmailTemplateRenderingServiceTests
    {
        [Test]
        public void GetBodyForSallers_UserNameAndPasswords_ReturnCorrectBody()
        {
            //arrange
            var emailTemplateService = new EmailTemplateRenderingService();
            string userName = "test";
            string region1Name = "region1Name";
            string region1Password = "region1Password";
            string region2Name = "region2Name";
            string region2Password = "region2Password";

            var regionPasswords = new Dictionary<string, string>
            {
                {region1Name, region1Password},
                {region2Name, region2Password}
            };

            //act
            string body=emailTemplateService.GetBodyForSallers(userName, regionPasswords);

            //assert
            Assert.That(body.Contains(userName));
            Assert.That(body.Contains(region1Name));
            Assert.That(body.Contains(region1Password));
            Assert.That(body.Contains(region2Name));
            Assert.That(body.Contains(region2Password));
        }
    }
}
