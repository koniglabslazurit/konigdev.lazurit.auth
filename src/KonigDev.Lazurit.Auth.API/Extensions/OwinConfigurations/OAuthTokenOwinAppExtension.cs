﻿using System;
using System.Configuration;
using System.Web;
using KonigDev.Lazurit.Auth.API.IOC;
using KonigDev.Lazurit.Auth.API.Providers;
using KonigDev.Lazurit.Auth.Grains.Interfaces;
using KonigDev.Lazurit.Core.Services;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Microsoft.Owin.Security.Jwt;
using Microsoft.Owin.Security.OAuth;
using Ninject;
using Ninject.Web.Common.OwinHost;
using Owin;

namespace KonigDev.Lazurit.Auth.API.Extensions.OwinConfigurations
{
    public static class OAuthTokenOwinAppExtension
    {
        /// <summary>
        /// Конфигурируем OWIN MiddleWare для поддержки возможности генерации токена
        /// </summary>
        /// <param name="app">Экземпляр приложения Owin</param>
        public static void UseOAuthTokenGeneration(this IAppBuilder app)
        {
            OAuthAuthorizationServerOptions oAuthServerOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/oauth/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                Provider = new CustomOAuthProvider(app.GetKernelFromOwinApp().Get<IAuthHandlersFactory>(), app.GetKernelFromOwinApp().Get<ILoggingService>()),
                AccessTokenFormat = new CustomJwtFormat(ConfigurationManager.AppSettings["as:HostUrl"]),
                AllowInsecureHttp = true
            };
            //todo при использовании на продакшене выставляем секьюрность по https, если на продакшене все клиенты стучатся по https

            //Добаяляем в OAuth 2.0 поддержку Bearer Access Token 
            app.UseOAuthAuthorizationServer(oAuthServerOptions);
        }

        public static void UseOAuthTokenForConsumer(this IAppBuilder app)
        {
            var issuer = ConfigurationManager.AppSettings["as:HostUrl"];

            string audienceId = ConfigurationManager.AppSettings["as:AudienceId"];
            var audienceSecret = TextEncodings.Base64Url.Decode(ConfigurationManager.AppSettings["as:AudienceSecret"]);

            app.UseJwtBearerAuthentication(
            new JwtBearerAuthenticationOptions
            {
                AuthenticationMode = AuthenticationMode.Active,
                AllowedAudiences = new[] { audienceId },
                IssuerSecurityTokenProviders = new IIssuerSecurityTokenProvider[]
                {
                        new SymmetricKeyIssuerSecurityTokenProvider(issuer, audienceSecret)
                }
            });

        }
    }
}