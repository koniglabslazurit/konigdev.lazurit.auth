﻿using KonigDev.Lazurit.Auth.Model.DTO.Users;
using DTOConstants= KonigDev.Lazurit.Auth.Model.DTO;

namespace KonigDev.Lazurit.Auth.API.Extensions.Filters
{
    public static class RoleDtoFilters
    {
        public static bool IsSeller(this RoleDto role)
        {
            return role.Name == DTOConstants.Constants.SallerRoleName;
        }
    }
}