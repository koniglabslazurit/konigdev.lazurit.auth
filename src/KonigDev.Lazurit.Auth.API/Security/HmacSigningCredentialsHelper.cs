﻿using System;
using Microsoft.IdentityModel.Tokens;

namespace KonigDev.Lazurit.Auth.API.Security
{
    //Класс, создающий подпись на основе алгоритма шифрования Hmac
    public class HmacSigningCredentialsHelper
    {
        public static string CreateSignatureAlgorithm(byte[] key)
        {
            switch (key.Length)
            {
                case 32:
                    return "http://www.w3.org/2001/04/xmldsig-more#hmac-sha256";
                case 48:
                    return "http://www.w3.org/2001/04/xmldsig-more#hmac-sha384";
                case 64:
                    return "http://www.w3.org/2001/04/xmldsig-more#hmac-sha512";
                default:
                    throw new InvalidOperationException("Unsupported key lenght");
            }
        }

        public static string CreateDigestAlgorithm(byte[] key)
        {
            switch (key.Length)
            {
                case 32:
                    return "http://www.w3.org/2001/04/xmlenc#sha256";
                case 48:
                    return "http://www.w3.org/2001/04/xmlenc#sha384";
                case 64:
                    return "http://www.w3.org/2001/04/xmlenc#sha512";
                default:
                    throw new InvalidOperationException("Unsupported key length");
            }
        }
    }
}