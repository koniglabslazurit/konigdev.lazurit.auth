﻿using KonigDev.Lazurit.Auth.Grains.Interfaces;
using Orleans;
using System;

namespace KonigDev.Lazurit.Auth.API.Factories
{
    public class OrleansGrainsFactory : IAuthHandlersFactory
    {
        public IAuthCommandHandler<TCommand> CreateCommandHandler<TCommand>()
        {
            return GrainClient.GrainFactory.GetGrain<IAuthCommandHandler<TCommand>>(Guid.NewGuid());
        }

        public IAuthQueryHandler<TQuery, TResult> CreateQueryHandler<TQuery, TResult>()
        {
            return GrainClient.GrainFactory.GetGrain<IAuthQueryHandler<TQuery, TResult>>(Guid.NewGuid());
        }
    }
}