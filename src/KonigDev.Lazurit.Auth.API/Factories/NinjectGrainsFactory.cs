﻿using KonigDev.Lazurit.Auth.API.IOC;
using KonigDev.Lazurit.Auth.Grains.Interfaces;
using Ninject;
using Ninject.Syntax;

namespace KonigDev.Lazurit.Auth.API.Factories
{
    public class NinjectGrainsFactory : IAuthHandlersFactory
    {
        private readonly IResolutionRoot _resolutionRoot;
        public NinjectGrainsFactory(IResolutionRoot resolutionRoot)
        {
            _resolutionRoot = resolutionRoot;
        }
        public IAuthCommandHandler<TCommand> CreateCommandHandler<TCommand>()
        {            
            return _resolutionRoot.Get<IAuthCommandHandler<TCommand>>();
        }

        public IAuthQueryHandler<TQuery, TResult> CreateQueryHandler<TQuery, TResult>()
        {
            return _resolutionRoot.Get<IAuthQueryHandler<TQuery, TResult>>();
        }
    }
}