﻿using System;
using System.Web.Http;
using KonigDev.Lazurit.Auth.API.Controllers.ApiControllers;
using KonigDev.Lazurit.Auth.API.Extensions.OwinConfigurations;
using KonigDev.Lazurit.Auth.API.IOC;
using Microsoft.Owin.Security.DataProtection;
using Ninject.Web.Common.OwinHost;
using Ninject.Web.WebApi.OwinHost;
using NLog;
using Owin;

namespace KonigDev.Lazurit.Auth.API
{
    public class Startup
    {
        public static IDataProtectionProvider DataProtectionProvider { get; private set; }
        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration httpConfig = new HttpConfiguration();

            DataProtectionProvider = app.GetDataProtectionProvider();

            app.UseOAuthTokenForConsumer();
            app.UseOAuthTokenGeneration();

            httpConfig.ConfigureWebApi();
            //Добавляем в middleWare поддержку кросс-доменных запросов
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

            //app.UseOrleans();

            app.UseNinjectMiddleware(app.GetKernelFromOwinApp);
            app.UseNinjectWebApi(httpConfig);

            /* loging start with version */
            try
            {
                var version = typeof(AccountController).Assembly.GetName().Version.ToString();
                var log = LogManager.GetCurrentClassLogger();
                log.Info($"I_Start:{(!string.IsNullOrEmpty(version) ? $"; Version - {version}" : "version undefined")}");
            }
            catch (Exception ex)
            {
                var log = LogManager.GetCurrentClassLogger();
                log.Info($"I_Start: with error {0}", ex.Message);
            }
        }
    }
}