﻿using System;
using System.Collections.Generic;
using KonigDev.Lazurit.Auth.API.Mappings.Interfaces;
using KonigDev.Lazurit.Auth.API.ViewModels;
using KonigDev.Lazurit.Auth.InternalAPI.DTO;
using KonigDev.Lazurit.Auth.Model.DTO.Users;

namespace KonigDev.Lazurit.Auth.API.Mappings.Implementations
{
    public class AccountVMMapper:IAccountVMMapper
    {
        public UserDto MapToDTO(CreateUserVM userVM)
        {
           return new UserDto
            {
                //UserName = userVM.Username,
                Email = userVM.Email,
                FirstName = userVM.FirstName,
                LastName = userVM.LastName,
                JoinDate = DateTime.Now.Date,
            };
        }

        public CreateUserDto MapToCreateUserDTO(CreateUserVM userVM)
        {
            return new CreateUserDto
            {
                //UserName = userVM.Username,
                Email = userVM.Email,
                FirstName = userVM.FirstName,
                LastName = userVM.LastName,
                JoinDate = DateTime.Now.Date,
                RoleName = userVM.RoleName
            };
        }

        public DtoUserConfirmRegistrationRequest MapVMUserConfirmToDTOUserConfirm(VmUserConfirmRegistrationRequest request)
        {
            return new DtoUserConfirmRegistrationRequest()
            {
                Password = request.Password,
                Address = request.Address,
                ConfirmPassword = request.ConfirmPassword,
                IsAgreed = request.IsAgreed,
                Login = request.Login,
                Phone = request.Phone,
                Token = request.Token,
                UserId =request.UserId 
            };
        }
    }
}