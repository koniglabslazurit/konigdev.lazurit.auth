﻿using KonigDev.Lazurit.Auth.API.ViewModels;
using KonigDev.Lazurit.Auth.InternalAPI.DTO;
using KonigDev.Lazurit.Auth.Model.DTO.Users;

namespace KonigDev.Lazurit.Auth.API.Mappings.Interfaces
{
    public interface IAccountVMMapper
    {
        UserDto MapToDTO(CreateUserVM userVM);
        CreateUserDto MapToCreateUserDTO(CreateUserVM userVM);
        DtoUserConfirmRegistrationRequest MapVMUserConfirmToDTOUserConfirm(VmUserConfirmRegistrationRequest request);
    }
}