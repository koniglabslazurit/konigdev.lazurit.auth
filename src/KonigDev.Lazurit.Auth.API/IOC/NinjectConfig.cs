﻿using System;
using System.Reflection;
using KonigDev.Lazurit.Auth.API.Mappings.Implementations;
using KonigDev.Lazurit.Auth.API.Mappings.Interfaces;
using Ninject;
using KonigDev.Lazurit.Auth.Grains.Interfaces;
using KonigDev.Lazurit.Auth.API.Factories;
using KonigDev.Lazurit.Auth.GrainResolver.NinjectModules;
using KonigDev.Lazurit.Auth.InternalAPI.NinjectModules;
using KonigDev.Lazurit.Auth.InternalAPI.Services.Implementations;
using Microsoft.Owin.Security.DataProtection;
using Owin;

namespace KonigDev.Lazurit.Auth.API.IOC
{
    public static class NinjectConfig
    {
        public static Lazy<IKernel> CreateKernel = new Lazy<IKernel>(() =>
        {
            var kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());
            RegisterServices(kernel);

            return kernel;
        });

        private static void RegisterServices(KernelBase kernel)
        {
            kernel.Bind<IAccountVMMapper>().To<AccountVMMapper>();
            kernel.Bind<IDataProtectionProvider>().ToMethod(x => Startup.DataProtectionProvider);

            // получаем фабрику для работы с "грейнами" без orlean.
            kernel.Bind<IAuthHandlersFactory>().To<NinjectGrainsFactory>();
            //получаем фабрику для работы с грейнами через orlean
            //kernel.Bind<IAuthHandlersFactory>().To<OrleansGrainsFactory>();


            kernel.Load<CommandQueryModule>();
            kernel.Load<BaseModule>();
            kernel.Load<InternalApiModule>();
        }

        public static IKernel GetKernelFromOwinApp(this IAppBuilder app)
        {
            IKernel kernel;
            if (app.Properties.Keys.Contains("kernel"))
                kernel = (IKernel)app.Properties["kernel"];
            else
            {
                kernel = CreateKernel.Value;
                app.Properties.Add("kernel", kernel);
            }
            return kernel;
        }
    }
}