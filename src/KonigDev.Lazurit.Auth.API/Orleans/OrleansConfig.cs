﻿using System;
using System.IO;
using System.Reflection;
using System.Threading;
using Orleans;
using Orleans.Runtime;
using Orleans.Runtime.Configuration;
using Owin;

namespace KonigDev.Lazurit.Auth.API.Orleans
{
    public static class OrleansConfig
    {
        /// <summary>
        /// Инициализация Orleans
        /// </summary>
        public static void UseOrleans(this IAppBuilder app)
        {
#if DEBUG
            ConfigureForLocalHost();
#else
            ConfigureForAzure(app);
#endif
        }

        #region For LocalHost

        private static void ConfigureForLocalHost()
        {
            var config = ClientConfiguration.LocalhostSilo();
            InitializeWithRetries(config, initializeAttemptsBeforeFailing: 5);
        }

        private static void InitializeWithRetries(ClientConfiguration config, int initializeAttemptsBeforeFailing)
        {
            int attempt = 0;
            while (true)
            {
                try
                {
                    GrainClient.Initialize(config);
                    Console.WriteLine("Client successfully connect to silo host");
                    break;
                }
                catch (SiloUnavailableException)
                {
                    attempt++;
                    Console.WriteLine($"Attempt {attempt} of {initializeAttemptsBeforeFailing} failed to initialize the Orleans client.");
                    if (attempt > initializeAttemptsBeforeFailing)
                    {
                        throw;
                    }
                    Thread.Sleep(TimeSpan.FromSeconds(2));
                }
            }
        }

        #endregion

        #region For Azure
        /// <summary>
        /// http://centur-orleans.readthedocs.io/en/gh-pages/Step-by-step-Tutorials/Cloud-Deployment.html
        /// </summary>
        /// <param name="app"></param>
        private static void ConfigureForAzure(IAppBuilder app)
        {
            var path = GetAssemblyPath();
            var configFilePath = Path.Combine(path, "ClientConfiguration.xml");
            var clientConfigFile = new FileInfo(configFilePath);
            if (!clientConfigFile.Exists)
            {
                throw new FileNotFoundException($"Не получается найти Orleans client файл конфигурации {clientConfigFile.FullName}",clientConfigFile.FullName);
            }
            app.Use(
                async (context, next) =>
                {
                    ////TODO Раскоментировать при настройки Azure
                    //if (!OrleansAzureClient.IsInitialized)
                    //{
                    //    OrleansAzureClient.Initialize(clientConfigFile);
                    //}

                    await next.Invoke();
                });
        }

        /// <summary>
        /// Возвращает путь выполняемой сбори
        /// </summary>
        /// <returns></returns>
        private static string GetAssemblyPath()
        {
            var codeBase = Assembly.GetExecutingAssembly().CodeBase;
            var codeBaseUri = new UriBuilder(codeBase);
            var pathString = Uri.UnescapeDataString(codeBaseUri.Path);
            return Path.GetDirectoryName(pathString);
        }
        #endregion
    }
}