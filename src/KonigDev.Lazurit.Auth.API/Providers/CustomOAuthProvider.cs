﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using KonigDev.Lazurit.Auth.API.Extensions.Filters;
using KonigDev.Lazurit.Auth.Grains.Interfaces;
using KonigDev.Lazurit.Auth.Model.DTO.Users;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Queries;
using KonigDev.Lazurit.Core.Services;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;

namespace KonigDev.Lazurit.Auth.API.Providers
{
    public class CustomOAuthProvider : OAuthAuthorizationServerProvider
    {
        private readonly IAuthHandlersFactory _authHandlersFactory;
        private readonly ILoggingService _logger;

        public CustomOAuthProvider(IAuthHandlersFactory authHandlersFactory, ILoggingService logger)
        {
            _authHandlersFactory = authHandlersFactory;
            _logger = logger;
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            try
            {
                string clientSecret;
                string clientId;
                if (!context.TryGetBasicCredentials(out clientId, out clientSecret))
                {
                    context.TryGetFormCredentials(out clientId, out clientSecret);
                }

                //TODO доставать из репозитория
                string audienceId = ConfigurationManager.AppSettings["as:AudienceId"];

                if (context.ClientId == null || context.ClientId != audienceId)
                {
                    context.SetError("invalid_clientId", "Не верный параметр Client_Id.");
                    return Task.FromResult<object>(null);
                }

                string userId = context.Parameters.Where(f => f.Key == "userId").Select(f => f.Value).SingleOrDefault()?[0];
                string userPhone = context.Parameters.Where(f => f.Key == "userPhone").Select(f => f.Value).SingleOrDefault()?[0];
                if (!string.IsNullOrEmpty(userId))
                    context.OwinContext.Set("userId", userId);
                if (!string.IsNullOrEmpty(userPhone))
                    context.OwinContext.Set("userPhone", userPhone);
                context.Validated();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

            return Task.FromResult<object>(null);
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            try
            {
                UserDto user = null;

                //добавляем возможность делать кросс-доменные запросы 
                var allowedOrigin = "*";
                context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });

                /* todo посмотреть в сторону оптимизации. Вытягивать все одним запросом? И для проверки наличия ползователия и для самой авторизации? */
                /* проверка на наличие пользоватлея */
                var existUser = await CheckUser(context);
                if (!existUser)
                {
                    context.SetError("user_not_found", "Не найден пользователь.");
                    return;
                }

                if (!string.IsNullOrEmpty(context.UserName))
                    user = await TryToFindUserByUserName(context.Password, context.UserName);
                else
                {
                    user = await TryToFindUserByUserId(context);
                    if (user == null)
                        user = await TryToFindUserByUserPhone(context);
                }

                if (user == null)
                {
                    context.SetError("invalid_grant", "Логин или пароль не верны.");
                    return;
                }

                ClaimsIdentity oAuthIdentity = user.ClaimsIdentity;
                List<RoleDto> dtoRoles = new List<RoleDto>();
                foreach (var userRole in user.Roles)
                {
                    var getRoleHandler = _authHandlersFactory.CreateQueryHandler<GetRoleQuery, RoleDto>();
                    RoleDto roleDto = await getRoleHandler.Execute(new GetRoleQuery { RoleId = userRole.RoleId });
                    dtoRoles.Add(roleDto);
                }

                if (!user.EmailConfirmed)
                {
                    if (dtoRoles.Any(p => p.IsSeller()))
                    {
                        oAuthIdentity.AddClaim(new Claim(ClaimTypes.UserData, user.EmailConfirmed.ToString()));
                    }
                    else
                    {
                        context.SetError("email_not_confirmed", "Пользователь не подтвердил Email.");
                        return;
                    }
                }

                foreach (var userRole in dtoRoles)
                {
                    oAuthIdentity.AddClaim(new Claim(ClaimTypes.Role, userRole.Name));
                }
                var ticket = new AuthenticationTicket(oAuthIdentity, null);

                context.Validated(ticket);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public async Task<bool> CheckUser(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var valid = false;
            if (!string.IsNullOrEmpty(context.UserName))
            {
                valid = await CheckUserExistByUserName(context.UserName);
            }
            else
            {
                valid = await CheckUserExistByUserId(context);
                if (!valid)
                    valid = await CheckUserExistByUserPhone(context);
            }
            return valid;
        }

        private async Task<bool> CheckUserExistByUserName(string email)
        {
            var findUserByUserName = await _authHandlersFactory.CreateQueryHandler<GetUserByEmailQuery, UserDto>().Execute(new GetUserByEmailQuery { Email = email });

            if (findUserByUserName == null)
                return false;
            else return true;
        }

        private async Task<bool> CheckUserExistByUserPhone(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var userPhone = context.OwinContext.Get<string>("userPhone");
            if (!string.IsNullOrEmpty(userPhone))
            {
                var findUserByPhone = await _authHandlersFactory.CreateQueryHandler<GetUserByPhoneQuery, UserDto>().Execute(new GetUserByPhoneQuery { UserPhone = userPhone });
                if (findUserByPhone == null)
                    return false;
                else return true;
            }
            return false;
        }

        private async Task<bool> CheckUserExistByUserId(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var userId = context.OwinContext.Get<string>("userId");
            if (!string.IsNullOrEmpty(userId))
            {
                var findUserById = await _authHandlersFactory.CreateQueryHandler<GetUserByIdQuery, UserDto>().Execute(new GetUserByIdQuery { Id = Guid.Parse(userId) });
                if (findUserById == null)
                    return false;
                else return true;
            }
            return false;
        }

        private async Task<UserDto> TryToFindUserByUserPhone(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var userPhone = context.OwinContext.Get<string>("userPhone");
            if (!string.IsNullOrEmpty(userPhone))
            {
                var findUserHandler = _authHandlersFactory.CreateQueryHandler<UserByPhoneAndPassQuery, UserDto>();
                return await findUserHandler.Execute(new UserByPhoneAndPassQuery
                {
                    AuthentificationType = "JWT",
                    Password = context.Password,
                    UserPhone = userPhone
                });
            }
            return await Task.FromResult(default(UserDto));
        }

        private async Task<UserDto> TryToFindUserByUserId(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var userId = context.OwinContext.Get<string>("userId");
            if (!string.IsNullOrEmpty(userId))
            {
                var findUserHandler = _authHandlersFactory.CreateQueryHandler<UserByIDAndPassQuery, UserDto>();
                return await findUserHandler.Execute(new UserByIDAndPassQuery
                {
                    AuthentificationType = "JWT",
                    Password = context.Password,
                    UserId = Guid.Parse(userId)
                });
            }
            return await Task.FromResult(default(UserDto));
        }

        private async Task<UserDto> TryToFindUserByUserName(string password, string userName)
        {
            var findUserHandler = _authHandlersFactory.CreateQueryHandler<UserByLoginAndPassQuery, UserDto>();
            return await findUserHandler.Execute(new UserByLoginAndPassQuery
            {
                AuthentificationType = "JWT",
                Password = password,
                UserName = userName
            });
        }
    }
}