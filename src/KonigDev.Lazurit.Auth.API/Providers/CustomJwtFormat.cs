﻿using System;
using System.Configuration;
using System.IdentityModel.Tokens;
using KonigDev.Lazurit.Auth.API.Security;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler.Encoder;

namespace KonigDev.Lazurit.Auth.API.Providers
{
    public class CustomJwtFormat : ISecureDataFormat<AuthenticationTicket>
    {
        private readonly string _issuer = string.Empty;

        public CustomJwtFormat(string issuer)
        {
            _issuer = issuer;
        }

        public string Protect(AuthenticationTicket data)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            string audienceId = ConfigurationManager.AppSettings["as:AudienceId"];
            string symmetricKeyAsBase64 = ConfigurationManager.AppSettings["as:AudienceSecret"];
            
            var keyByteArray = TextEncodings.Base64Url.Decode(symmetricKeyAsBase64);
            var sygningKey= new SigningCredentials(new InMemorySymmetricSecurityKey(keyByteArray), HmacSigningCredentialsHelper.CreateSignatureAlgorithm(keyByteArray), HmacSigningCredentialsHelper.CreateDigestAlgorithm(keyByteArray));
            var issued = data.Properties.IssuedUtc;
            var expires = data.Properties.ExpiresUtc;

            var token = new JwtSecurityToken(_issuer, audienceId, data.Identity.Claims, issued?.UtcDateTime, expires?.UtcDateTime, sygningKey);
            var handler = new JwtSecurityTokenHandler();
            var jwt = handler.WriteToken(token);
            return jwt;
        }

        public AuthenticationTicket Unprotect(string protectedText)
        {
            throw new NotImplementedException();
        }
    }
}