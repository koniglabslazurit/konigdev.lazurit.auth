﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Helpers;
using System.Web.Http;
using KonigDev.Lazurit.Auth.API.ViewModels;
using KonigDev.Lazurit.Auth.Model.DTO.Users;

namespace KonigDev.Lazurit.Auth.API.Areas.HelpPage.SampleGeneration.TypeSampleHelpers
{
    public static class CustomSampleExtensions
    {
        public static HttpConfiguration ForCreateUserVMSamples(this HttpConfiguration config)
        {
            var sample = new CreateUserVM
            {
                Email = "test@email.ru",
                //Username = "testUser",
                FirstName = "Иван",
                LastName = "Иванов",
                Password = "123456",
                ConfirmPassword = "123456",
                RoleName = "salonSaller"
            };
            config.SetSampleForType(sample.ToForUrlEncoded(), new MediaTypeHeaderValue("application/x-www-form-urlencoded"), typeof(CreateUserVM));
            config.SetSampleForType(sample.ToJSON(), new MediaTypeHeaderValue("application/json"), typeof(CreateUserVM));
            config.SetSampleForType(sample.ToJSON(), new MediaTypeHeaderValue("text/json"), typeof(CreateUserVM));
            config.SetSampleForType(sample.ToXml<CreateUserVM>(), new MediaTypeHeaderValue("application/xml"), typeof(CreateUserVM));
            config.SetSampleForType(sample.ToXml<CreateUserVM>(), new MediaTypeHeaderValue("text/xml"), typeof(CreateUserVM));
            return config;
        }

        public static HttpConfiguration ForUserDtoSample(this HttpConfiguration config)
        {
            var sample = new UserDto
            {
                Id = Guid.NewGuid(),
                Email = "test@email.ru",
                UserName = "testUser",
                FirstName = "Иван",
                LastName = "Иванов",
                PhoneNumber = "999 9999 99999",
                Roles = new List<UserRoleDto>
                {new UserRoleDto {RoleId=Guid.NewGuid(),UserId=Guid.NewGuid() }}
            };
            config.SetSampleForType(sample.ToJSON(), new MediaTypeHeaderValue("application/json"), typeof(UserDto));
            config.SetSampleForType(sample.ToJSON(), new MediaTypeHeaderValue("text/json"), typeof(UserDto));
            config.SetSampleForType(sample.ToXml<UserDto>(), new MediaTypeHeaderValue("application/xml"), typeof(UserDto));
            config.SetSampleForType(sample.ToXml<UserDto>(), new MediaTypeHeaderValue("text/xml"), typeof(UserDto));
            return config;
        }

        public static HttpConfiguration ForVmUserConfirmEmailRequestSample(this HttpConfiguration config)
        {
            var sample = new VmUserConfirmEmailRequest
            {
               UserId=Guid.NewGuid(),
               HtmlBody=@"<div>Для подтверждения регистрации в приложении XXX пройдите по ссылке YYY</div>"
            };
            config.SetSampleForType(sample.ToForUrlEncoded(), new MediaTypeHeaderValue("application/x-www-form-urlencoded"), typeof(VmUserConfirmEmailRequest));
            config.SetSampleForType(sample.ToJSON(), new MediaTypeHeaderValue("application/json"), typeof(VmUserConfirmEmailRequest));
            config.SetSampleForType(sample.ToJSON(), new MediaTypeHeaderValue("text/json"), typeof(VmUserConfirmEmailRequest));
            config.SetSampleForType(sample.ToXml<VmUserConfirmEmailRequest>(), new MediaTypeHeaderValue("application/xml"), typeof(VmUserConfirmEmailRequest));
            config.SetSampleForType(sample.ToXml<VmUserConfirmEmailRequest>(), new MediaTypeHeaderValue("text/xml"), typeof(VmUserConfirmEmailRequest));
            return config;
        }
    }
}