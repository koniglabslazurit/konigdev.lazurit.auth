﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml;
using System.Xml.Serialization;

namespace KonigDev.Lazurit.Auth.API.Areas.HelpPage.SampleGeneration.TypeSampleHelpers
{
    public static class FormatGeneratedHelper
    {
        public static string ToJSON(this object obj)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return serializer.Serialize(obj);
        }

        public static string ToJSON(this object obj, int recursionDepth)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            serializer.RecursionLimit = recursionDepth;
            return serializer.Serialize(obj);
        }

        public static string ToForUrlEncoded(this object obj)
        {
            string result = string.Empty;
            foreach (var prop in obj.GetType().GetProperties())
            {
                var val = prop.GetValue(obj);
                if ((!(val is string) && val != null) || !string.IsNullOrWhiteSpace((string) val))
                    result += $"{prop.Name}={prop.GetValue(obj)}&";
            }
            result = result.Remove(result.Length - 1);
            return result;
        }

        public static string ToXml<T>(this object obj)
        {
            XmlSerializer xsSubmit = new XmlSerializer(typeof(T));
            var xml = string.Empty;

            using (var sww = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(sww))
                {
                    xsSubmit.Serialize(writer, obj);
                    xml = sww.ToString(); // Your XML
                }
            }
            return xml;
        }
    }
}