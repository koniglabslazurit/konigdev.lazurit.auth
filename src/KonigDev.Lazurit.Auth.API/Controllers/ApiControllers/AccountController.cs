﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using KonigDev.Lazurit.Auth.API.Extensions;
using KonigDev.Lazurit.Auth.API.Mappings.Interfaces;
using KonigDev.Lazurit.Auth.API.ViewModels;
using KonigDev.Lazurit.Auth.Grains.Interfaces;
using KonigDev.Lazurit.Auth.InternalAPI.AccountApi.Interfaces;
using KonigDev.Lazurit.Auth.Model.DTO.Users;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Commands.Email;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Queries;
using KonigDev.Lazurit.Core.Services;

namespace KonigDev.Lazurit.Auth.API.Controllers.ApiControllers
{
    [NotImplExceptionFilter]
    [RoutePrefix("api/accounts")]
    public class AccountController : ApiController
    {
        private readonly IAccountVMMapper _accountVmMapper;
        private readonly IAuthHandlersFactory _authFactory;
        private readonly IAccountInternalAPI _accountInternalApi;
        private readonly ILoggingService _logger;

        public AccountController(IAccountVMMapper accountVmMapper, IAuthHandlersFactory authFactory,
            IAccountInternalAPI accountInternalApi, ILoggingService loggingService)
        {
            _accountVmMapper = accountVmMapper;
            _authFactory = authFactory;
            _accountInternalApi = accountInternalApi;
            _logger = loggingService;
        }

        /// <summary>
        /// Получение всех пользователей системы.
        /// </summary>
        /// <returns>Список пользователей</returns>
        [Authorize]
        [Route("users")]
        [ResponseType(typeof(List<UserDto>))]
        public async Task<IHttpActionResult> GetUsers()
        {
            var queryHandler = _authFactory.CreateQueryHandler<AllUsersQuery, List<UserDto>>();
            List<UserDto> result = await queryHandler.Execute(new AllUsersQuery());
            return Ok(result);
        }

        /// <summary>
        /// Получение пользователя по ID
        /// </summary>
        /// <param name="id">ID пользователя в формате Guid</param>
        /// <returns>Информацию о пользователе</returns>
        [Authorize]
        [Route("user/{id:guid}", Name = "GetUserById")]
        [ResponseType(typeof(UserDto))]
        public async Task<IHttpActionResult> GetUser(Guid id)
        {
            var getUserQueryHandler = _authFactory.CreateQueryHandler<GetUserByIdQuery, UserDto>();
            var user = await getUserQueryHandler.Execute(new GetUserByIdQuery { Id = id });

            if (user == null) return NotFound();

            return Ok(user);
        }

        /// <summary>
        /// Регистрация пользователя в системе
        /// </summary>
        /// <param name="createUserVm">Модель представления дл пользователя</param>
        /// <returns>Сообщение Http ответа, включающего статус код (StatusCode) и данные (Data)</returns>
        [AllowAnonymous]
        [Route("create")]
        public async Task<HttpResponseMessage> CreateUser(CreateUserVM createUserVm)
        {
            if (!ModelState.IsValid)
            {
                var errorMessage = GetModelStateErrors(ModelState);
                _logger.Error(errorMessage, createUserVm);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errorMessage);
            }

            var confirmationTokenResult = await _accountInternalApi.CreateUser(createUserVm.Password, _accountVmMapper.MapToCreateUserDTO(createUserVm));

            return !confirmationTokenResult.IsSuccess ?
                Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Ошибка создания пользователя: " + confirmationTokenResult.ErrorMessage) :
                Request.CreateResponse(HttpStatusCode.OK, new VmUserTokenResponse { UserId = confirmationTokenResult.UserId, Token = confirmationTokenResult.Token });
        }

        /// <summary>
        /// Послать email сообщение о подтверждении регистрации пользователя
        /// </summary>
        /// <param name="request">Модель представления для отсылки подтверждения регистрации пользователя</param>
        /// <returns>Сообщение Http ответа, включающего статус код (StatusCode) и данные (Data)</returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("sendConfirmEmail")]
        public async Task<HttpResponseMessage> SendEmail(VmUserConfirmEmailRequest request)
        {
            var sendEmailCmdHandler = _authFactory.CreateCommandHandler<SendEmailToUserCmd>();
            await sendEmailCmdHandler.Execute(new SendEmailToUserCmd
            {
                UserId = request.UserId,
                Subject = "Подтверждение регистрации",
                Body = request.HtmlBody
            });
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        /// <summary>
        /// Подтвердить Email пользователя
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Сообщение Http ответа, включающего статус код (StatusCode) и данные (Data). При наличии ошибки ответ содержит ошибку.</returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("ConfirmEmail", Name = "ConfirmEmailRoute")]
        public async Task<IHttpActionResult> ConfirmEmail(VmUserConfirmRegistrationRequest request)
        {
            if (!ModelState.IsValid)
            {
                var errorMessage = GetModelStateErrors(ModelState);
                _logger.Error(errorMessage, request);
                return BadRequest(errorMessage);
            }

            var confirmResult = await _accountInternalApi.ConfirmEmail(_accountVmMapper.MapVMUserConfirmToDTOUserConfirm(request));
            if (!confirmResult.IsSuccess)
                return BadRequest(confirmResult.ErrorMessage);
            return Ok();
        }

        /// <summary>
        /// Восстановаление пароля пользователя по Email
        /// </summary>
        /// <param name="email">email пользователя, пароль которого необходимо восстановить</param>
        /// <returns>Сообщение Http ответа, включающего статус код (StatusCode) и данные (Data). При наличии ошибки ответ содержит ошибку.</returns>
        [Route("RecoveryPassword")]
        public async Task<IHttpActionResult> RecoveryPassword(string email)
        {
            var recoveryResult = await _accountInternalApi.RecoveryPasswordAsync(email);
            if (recoveryResult.IsSuccess)
                return Ok();
            else return BadRequest(recoveryResult.ErrorMessage);
        }

        private static string GetModelStateErrors(ModelStateDictionary modelState)
        {
            var message = modelState.ToDictionary(p => p.Key, p => p.Value.Errors.Select(e => e.ErrorMessage))
                  .Where(p => !string.IsNullOrWhiteSpace(p.Key))
                  .Select(error => error.Value.ToList())
                  .Aggregate("", (current1, e) => e.Aggregate(current1, (current, str) => string.Format("{0}{1};", current, str)));
            return message;
        }
    }
}
