using System;

namespace KonigDev.Lazurit.Auth.API.ViewModels
{
    public class VmUserTokenResponse { public Guid UserId { get; set; } public string Token { get; set; } }
}