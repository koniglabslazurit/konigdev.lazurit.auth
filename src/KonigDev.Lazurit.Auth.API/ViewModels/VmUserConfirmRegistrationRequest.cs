﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KonigDev.Lazurit.Auth.API.ViewModels
{
    /// <summary>
    /// Модель представления для подтверждения регистрации пользователя
    /// </summary>
    public class VmUserConfirmRegistrationRequest
    {
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        [Required]
        public Guid UserId { get; set; }

        /// <summary>
        /// Токен, сгенерированный для подтверждения регистрации
        /// </summary>
        [Required]
        public string Token { get; set; }

        /// <summary>
        /// Логин пользователя
        /// </summary>
        [Required]
        public string Login { get; set; }

        /// <summary>
        /// Пароль
        /// </summary>
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        /// <summary>
        /// Подтверждение пароля
        /// </summary>
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Подтверждение пароля")]
        [Compare("Password", ErrorMessage = "Поля пароль и подтверждение пароля не совпадают.")]
        public string ConfirmPassword { get; set; }

        /// <summary>
        /// Телефон пользователя
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// True-пользователь согласен на получение рассылки
        /// </summary>
        public bool IsAgreed { get; set; }
        /// <summary>
        /// Адрес пользвоателя
        /// </summary>
        public string Address { get; set; }
    }
}