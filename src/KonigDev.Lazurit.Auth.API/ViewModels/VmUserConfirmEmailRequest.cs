﻿using System;

namespace KonigDev.Lazurit.Auth.API.ViewModels
{
    /// <summary>
    /// Модель представления для отсылки email сообщения подтверждения регистрации пользователя
    /// </summary>
    public class VmUserConfirmEmailRequest
    {
        /// <summary>
        /// Уникальный идентификатор пользователя
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Строка, содержащая Html разметку письма. Для каждого клиента тело письма может быть разным, поэтому клиенты генерируют сами тело письма.
        /// </summary>
        public string HtmlBody { get; set; }
    }
}