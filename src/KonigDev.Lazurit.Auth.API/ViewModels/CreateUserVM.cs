﻿using System.ComponentModel.DataAnnotations;

namespace KonigDev.Lazurit.Auth.API.ViewModels
{
    /// <summary>
    /// Модель представления для создания пользователя
    /// </summary>
    public class CreateUserVM
    {
        /// <summary>
        /// Электронная почта
        /// </summary>
        [Required]
        [EmailAddress]
        [Display(Name = "Электронная почта")]
        public string Email { get; set; }

        ///// <summary>
        ///// Логин пользователя
        ///// </summary>
        //[Required]
        //[Display(Name = "Логин")]
        //public string Username { get; set; }

        /// <summary>
        /// Имя пользователя
        /// </summary>
        [Display(Name = "Имя пользователя")]
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия пользователя
        /// </summary>
        [Display(Name = "Фамилия пользователя")]
        public string LastName { get; set; }

        /// <summary>
        /// Название Роли, к которой будет пользователь пренадлежать. (Список Ролей: contentManager, marketer, buyer, region, salonSaller, shopSaller, saller, anonym )
        /// </summary>
        [Display(Name = "Название Роли, к которой будет пользователь пренадлежать")]
        public string RoleName { get; set; }

        /// <summary>
        /// Пароль
        /// </summary>
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        /// <summary>
        /// Подтверждение пароля.
        /// </summary>
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Подтверждение пароля")]
        [Compare("Password", ErrorMessage = "Поле пароль и подтверждение пароля не совпадают.")]
        public string ConfirmPassword { get; set; }
    }
}