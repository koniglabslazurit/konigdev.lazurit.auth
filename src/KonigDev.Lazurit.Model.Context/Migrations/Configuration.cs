using KonigDev.Lazurit.Model.Entities;

namespace KonigDev.Lazurit.Model.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DataContext.LazuritBaseContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DataContext.LazuritBaseContext context)
        {
            var fromMailSetting = context.Set<Settings>().SingleOrDefault(x => x.Name == Constants.FromMailName);
            if (fromMailSetting == null)
                context.Set<Settings>().Add(new Settings { Id = Guid.NewGuid(), Name = Constants.FromMailName, Value = Constants.FromMailTestValue });
            var smtpPortSetting = context.Set<Settings>().SingleOrDefault(x => x.Name == Constants.SmtpPortName);
            if (smtpPortSetting == null)
                context.Set<Settings>().Add(new Settings { Id = Guid.NewGuid(), Name = Constants.SmtpPortName, Value = Constants.SmtpPortTestValue });
            var smtpHostSetting = context.Set<Settings>().SingleOrDefault(x => x.Name == Constants.SmtpHostName);
            if (smtpHostSetting == null)
                context.Set<Settings>().Add(new Settings { Id = Guid.NewGuid(), Name = Constants.SmtpHostName, Value = Constants.SmtpHostTestValue });
            var smtpUserNameSetting = context.Set<Settings>().SingleOrDefault(x => x.Name == Constants.SmtpUserCredentName);
            if (smtpUserNameSetting == null)
                context.Set<Settings>().Add(new Settings { Id = Guid.NewGuid(), Name = Constants.SmtpUserCredentName, Value = Constants.UserNameTestValue });
            var smtpPasswordSetting = context.Set<Settings>().SingleOrDefault(x => x.Name == Constants.SmtpPasswordCredentName);
            if (smtpPasswordSetting == null)
                context.Set<Settings>().Add(new Settings { Id = Guid.NewGuid(), Name = Constants.SmtpPasswordCredentName, Value = Constants.PasswordTestValue });
        }
    }
}
