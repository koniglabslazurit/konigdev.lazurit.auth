namespace KonigDev.Lazurit.Model.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UsersProfiles",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Title = c.String(),
                        Email = c.String(),
                        Phone = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Address = c.String(),
                        SyncCode1C = c.String()
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UsersToShowRoom",
                c => new
                    {
                        UserId = c.Guid(nullable: false),
                        ShowRoomId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.ShowRoomId })
                .ForeignKey("dbo.Showrooms", t => t.ShowRoomId)
                .ForeignKey("dbo.UsersProfiles", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.ShowRoomId);
            
            CreateTable(
                "dbo.Showrooms",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Title = c.String(),
                        CodeAddress = c.String(),
                        CodeCity = c.String(),
                        SyncCode1C = c.String(),
                        RegionId = c.Guid(nullable: false),
                        CityId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cities", t => t.CityId)
                .ForeignKey("dbo.Regions", t => t.RegionId)
                .Index(t => t.RegionId)
                .Index(t => t.CityId);
            
            CreateTable(
                "dbo.Cities",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Title = c.String(),
                        RegionId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Regions", t => t.RegionId)
                .Index(t => t.RegionId);
            
            CreateTable(
                "dbo.Regions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Title = c.String(),
                        SyncCode1C = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UsersToShowRoom", "UserId", "dbo.UsersProfiles");
            DropForeignKey("dbo.UsersToShowRoom", "ShowRoomId", "dbo.Showrooms");
            DropForeignKey("dbo.Showrooms", "RegionId", "dbo.Regions");
            DropForeignKey("dbo.Showrooms", "CityId", "dbo.Cities");
            DropForeignKey("dbo.Cities", "RegionId", "dbo.Regions");
            DropIndex("dbo.Cities", new[] { "RegionId" });
            DropIndex("dbo.Showrooms", new[] { "CityId" });
            DropIndex("dbo.Showrooms", new[] { "RegionId" });
            DropIndex("dbo.UsersToShowRoom", new[] { "ShowRoomId" });
            DropIndex("dbo.UsersToShowRoom", new[] { "UserId" });
            DropTable("dbo.Regions");
            DropTable("dbo.Cities");
            DropTable("dbo.Showrooms");
            DropTable("dbo.UsersToShowRoom");
            DropTable("dbo.UsersProfiles");
        }
    }
}
