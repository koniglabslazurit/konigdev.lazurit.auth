namespace KonigDev.Lazurit.Model.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRelationsUserToRegions : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Settings",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Title = c.String(),
                        Value = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserProfileRegions",
                c => new
                    {
                        UserProfile_Id = c.Guid(nullable: false),
                        Region_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserProfile_Id, t.Region_Id })
                .ForeignKey("dbo.UsersProfiles", t => t.UserProfile_Id, cascadeDelete: true)
                .ForeignKey("dbo.Regions", t => t.Region_Id, cascadeDelete: true)
                .Index(t => t.UserProfile_Id)
                .Index(t => t.Region_Id);
            
            AddColumn("dbo.Regions", "LazuritPasswordHash", c => c.String());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserProfileRegions", "Region_Id", "dbo.Regions");
            DropForeignKey("dbo.UserProfileRegions", "UserProfile_Id", "dbo.UsersProfiles");
            DropIndex("dbo.UserProfileRegions", new[] { "Region_Id" });
            DropIndex("dbo.UserProfileRegions", new[] { "UserProfile_Id" });
            DropColumn("dbo.Regions", "LazuritPasswordHash");
            DropTable("dbo.UserProfileRegions");
            DropTable("dbo.Settings");
        }
    }
}
