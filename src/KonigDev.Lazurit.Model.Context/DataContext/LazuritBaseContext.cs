﻿using KonigDev.Lazurit.Model.Context.Migrations;
using KonigDev.Lazurit.Model.Entities;
using System.Data.Entity;

namespace KonigDev.Lazurit.Model.Context.DataContext
{
    public class LazuritBaseContext : DbContext
    {
        public LazuritBaseContext() : base("name=LazuritBaseContext")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<LazuritBaseContext, Configuration>("LazuritBaseContext"));
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            #region User
            modelBuilder.Entity<UserProfile>()
                .ToTable("UsersProfiles")
                .HasKey(p => p.Id);
            #endregion

            #region UserToShowRoom
            modelBuilder.Entity<UserToShowRoom>()
                .ToTable("UsersToShowRoom")
                .HasKey(p => new { p.UserId, p.ShowRoomId });

            modelBuilder.Entity<UserToShowRoom>()
                .HasRequired(p => p.ShowRoom)
                .WithMany()
                .HasForeignKey(p => p.ShowRoomId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserToShowRoom>()
                .HasRequired(p => p.User)
                .WithMany()
                .HasForeignKey(p => p.UserId)
                .WillCascadeOnDelete(false);
            #endregion           

            #region City
            modelBuilder.Entity<City>()
                .ToTable("Cities")
                .HasKey(p => p.Id);

            modelBuilder.Entity<City>()
                .HasRequired(p => p.Region)
                .WithMany()
                .HasForeignKey(p => p.RegionId)
                .WillCascadeOnDelete(false);
            #endregion

            #region Region
            modelBuilder.Entity<Region>()
                .ToTable("Regions")
                .HasKey(p => p.Id);
            #endregion

            #region ShowRoom
            modelBuilder.Entity<Showroom>()
                .ToTable("Showrooms")
                .HasKey(p => p.Id);

            modelBuilder.Entity<Showroom>()
                .HasRequired(p => p.Region)
                .WithMany()
                .HasForeignKey(p => p.RegionId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Showroom>()
                .HasOptional(p => p.City)
                .WithMany()
                .HasForeignKey(p => p.CityId)
                .WillCascadeOnDelete(false);

            #endregion

            #region RegionsToUsers

            modelBuilder.Entity<UserProfile>()
                .HasMany<Region>(r => r.Regions)
                .WithMany(c => c.UserProfiles);
            #endregion

            #region Setiings

            modelBuilder.Entity<Settings>()
                .ToTable("Settings")
                .HasKey(p => p.Id);
            #endregion

            base.OnModelCreating(modelBuilder);
        }
    }
}
