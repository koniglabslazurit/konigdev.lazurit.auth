﻿using System.Collections.Generic;
using KonigDev.Lazurit.Auth.Grains.Interfaces;
using KonigDev.Lazurit.Auth.Grains.User.CommandHandlers;
using KonigDev.Lazurit.Auth.Grains.User.CommandHandlers.Email;
using KonigDev.Lazurit.Auth.Grains.User.CommandHandlers.Roles;
using KonigDev.Lazurit.Auth.Grains.User.CommandHandlers.UsersHandlers;
using KonigDev.Lazurit.Auth.Grains.User.QueryHandlers;
using KonigDev.Lazurit.Auth.Model.DTO.Common;
using KonigDev.Lazurit.Auth.Model.DTO.Users;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Commands;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Commands.Email;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Queries;
using Ninject.Modules;
using KonigDev.Lazurit.Auth.Model.DTO.Users.Commands.Roles;

namespace KonigDev.Lazurit.Auth.GrainResolver.NinjectModules
{
    public class CommandQueryModule:NinjectModule
    {
        public override void Load()
        {
            Bind<IAuthQueryHandler<GetRoleQuery, RoleDto>>().To<GetRoleQueryHandler>();
            Bind<IAuthQueryHandler<AllUsersQuery, List<UserDto>>>().To<AllUsersQueryHandler>();
            Bind<IAuthQueryHandler<GetUserByIdQuery, UserDto>>().To<GetUserByIdQueryHandler>();
            Bind<IAuthCommandHandler<CreateUserCmd>>().To<CreateUserCmdHandler>();
            Bind<IAuthCommandHandler<SendEmailToUserCmd>>().To<SendEmailToUserCmdHandler>();
            Bind<IAuthQueryHandler<GetEmailConfirmTokenForUserQuery, ConfirmationTokenForUserDto>>().To<GetEmailConfirmTokenForUserQueryHandler>();
            Bind<IAuthCommandHandler<ConfirmEmailForUserCmd>>().To<ConfirmEmailForUserCmdHandler>();
            Bind<IAuthQueryHandler<GetEmailConfirmTokenForUserByEmailQuery, ConfirmationTokenForUserDto>>().To<GetEmailConfirmTokenForUserByEmailQueryHandler>();
            Bind<IAuthQueryHandler<CheckUserByEmailQuery, CheckUserByEmailDtoResult>>().To<CheckUserByEmailQueryHandler>();
            Bind<IAuthCommandHandler<ChangeEmailForUserCmd>>().To<ChangeEmailForUserCmdHandler>();
            Bind<IAuthQueryHandler<GetUserByEmailQuery, UserDto>>().To<GetUserByEmailQueryHandler>();
            Bind<IAuthCommandHandler<SendRecoveryPasswordForUsersCmd>>().To<SendRecoveryPasswordForUsersCmdHandler>();
            Bind<IAuthQueryHandler<UserByLoginAndPassQuery, UserDto>>().To<UserByLoginAndPassQueryHandler>();
            Bind<IAuthCommandHandler<UpdateUserIdentityCmd>>().To<UpdateUserIdentityCmdHandler>();
            Bind<IAuthQueryHandler<UserByIDAndPassQuery, UserDto>>().To<UserByIDAndPassQueryHandler>();
            Bind<IAuthCommandHandler<SetUserEmailVerifiedCmd>>().To<SetUserEmailVerifiedCmdHandler>();
            Bind<IAuthQueryHandler<UserByPhoneAndPassQuery, UserDto>>().To<UserByPhoneAndPassQueryHandler>();
            Bind<IAuthCommandHandler<RestorePasswordCmd>>().To<RestorePasswordCmdHandler>();
            Bind<IAuthQueryHandler<GetUsersByRequestQuery, DtoCommonTableResponse<DtoUserItem>>>().To<GetUsersByRequestQueryHandler>();
            Bind<IAuthCommandHandler<UpdateUserRoleCmd>>().To<UpdateUserRoleCmdHandler>();
            Bind<IAuthCommandHandler<DeleteUserCmd>>().To<DeleteUserCmdHandler>();
            Bind<IAuthQueryHandler<GetUserByPhoneQuery, UserDto>>().To<GetUserByPhoneQueryHandler>();
        }
    }
}
