﻿using System;
using KonigDev.Lazurit.Auth.Grains.Factories;
using KonigDev.Lazurit.Auth.Grains.Services.Implementations;
using KonigDev.Lazurit.Auth.Grains.Services.Interfaces;
using KonigDev.Lazurit.Auth.Model.Context.IdentityEntities;
using KonigDev.Lazurit.Auth.Model.Context.Managers;
using KonigDev.Lazurit.Auth.Model.Mappings.User.Implementations;
using KonigDev.Lazurit.Auth.Model.Mappings.User.Interface;
using KonigDev.Lazurit.Core.Services;
using KonigDev.Lazurit.Core.Transactions;
using Microsoft.AspNet.Identity;
using Ninject;
using Ninject.Modules;

namespace KonigDev.Lazurit.Auth.GrainResolver.NinjectModules
{
    public class BaseModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IEmailTemplateRenderingService>().To<EmailTemplateRenderingService>();
            Bind<ICryptoService>().To<CryptoService>().InTransientScope();
            Bind<ISettingsProvider>().To<DefaultSettingsProvider>().InTransientScope();
            Bind<IUserMapper>().To<UserMapper>().InTransientScope();
            Bind<IRoleMapper>().To<RoleMapper>().InTransientScope();
            Bind<IDBContextFactory>().To<DBContextFactory>().InTransientScope();

            Bind<IIdentityMessageService>().To<EmailService>();
            Bind<IUserStore<ApplicationUser,Guid>>().ToMethod(x => new ApplicationUserStore(Kernel.Get<IDBContextFactory>().CreateAuthIdentityDbContext()));
            Bind<IApplicationUserManager>().To<ApplicationUserManager>();
            Bind<IApplicationRoleManager>().ToMethod(x =>
                      new ApplicationRoleManager(new ApplicationRoleStore(Kernel.Get<IDBContextFactory>().CreateAuthIdentityDbContext())));

            Bind<ITransactionFactory>().To<TransactionFactory>();
        }
    }
}
